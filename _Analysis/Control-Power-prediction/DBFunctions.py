import sqlite3
import numpy as np
import pandas as pd
import datetime as dt
import os

__datapath = os.path.dirname(__file__)
path = __datapath+'\\Prognosedaten.db'

def GetDFfromSqlite(path,tableName,colName=None):
    cnx = sqlite3.connect(path)
    if colName!=None:
        return pd.read_sql_query('select TimeStamp, '+ colName + ' from %s' % tableName,cnx,coerce_float=True)
    else:
        return pd.read_sql_query('select * from %s' % tableName,cnx,coerce_float=True)

def GetDataFrameFromTable(tableName, colName=None):
    df = GetDFfromSqlite(path, tableName,colName)
    df.TimeStamp = pd.to_datetime(df.TimeStamp)
    df = df.set_index('TimeStamp')
    try:
        df = df.drop(columns='Id')
    except:
        return df
    return df

def GetALL():
    return GetNRVSaldo().join(GetLoad()).join(GetIDPrices()).join(GetDAPrices()).join(GetRenewables()).join(GetGeneration()).join(GetGeneration_Units())

def GetNRVSaldo():
    return GetDataFrameFromTable('NRV')

def GetLoad():
    return GetDataFrameFromTable('GridLoad').rename(columns={'Volume':'LoadVolume'})

def GetIDPrices():
    return GetDataFrameFromTable('IntraDay').rename(columns={'Price':'IDPrice','Volume':'IDVolume'})

def GetDAPrices():
    return GetDataFrameFromTable('DayAhead').rename(columns={'Price':'DAPrice','Volume':'DAVolume'})

def GetRenewables():
    return GetDataFrameFromTable('RE')

def GetSRL():
    return GetDataFrameFromTable('SRL')

def GetMRL():
    return GetDataFrameFromTable('MRL')

def GetRebap():
    return GetDataFrameFromTable('Rebap')

def GetGeneration():
    return GetDataFrameFromTable('Generation')

def GetWxMzdb(wxdb_path='C:\\Users\\henri\\source\\repos\\mozubi\\winMoz\\winMoz\\Data\\Files\\SQLITE\\mzdb_wx.db',wx_col='DE_temperature'):
    ## TEMPERATURE from database
    df = GetDFfromSqlite(wxdb_path,'data_weatherUTC')[[wx_col,'utc_timestamp']]
    df.utc_timestamp = pd.to_datetime(df.utc_timestamp)
    df = df.set_index('utc_timestamp')
    df = df.resample(dt.timedelta(minutes=15)).interpolate(method='pchip')
    return df

def GetWx():
    return GetDataFrameFromTable('Weather')

def GetWxSet(colName):
    """ Methode lädt Wetterdaten : Quelle-Renewables.ninja

    Keyword Arguments:
        colName = Columnname/n der Datenbank. Verknüpfung über 'Setname1, Setname2'.
        Auswahl aus folgenden Namen:
        Air_density,Cloud_cover, Irradiance_surface, Irradiance_toa, Precipitation, Snow_mass, Snowfall, SourceId, Temperature, TimeStamp

    Returns:
        [DataFrame] -- [Ausgewählte Wetterdaten]
    """    
    return GetDataFrameFromTable('Weather',colName=colName)

def GetGeneration_Units():
    df = GetDataFrameFromTable('Generation_Units')
    df = df.drop(columns=['HKW_Altbach-Deizisau_Block_2'])
    df = df.resample(dt.timedelta(minutes=15)).interpolate(method='pchip')
    return df
    

def GetSRL4s(fullpath):
    SRL = pd.read_csv(fullpath,delimiter=',', header=None)
    SRL['TimeStamp'] = SRL.iloc[:,0].astype(str) + ' ' + SRL.iloc[:,1].astype(str)
    SRL['TimeStamp'] = pd.to_datetime(SRL['TimeStamp'])
    SRL['Power'] = pd.to_numeric(SRL.iloc[:,3])
    SRL = SRL.drop(columns=[0,1,2,3])
    SRL = SRL.set_index('TimeStamp')
    return SRL

def GetTendersFromCsv(fullpath):
    TD = pd.read_csv(fullpath,delimiter=';')
    TD.DATE_FROM = pd.to_datetime(TD.DATE_FROM, yearfirst=False,dayfirst=True)
    TD.DATE_TO = pd.to_datetime(TD.DATE_TO, yearfirst=False,dayfirst=True)
    return TD