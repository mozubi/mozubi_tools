import pandas as pd
import numpy as np
import datetime
from sklearn import preprocessing
import matplotlib.pyplot as plt
import seaborn as sns

def NormalizeDf(dfIn):
    x = dfIn.values #returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled)
    df.columns = dfIn.columns
    return df

def AppendTimeIndexes(df):
    """    Method appends time-indexes (hour (0-23), quarterhour(0-95), weekday(0-6), dayofyear(0-364)) to dataframe (df) and returns it"""
    idx = df.index
    hours,qhs,weekday,dayofyear=[],[],[],[]
    for i in range(0,len(df)):
        hours.append(idx[i].hour)
        qhs.append(idx[i].hour*4+(idx[i].minute/60*4))
        weekday.append(idx[i].weekday())
        dayofyear.append(idx[i].timetuple().tm_yday)

    df['QuarterHour'] = qhs
    df['Hour'] = hours
    df['WeekDay'] = weekday
    df['DayOfYear'] = dayofyear
    return df

def PlotCorrelationMatrix(cor,annot=False):
    sns.heatmap(cor, annot=annot)
    plt.show()

def GetRelevantFeaturesFromCorr(corArr,target, corrTargetValue):
    #Correlation with output variable
    #Selecting highly correlated features
    relevant_features = corArr[target>corrTargetValue]
    # relevant_features.sort_values(ascending=False)
    return relevant_features