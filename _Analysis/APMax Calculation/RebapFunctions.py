import sqlite3
import numpy as np
import pandas as pd
import datetime as dt
import os

datapath = os.getcwd()+'\Data'

def GetCostsAndRevenueFromSRLTable(SRL):
    kosten = []
    erloes = []

    for i in range(0,len(SRL)):
        cost = GetCosts(SRL.Arbeitspreis_Pos[i],SRL.AbgerMenge_Pos[i],True) + GetCosts(SRL.Arbeitspreis_Neg[i],SRL.AbgerMenge_Neg[i],False) 
        kosten.append(cost)
        
        revenue = GetRevenue(SRL.Arbeitspreis_Pos[i],SRL.AbgerMenge_Pos[i],True) + GetRevenue(SRL.Arbeitspreis_Neg[i],SRL.AbgerMenge_Neg[i],False)
        erloes.append(revenue)

    return pd.DataFrame( np.array([SRL.index,kosten, erloes]).transpose(), columns=['DateTime','Kosten','Erloese']).set_index('DateTime')


def GetCostsAndRevenue(td,mols,product,demand):
    costs = []
    revenue = []
    demand = np.absolute(demand)
    cumCapacity = mols['MOL_CUMULATIVE_CAPACITY_['+product+']'].dropna().reset_index(drop=True)
    idx = cumCapacity.index[cumCapacity > demand][0]

    td = td.where(td.PRODUCT == product).dropna()
    td = td.sort_values(by='ENERGY_PRICE_[EUR/MWh]').reset_index(drop=True)

    for j in range(0,idx):
        bidCapacity = td['ALLOCATED_CAPACITY_[MW]'][j]
        if td['ENERGY_PRICE_PAYMENT_DIRECTION'][j] == 'GRID_TO_PROVIDER':
            costs.append(bidCapacity/900*td['ENERGY_PRICE_[EUR/MWh]'][j])
            revenue.append(0)
        else:
            revenue.append(bidCapacity/900*td['ENERGY_PRICE_[EUR/MWh]'][j])
            costs.append(0)
        demand -= bidCapacity

    if td['ENERGY_PRICE_PAYMENT_DIRECTION'][idx] == 'GRID_TO_PROVIDER':
        costs.append(demand/900*td['ENERGY_PRICE_[EUR/MWh]'][idx])
        revenue.append(0)
    else:
        revenue.append(demand/900*td['ENERGY_PRICE_[EUR/MWh]'][idx])
        costs.append(0)

    return (np.array(costs).sum(), np.array(revenue).sum())

def GetTender(tenders,time):
    tenders = tenders.where(tenders.DATE_FROM <= time).dropna()
    tenders = tenders.where(tenders.DATE_FROM == tenders.DATE_FROM.max()).dropna().reset_index(drop=True)
    return tenders

def GetMOLsForTender(td, time):
    products = td.PRODUCT.unique()
    mols=[]
    for product in products:
        mols.append(__getMOL4Product(td,product))

    df = pd.DataFrame()
    for i in range(0,len(mols)):
        df = pd.concat([df,mols[i]],axis=1)

    df = df.reset_index()
    return df

def __getMOL4Product(td,product):
    td = td.where(td.PRODUCT == product).dropna()
    td = td.sort_values(by='ENERGY_PRICE_[EUR/MWh]')
    # td.where(td['ENERGY_PRICE_PAYMENT_DIRECTION'] == 'PROVIDER_TO_GRID')['ENERGY_PRICE_[EUR/MWh]'] = td.where(td['ENERGY_PRICE_PAYMENT_DIRECTION'] == 'PROVIDER_TO_GRID').dropna()['ENERGY_PRICE_[EUR/MWh]']

    df = pd.DataFrame()
    df.DATE_FROM = td.DATE_FROM
    df.DATE_TO = td.DATE_TO
    df['MOL_ENERGY_PRICE_['+product+']'] = td['ENERGY_PRICE_[EUR/MWh]']
    df['MOL_CUMULATIVE_CAPACITY_['+product+']'] = td['ALLOCATED_CAPACITY_[MW]'].cumsum()
    return df.dropna().reset_index(drop=True)

def GetAPmaxFromMOL(power,mols,product):
    if np.absolute(power) <  np.absolute(mols['MOL_CUMULATIVE_CAPACITY_['+product+']'].values[0]):
        return mols['MOL_ENERGY_PRICE_['+product+']'].values[0]
    elif np.absolute(power) >= np.absolute(mols['MOL_CUMULATIVE_CAPACITY_['+product+']'].max()):
        return mols['MOL_ENERGY_PRICE_['+product+']'].max()
    else:
        return mols.where(mols['MOL_CUMULATIVE_CAPACITY_['+product+']'] >= np.absolute(power))['MOL_ENERGY_PRICE_['+product+']'].dropna().values[0]

def GetZeitScheibe(t):
    if t.weekday() > 5 or t.hour >= 20 or t.hour < 8:
        return 'NT'
    else: 
        return 'HT'

def GetProduct(time, isPositive):
    if isPositive:
        return 'POS_' + GetZeitScheibe(time.to_pydatetime())
    else:
        return 'NEG_' + GetZeitScheibe(time.to_pydatetime())

def AEP1(kostenNrv, erloeseNrv, saldoNrv):
    return (kostenNrv-erloeseNrv)/saldoNrv

def AEP2(aep1, APmax):
    if aep1 >= 0:
        return min(np.abs(aep1),np.abs(APmax))
    else:
        return (-1)*min(np.abs(aep1),np.abs(APmax))

def AEP3(aep2, IDweighted, saldoNrv):
    if saldoNrv < 0:
        return min(IDweighted, aep2)
    else:
        return max(IDweighted,aep2)

def AEP4(aep3,saldoNrv,RLpos,RLneg):
    if saldoNrv > 0.8*RLpos:
        return aep3+max(100,0.5*np.abs(aep3))
    elif saldoNrv < -0.8*RLneg:
        return aep3-max(100,0.5*np.abs(aep3))
    else:
        return aep3

def GetCosts(price, volume, isPositiveProduct):
    if isPositiveProduct:
        if price > 0:
            return price*volume
        else:
            return 0
    else:
        if price < 0:
            return np.absolute(price*volume)
        else:
            return 0

def GetRevenue(price, volume, isPositiveProduct):
    if isPositiveProduct:
        if price < 0:
            return np.absolute(price*volume)
        else:
            return 0
    else:
        if price > 0:
            return -price*volume
        else:
            return 0

def GetWeightedAvgID(IDPrices, IDVolumes):
    prod = []
    for i in range(0,4):
        prod.append(IDPrices[i]*IDVolumes[i])
    
    return np.sum(prod)/np.sum(IDVolumes)