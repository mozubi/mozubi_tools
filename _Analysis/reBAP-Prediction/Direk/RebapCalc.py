import numpy as np

def GetRebap(AP_max,NRV_Saldo,Intraday_Preis,SRL_arbeitspreis_Pos,MRL_arbeitspreis_Pos,SRL_arbeitspreis_Neg,MRL_arbeitspreis_Neg,SRL_AbgerMenge_Pos,SRL_AbgerMenge_Neg,MRL_AbgerMenge_Pos,MRL_AbgerMenge_Neg):
    AEP1  = (SRL_AbgerMenge_Pos*SRL_arbeitspreis_Pos-SRL_AbgerMenge_Neg*SRL_arbeitspreis_Neg+MRL_AbgerMenge_Pos*MRL_arbeitspreis_Pos-MRL_AbgerMenge_Neg*MRL_arbeitspreis_Neg)/NRV_Saldo
    AEP2  = (np.clip(AEP1.copy(), -AP_max, AP_max)) 
    AEP20 = GetAEP20(NRV_Saldo.copy(),AEP2.copy(),Intraday_Preis.copy())
    AEP3  = GetAEP3(NRV_Saldo.copy(),Intraday_Preis.copy(),AEP20.copy())
    AEP4  = GetAEP4(AEP3.copy(),NRV_Saldo.copy(),SRL_arbeitspreis_Pos.copy(),MRL_arbeitspreis_Pos.copy(),SRL_arbeitspreis_Neg.copy(),MRL_arbeitspreis_Neg.copy())
    
    return AEP4

def GetAEP3(nrvsaldo,intraday,AEP20):
    AEP3 = nrvsaldo.copy()
    truth = nrvsaldo >= 0
    for i in range(truth.shape[0]):
        if (truth[i]):
           #print(np.maximum(arr2[i],arr3[i]))#this happens if its positive
            AEP3[i] = np.maximum(intraday[i],AEP20[i])
        else:
            #print(np.minimum(arr2[i],arr3[i]))#this happens if its negative
            AEP3[i] = np.minimum(intraday[i],AEP20[i])

    return AEP3

def GetAEP20(nrvsaldo,AEP2,intraday):
    #nrvsaldo = np.array([-1,2,3,4,5])
    #AEP2     = np.array([1,2,-1000,4,5])
    #intraday = np.array([-1,-2,3,4,5])
    AEP20    = AEP2.copy()

    truth =  (nrvsaldo > -125) & (nrvsaldo < 125) & (AEP2 < 0)
    for i in range(truth.shape[0]):
        if (truth[i]):
            AEP20[i] = -1* np.minimum(abs(AEP2[i]),abs(intraday[i]-100-150*nrvsaldo[i]/125))
    truth =  (nrvsaldo > -125) & (nrvsaldo < 125) & (AEP2 >= 0)
    for i in range(truth.shape[0]):
        if (truth[i]):
            AEP20[i] = np.minimum(abs(AEP2[i]),abs(intraday[i]+100+150*nrvsaldo[i]/125))
            #print(np.maximum(arr2[i],arr3[i]))#this happens if its positive
            #AEP3[i] = np.maximum(intraday[i],AEP20[i])
    return AEP20

def GetAEP4(AEP3,NRV_Saldo,SRL_arbeitspreis_Pos,MRL_arbeitspreis_Pos,SRL_arbeitspreis_Neg,MRL_arbeitspreis_Neg):
    AEP4 = AEP3.copy()
    RL_Pos = SRL_arbeitspreis_Pos + MRL_arbeitspreis_Pos
    RL_Neg = SRL_arbeitspreis_Neg + MRL_arbeitspreis_Neg


    truth = NRV_Saldo > (0.8*RL_Pos)
    for i in range(truth.shape[0]):
        if (truth[i]):
            AEP4[i] = AEP3[i] + np.maximum(100,0.5*abs(AEP3[i]))

    truth = NRV_Saldo < (-0.8*RL_Neg)
    for i in range(truth.shape[0]):
        if (truth[i]):            
            AEP4[i] = AEP3[i] - np.maximum(100,0.5*abs(AEP3[i]))
    
    return AEP4