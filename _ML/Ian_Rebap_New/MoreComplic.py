import warnings
warnings.filterwarnings('ignore')
import eli5
from eli5.sklearn import PermutationImportance
from sklearn.svm import SVC
from sklearn.feature_selection import SelectFromModel
import pandas as pd
import seaborn as sns
import sqlite3
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

cnx = sqlite3.connect('Prognosedaten.db')
strSQL = 'SELECT * from SRL, MRL,Intraday, NRV, RE, SpotMarket WHERE SRL.SRL_DateTime = RE.RE_DateTime AND SRL.SRL_DateTime = MRL.MRL_DateTime AND SRL.SRL_DateTime = Intraday.Intraday_DateTime AND SRL.SRL_DateTime = NRV.NRV_DateTime AND SRL.SRL_DateTime = SpotMarket.Spot_DateTime AND SRL.SRL_AbgerMenge_Pos <> "-" AND SRL.SRL_AbgerMenge_Neg <> "-" AND SRL.SRL_Leistungspreis_Neg <> "-" AND SRL.SRL_Leistungspreis_POS <> "-" ';
df = pd.read_sql_query(strSQL,cnx)

df = df.drop("SRL_DateTime",axis=1)
df = df.drop("SRL_Key",axis=1)
df = df.drop("MRL_DateTime",axis=1)
df = df.drop("MRL_Key",axis=1)
df = df.drop("Intraday_DateTime",axis=1)
df = df.drop("Intraday_Key",axis=1)
df = df.drop("NRV_DateTime",axis=1)
df = df.drop("NRV_Key",axis=1)
df = df.drop("Spot_DateTime",axis=1)
df = df.drop("Spot_Key",axis=1)
df = df.drop("RE_DateTime",axis=1)
df = df.drop("RE_Key",axis=1)
#df = df.drop("Key",axis=1)

df=df.sample(frac=0.01,random_state=200)

train=df.sample(frac=0.8,random_state=200) #random state is a seed value
test=df.drop(train.index)

#train, test = train_test_split(df, test_size=0.2)
pdy_train = train['SRL_AbgerMenge_Pos']
pdX_train = train.drop(['SRL_AbgerMenge_Pos'], axis=1)

pdy_test = test['SRL_AbgerMenge_Pos']
pdX_test = test.drop(['SRL_AbgerMenge_Pos'], axis=1)
labels = list(pdX_train.columns)
y_train = pdy_train.to_numpy()
X_train = pdX_train.to_numpy()
y_test  = pdy_test.to_numpy()
X_test  = pdX_test.to_numpy()

#print(X)
#print("sssssssss")
#print(Y)
print(X_train)
print("sssssssss")
print(y_train)
print(labels)
svc = SVC().fit(X_train, y_train)
perm = PermutationImportance(svc).fit(X_test, y_test)
print(eli5.show_weights(perm))