import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import seaborn as sns
import sqlite3
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
import numpy as np
from sklearn.metrics import mean_squared_error
from math import sqrt

cnx = sqlite3.connect('Prognosedaten.db')
strSQL = 'SELECT * from Rebap, SRL, MRL,Intraday, NRV, RE, SpotMarket WHERE SRL.SRL_DateTime = RE.RE_DateTime AND SRL.SRL_DateTime = Rebap.Rebap_DateTime AND SRL.SRL_DateTime = MRL.MRL_DateTime AND SRL.SRL_DateTime = Intraday.Intraday_DateTime AND SRL.SRL_DateTime = NRV.NRV_DateTime AND SRL.SRL_DateTime = SpotMarket.Spot_DateTime AND SRL.SRL_AbgerMenge_Pos <> "-" AND SRL.SRL_AbgerMenge_Neg <> "-" AND SRL.SRL_Leistungspreis_Neg <> "-" AND SRL.SRL_Leistungspreis_POS <> "-" ';
df = pd.read_sql_query(strSQL,cnx)

df = df.drop("SRL_DateTime",axis=1)
df = df.drop("SRL_Key",axis=1)
df = df.drop("MRL_DateTime",axis=1)
df = df.drop("MRL_Key",axis=1)
df = df.drop("Intraday_DateTime",axis=1)
df = df.drop("Intraday_Key",axis=1)
df = df.drop("NRV_DateTime",axis=1)
df = df.drop("NRV_Key",axis=1)
df = df.drop("Spot_DateTime",axis=1)
df = df.drop("Spot_Key",axis=1)
df = df.drop("RE_DateTime",axis=1)
df = df.drop("RE_Key",axis=1)
df = df.drop("Rebap_DateTime",axis=1)
df = df.drop("Rebap_Key",axis=1)

#print(df)

train=df.sample(frac=0.8,random_state=200) #random state is a seed value
test=df.drop(train.index)

rebap_test = (test['Rebap_Preis']).to_numpy()

pdY_train = train[['SRL_AbgerMenge_Pos', 'SRL_Arbeitspreis_Pos', 'SRL_AbgerMenge_Neg','SRL_Arbeitspreis_Neg','NRV_Saldo']].copy()
pdX_train = train.drop(['SRL_AbgerMenge_Pos', 'SRL_Arbeitspreis_Pos', 'SRL_AbgerMenge_Neg','SRL_Arbeitspreis_Neg','NRV_Saldo','Rebap_Preis'], axis=1)

pdY_test = test[['SRL_AbgerMenge_Pos', 'SRL_Arbeitspreis_Pos', 'SRL_AbgerMenge_Neg','SRL_Arbeitspreis_Neg','NRV_Saldo']].copy()
pdX_test = test.drop(['SRL_AbgerMenge_Pos', 'SRL_Arbeitspreis_Pos', 'SRL_AbgerMenge_Neg','SRL_Arbeitspreis_Neg','NRV_Saldo','Rebap_Preis'], axis=1)
#print(rebap_test)
print(pdX_train.columns)
#print(pdX_train)
Xtrain = pdX_train.to_numpy()
Ytrain = pdY_train.to_numpy()

Xtest  = pdX_test.to_numpy()
Ytest  = pdY_test.to_numpy()

print("Data poccessed starting on the ML stuff")

callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', patience=20)

def get_model():

	model = tf.keras.Sequential()
	model.add(tf.keras.layers.Dense(20, activation='relu', input_shape=[Xtrain.shape[1]]))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(20,    activation='relu'))
	model.add(keras.layers.Dense(10,    activation='relu'))
	model.add(tf.keras.layers.Dense(5))
	
	sgd = tf.optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
	rmspop = tf.keras.optimizers.RMSprop(0.001)#chills at 2.3
	model.compile(optimizer=rmspop,loss='mean_squared_logarithmic_error')
	return model

#optimizer='Adadelta' potentially really good
#optimizer='Adagrad' potentially good


#loss='mean_absolute_error'	
#loss='mean_squared_error'
#loss='mean_squared_logarithmic_error'

model = get_model()
_ = model.fit(Xtrain,Ytrain,
	#batch_size=64,
	epochs=100,
	#steps_per_epoch=5,
	#verbose=0,
	validation_data = (Xtest,Ytest))#,
	#callbacks=[callback])
#print(pdX_train)


predictions = model.predict(Xtest)


#print(predictions)
rebapPredict = (predictions[:,0]*predictions[:,1]+(-1)*predictions[:,2]*predictions[:,3])/predictions[:,4]
rebapPredict = np.clip(rebapPredict, -1000, 1000)
#print(rebapPredict)
plt.plot(rebap_test,color='green',linewidth=0.5)
plt.plot(rebapPredict,color='red',linewidth=0.5)
rms = sqrt(mean_squared_error(rebapPredict, rebap_test))
print("RMS: ")
print(rms)
plt.show()