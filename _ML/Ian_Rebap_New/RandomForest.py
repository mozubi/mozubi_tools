import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from mlxtend.evaluate import feature_importance_permutation

from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
import warnings
warnings.filterwarnings('ignore')
import eli5
from eli5.sklearn import PermutationImportance
from sklearn.svm import SVC
from sklearn.feature_selection import SelectFromModel
import pandas as pd
import seaborn as sns
import sqlite3
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

#There are 3 things that can be changed in this file to get cool results
#1. the percentage value at line 26 choses what percentage of the total lines are used for the calculation, less saves time(currently at 0.01 it only runs the random forest on 1% of all lines)
#2. The items in the array in the for loop at line 28 can be added/removed, these are the features that we want to analyse
#3. comment out the plt.show() by line 133 if you want it to run without showing the graph every time, could be usefull if you want it to generate the images overnight. 

Percentage = 0.8 

for target in ['SRL_AbgerMenge_Pos','SRL_Arbeitspreis_Pos','SRL_AbgerMenge_Neg','SRL_Arbeitspreis_Neg','NRV_saldo']:

	cnx = sqlite3.connect('Prognosedaten.db')
	strSQL = 'SELECT * from GridLoad,Rebap, SRL, MRL,Intraday, NRV, RE, SpotMarket WHERE SRL.SRL_DateTime = RE.RE_DateTime AND SRL.SRL_DateTime = GridLoad.GridLoad_DateTime AND SRL.SRL_DateTime = Rebap.Rebap_DateTime AND SRL.SRL_DateTime = MRL.MRL_DateTime AND SRL.SRL_DateTime = Intraday.Intraday_DateTime AND SRL.SRL_DateTime = NRV.NRV_DateTime AND SRL.SRL_DateTime = SpotMarket.Spot_DateTime AND SRL.SRL_AbgerMenge_Pos <> "-" AND SRL.SRL_AbgerMenge_Neg <> "-" AND SRL.SRL_Leistungspreis_Neg <> "-" AND SRL.SRL_Leistungspreis_POS <> "-"';
	df = pd.read_sql_query(strSQL,cnx)

	df = df.drop("SRL_DateTime",axis=1)
	df = df.drop("SRL_Key",axis=1)
	df = df.drop("MRL_DateTime",axis=1)
	df = df.drop("MRL_Key",axis=1)
	df = df.drop("Intraday_DateTime",axis=1)
	df = df.drop("Intraday_Key",axis=1)
	df = df.drop("NRV_DateTime",axis=1)
	df = df.drop("NRV_Key",axis=1)
	df = df.drop("Spot_DateTime",axis=1)
	df = df.drop("Spot_Key",axis=1)
	df = df.drop("RE_DateTime",axis=1)
	df = df.drop("RE_Key",axis=1)
	df = df.drop("GridLoad_DateTime",axis=1)
	df = df.drop("GridLoad_Key",axis=1)
#df = df.drop("Key",axis=1)

	
	df=df.sample(frac=Percentage,random_state=200)

	train=df.sample(frac=0.8,random_state=200) #random state is a seed value
	test=df.drop(train.index)
	
###change this to change the target field:
	#target = 'SRL_AbgerMenge_Neg'

#train, test = train_test_split(df, test_size=0.2)
	pdy_train = train[target]
	pdX_train = train.drop([target], axis=1)

	pdy_test = test[target]
	pdX_test = test.drop([target], axis=1)
	labels  = list(pdX_train.columns)
	y_train = pdy_train.to_numpy().astype(int)
	X_train = pdX_train.to_numpy().astype(int)
	y_test  = pdy_test.to_numpy().astype(int)
	X_test  = pdX_test.to_numpy().astype(int)

	X = np.concatenate((X_train, X_test), axis=0)
	Y = np.concatenate((y_train, y_test), axis=0)
	
	#print(list(df.columns))
	#print(X_test)
	#print(y_test)
# Build a classification task using 3 informative features
#X, y = make_classification(n_samples=10000,
#                           n_features=10,
#                           n_informative=3,
#                           n_redundant=0,
#                           n_repeated=0,
#                           n_classes=2,
#                           random_state=0,
#                           shuffle=False)

#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1, stratify=y)


	
#print(X_train)
#print(y_train)



	forest = RandomForestClassifier(n_estimators=250,random_state=0)

	#print(X_train)
	#print(y_train)
	#print('SHAPES')
	#print(X_train.shape)
	#print(y_train.shape)
	
	forest.fit(X_train, y_train)

	print('Training accuracy:', np.mean(forest.predict(X_train) == y_train)*100)
	print('Test accuracy:', np.mean(forest.predict(X_test) == y_test)*100)

	labels = labels
	labelsout = labels

	
	importance_vals = forest.feature_importances_
	print(importance_vals)

	std = np.std([tree.feature_importances_ for tree in forest.estimators_],axis=0)
			 
	indices = np.argsort(importance_vals)[::-1]
	i = 0
	for x in indices:
		labelsout[i] = labels[x]
		i+=1
	#print("I:")
	#print(labels)
	# Plot the feature importances of the forest
	plt.figure()
	plt.title("Random Forest feature importance for " + target)
	fig = plt.bar(range(X.shape[1]), importance_vals[indices],
			yerr=std[indices], align="center")
	plt.xticks(range(X.shape[1]), labelsout, rotation='vertical')
	plt.xlim([-1, X.shape[1]])
	plt.ylim([0, 0.5])
	
	plt.savefig(target+'.png')
	plt.show()