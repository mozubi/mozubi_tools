import pandas as pd
import seaborn as sns
import sqlite3
import matplotlib.pyplot as plt

#Pearson correlation
# — A value closer to 0 implies weaker correlation (exact 0 implying no correlation)
# — A value closer to 1 implies stronger positive correlation
# — A value closer to -1 implies stronger negative correlation

cnx = sqlite3.connect('Prognosedaten.db')
strSQL = 'SELECT * from GridLoad,Rebap, MRL ,Intraday, NRV, RE, SpotMarket WHERE MRL.MRL_DateTime = RE.RE_DateTime AND MRL.MRL_DateTime = GridLoad.GridLoad_DateTime AND MRL.MRL_DateTime = Rebap.Rebap_DateTime AND MRL.MRL_DateTime = Intraday.Intraday_DateTime AND MRL.MRL_DateTime = NRV.NRV_DateTime AND MRL.MRL_DateTime = SpotMarket.Spot_DateTime AND MRL.MRL_AbgerMenge_Pos <> "-" AND MRL.MRL_AbgerMenge_Neg <> "-" AND MRL.MRL_Leistungspreis_Neg <> "-" AND MRL.MRL_Leistungspreis_POS <> "-" AND MRL_DateTime NOT LIKE "%2018%"';
df = pd.read_sql_query(strSQL,cnx)

df = df.drop("MRL_DateTime",axis=1)
df = df.drop("MRL_Key",axis=1)
df = df.drop("Intraday_DateTime",axis=1)
df = df.drop("Intraday_Key",axis=1)
df = df.drop("NRV_DateTime",axis=1)
df = df.drop("NRV_Key",axis=1)
df = df.drop("Spot_DateTime",axis=1)
df = df.drop("Spot_Key",axis=1)
df = df.drop("RE_DateTime",axis=1)
df = df.drop("RE_Key",axis=1)
df = df.drop("GridLoad_DateTime",axis=1)
df = df.drop("GridLoad_Key",axis=1)
print(df)


plt.figure(figsize=(10,8))
cor = df.corr()

sns.heatmap(cor, xticklabels=cor.columns,yticklabels=cor.columns, annot=False)
plt.show()