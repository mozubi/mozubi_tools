import numpy as np
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
from sklearn import preprocessing
import os
import pandas as pd
import seaborn as sns


path = 'C:\\Users\\henri\\source\\repos\\mozubi_tools\\TensorFlow\\rebap\\All_with_Saldo.csv'


df = pd.read_csv(path, delimiter=';',  low_memory=False)
df = df.drop(columns= ['Datum', 'Zeit','pv_50HerzProjection','wind_50HerzProjection','pv_amperionProjection','wind_amperionProjection','pv_transnetBWProjection','wind_transnetBWProjection','wind_tennetProjection','pv_tennetProjection'])
df = df.iloc[69000:,:57]

#Pearson correlation
# — A value closer to 0 implies weaker correlation (exact 0 implying no correlation)
# — A value closer to 1 implies stronger positive correlation
# — A value closer to -1 implies stronger negative correlation
plt.figure(figsize=(8,6))
cor = df.corr()
sns.heatmap(cor, annot=False)
plt.show()






