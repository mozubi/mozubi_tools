﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataVis
{
    public class Agent
    {
        public int ID { get; set; }
        public string Type;
        public string TSO_Area;
        public List<Plant> plants;

        public Agent(int ID, string Type, string TSO_Area)
        {
            this.ID       = ID;
            this.Type     = Type;
            this.TSO_Area = TSO_Area;

            plants = new List<Plant>();
        }

        public double GetTotalMWfromNuts2Zone(string zone)
        {
            double total = 0;
            foreach(Plant p in plants)
            {
                if (zone.Equals(p.Nuts2))
                {
                    total += p.PowerMW;
                }
            }
            return total;
        }

        public void addPlant(Plant t)
        {
            this.plants.Add(t);
        }
    }
}
