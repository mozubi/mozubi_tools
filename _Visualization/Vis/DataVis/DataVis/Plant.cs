﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataVis
{
    public enum Type
    {
        plant_thermal,
        plant_pv,
        plant_hydro,
        plant_wind  
    }

    public enum SubType
    {
        Blank,
        bhkw,
        sewageLandfillGas,
        pv_roof,
        onshore,
        geoThermal 
    }
    public class Plant
    {
        public string Nuts2;
        public SubType cSubType;
        public Type cType;
        public int Count;
        public double PowerMW;

        public Plant(string Nuts2, string iType, string iSubType, int Count, double PowerMW)
        {
            this.Nuts2    = Nuts2;

            switch (iSubType)
            {
                case "":
                    this.cSubType = SubType.Blank;
                    break;
                case "bhkw":
                    this.cSubType = SubType.bhkw;
                    break;
                case "sewageLandfillGas":
                    this.cSubType = SubType.sewageLandfillGas;
                    break;
                case "pv_roof":
                    this.cSubType = SubType.pv_roof;
                    break;
                case "onshore":
                    this.cSubType = SubType.onshore;
                    break;
                case "geoThermal":
                    this.cSubType = SubType.geoThermal;
                    break;
                default:
                    break;
            }
            switch (iType)
            {
                case "plant_thermal":
                    this.cType = Type.plant_thermal;
                    break;
                case "plant_pv":
                    this.cType = Type.plant_pv;
                    break;
                case "plant_hydro":
                    this.cType = Type.plant_hydro;
                    break;
                case "plant_wind":
                    this.cType = Type.plant_wind;
                    break;
                default:
                    break;
            }

            this.Count    = Count;
            this.PowerMW  = PowerMW;
        }
    }


}
