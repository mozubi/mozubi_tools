﻿using GMap.NET;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataVis
{

    public class Feature
    {
        public string id { get; set; }
        public string na { get; set; }
        public List<List<PointLatLng>> regions { get; set; }

        public List<GraphicsPath> paths { get; set; }

        public void MakePaths()
        {
           // foreach (var Nut in this.features)//for each NUT2 area
           // {
                //foreach (var el in DATA.features)
                // foreach (List<PointLatLng> area in Nut.coordinates)
            foreach (List<PointLatLng> subregion in this.regions)//go through every polygon in an region
            {
                List<Point> points = new List<Point>();
                foreach (PointLatLng p in subregion)
                {
                    points.Add(new Point((int) (p.Lng*4) + 100, (int)(p.Lat*4) + 200));
                }
                this.InsertGraphicsPathFromPoints(points);
            }
            
                

           // }
        }



        public Feature(string id, String na)
        {
            this.paths = new List<GraphicsPath>();
            this.id = id;
            this.na = na;
            regions = new List<List<PointLatLng>>();
        }

        public void InsertGraphicsPathFromPoints(List<Point> inn)
        {
            this.paths.Add(new GraphicsPath());
            this.paths.ElementAt(paths.Count-1).AddPolygon(inn.ToArray());
            // this.paths.addAddPolygon(inn.ToArray());
        }

    }



    public class GeoJsonFile
    {
        private int ZoomFactor = 5;
        public string type { get; set; }
        public List<Feature> features { get; set; }

        public GeoJsonFile(string type)
        {
            this.type = type;
            features = new List<Feature>();
        }

        public void zoomArea(bool In)
        {
            if (In)
                ZoomFactor+=3;
            else
                ZoomFactor--;
            
           
                foreach (Feature feature in features)
                {
                    feature.paths.Clear();
                    foreach (List<PointLatLng> subregion in feature.regions)
                    {
                       // subregion.
                        List<Point> points = new List<Point>();
                        foreach (PointLatLng p in subregion)
                        {
                            points.Add(new Point((int)(p.Lng * ZoomFactor), (int)(p.Lat * ZoomFactor)));
                        }
                        feature.paths.Add(new GraphicsPath());
                        feature.paths.ElementAt(feature.paths.Count - 1).AddPolygon(points.ToArray());
                    }
                } 
        }
    }
}
