﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ClipperLib;
using System.Drawing.Drawing2D;
using System.IO;

namespace DataVis
{

    public partial class Form1 : Form    {
        List<Agent> agents;
        bool drawRect = false;
        GMapPolygon polygon;
        Rectangle mouseNewRect;
        GraphicsPath path;
        GeoJsonFile allareas = RandomFuncs.GetJsonFile("https://raw.githubusercontent.com/eurostat/Nuts2json/master/2016/4258/20M/nutsrg_2.json");
        private delegate void DoPaint(object sender, PaintEventArgs e);


        //  private IGeckoWebBrowser geckoWebBrowser1;

        public Form1()
        {
            InitializeComponent();
            agents = RandomFuncs.parseXMLfile();


            // geckoWebBrowser1 = new GeckoWebBrowser();
            //    Xpcom.Initialize("Firefox");
            //     geckoWebBrowser1.Navigate("www.bbc.com");
        }



        private void button1_Click(object sender, EventArgs e)
        {



            /*  gMapControl1.MapProvider = GMapProviders.GoogleMap;
              gMapControl1.Position = new PointLatLng(52.2659416, 10.5267296);

              GMapOverlay polygons = new GMapOverlay("polygons");
              List<PointLatLng> points = new List<PointLatLng>();

              points.Add(new PointLatLng(67.083703, -17.165769)); //outter shape
              points.Add(new PointLatLng(67.809726, 37.154292));
              points.Add(new PointLatLng(49.935245, 32.867706));
              points.Add(new PointLatLng(41.221471, -3.183191));
              points.Add(new PointLatLng(67.083703, -17.165769));

              points.Add(new PointLatLng(52.2659416, 10.5267296)); //inner shape
              points.Add(new PointLatLng(62.2659416, 10.5267296));
              points.Add(new PointLatLng(62.2659416, 20.5267296));
              points.Add(new PointLatLng(52.2659416, 20.5267296));
              points.Add(new PointLatLng(52.2659416, 10.5267296));

              GMapPolygon polygon = new GMapPolygon(points, "test");
              polygon.Fill = new SolidBrush(Color.NavajoWhite);
              polygons.Polygons.Add(polygon);
              gMapControl1.Overlays.Add(polygons);*/


            //GMapPolygon polygon;
            // c.AddPolygon(subj,);



            /*
            polygon.Fill = new SolidBrush(Color.NavajoWhite);
            polygons.Polygons.Add(polygon);
            gMapControl1.Overlays.Add(polygons);*/



            //setting up the map and its properties
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
             double lat = 52.2659416;
             double lon = 10.5267296;

             gMapControl1.Position = new PointLatLng(lat, lon);

             gMapControl1.MinZoom = 1;
             gMapControl1.MaxZoom = 100;
             gMapControl1.Zoom = 2;

             //getting all data from the GeoJson
             GeoJsonFile allareas = RandomFuncs.GetJsonFile("https://raw.githubusercontent.com/eurostat/Nuts2json/master/2016/4258/20M/nutsrg_2.json");
             var polygon = new GMapPolygon(new List<PointLatLng>(),"s");
             foreach (var Nut in allareas.features)//for each NUT2 area
             {
                 //foreach (var el in DATA.features)
                // foreach (List<PointLatLng> area in Nut.coordinates)
                 foreach (List<PointLatLng> subregion in Nut.regions)//go through every polygon in an region
                 {
                     polygon = new GMapPolygon(subregion, Nut.id);//add the coordinates which is a list to the Polygon
                     polygon.IsHitTestVisible = true;
                     polygon.Tag = Nut.na;

                     //colour stuff of polygons  more can be found here https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.brushes?view=netframework-4.8

                     polygon.Fill = new SolidBrush(Color.NavajoWhite);   
                     Pen skyBluePen = new Pen(Brushes.Black);
                     skyBluePen.Width = 1.0F;
                     polygon.Stroke = skyBluePen; 

                     var polygons = new GMapOverlay("Polygons");
                     polygons.Polygons.Add(polygon);
                     gMapControl1.Overlays.Add(polygons);
                 }


             }
             

        }

        private void gMapControl1_OnPolygonClick(GMapPolygon item, System.Windows.Forms.MouseEventArgs e)
        {
            MessageBox.Show(String.Format("Polygon {0} with tag {1} was clicked",item.Name, item.Tag));
        }

        private void gMapControl1_OnPolygonEnter(GMapPolygon item)
        {
            chart1.Series.Clear();
            chart1.Legends.Clear();

            chart1.Legends.Add("MyLegend");
            chart1.Legends[0].LegendStyle = LegendStyle.Table;
            chart1.Legends[0].Docking = Docking.Bottom;
            chart1.Legends[0].Alignment = StringAlignment.Center;
            chart1.Legends[0].Title = "MyTitle";
            chart1.Legends[0].BorderColor = Color.Black;


            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisX.MinorGrid.Enabled = false;


            //reset your chart series and legends
            chart2.Series.Clear();
            chart2.Legends.Clear();
            //Add a new Legend(if needed) and do some formating
            chart2.Legends.Add("MyLegend");
            chart2.Legends[0].LegendStyle = LegendStyle.Table;
            chart2.Legends[0].Docking = Docking.Bottom;
            chart2.Legends[0].Alignment = StringAlignment.Center;
            chart2.Legends[0].Title = "MyTitle";
            chart2.Legends[0].BorderColor = Color.Black;
            //Add a new chart-series
            string seriesname = "MySeriesName";
            chart2.Series.Add(seriesname);
            //set the chart-type to "Pie"
            chart2.Series[seriesname].ChartType = SeriesChartType.Pie;



            //webBrowser1.ScriptErrorsSuppressed = true;
            // webBrowser1.DocumentText = System.IO.File.ReadAllText(@"C:\Users\Ian\Documents\GitHub\mozubi_tools\Vis\DataVis\Page_Graphs.html");
            //item.name is die kode van die Nuts2 area
            label1.Text = "name: " + item.Tag +"\nID:" + item.Name ;
            foreach (Agent agent in agents)
            {
                // System.Console.WriteLine(agent.ID);
                // if(agent.ID == item.Name)
               // chart1.Series.Add(agent.TSO_Area);
               
             //   chart1.Series[agent.TSO_Area].Points.Add(((int)agent.GetTotalMWfromNuts2Zone(item.Name)));
                // chart1.Series[agent.TSO_Area]["PointWidth"] = "5";






                //Add some datapoints so the series. in this case you can pass the values to this method
                if ((int)agent.GetTotalMWfromNuts2Zone(item.Name) > 0)
                    chart2.Series[seriesname].Points.AddXY(agent.TSO_Area, (int)agent.GetTotalMWfromNuts2Zone(item.Name));
                if ((int)agent.GetTotalMWfromNuts2Zone(item.Name) > 0)
                {
                    chart1.Series.Add(agent.TSO_Area);
                    chart1.Series[agent.TSO_Area].Points.Add((int)agent.GetTotalMWfromNuts2Zone(item.Name));
                }




                label1.Text = label1.Text + '\n' + agent.TSO_Area + ": " + agent.GetTotalMWfromNuts2Zone(item.Name) +"MW";
              //  foreach (Plant plant in agent.plants)
             //   {
                   // TotalMW += plant.PowerMW;
                   // label1.Text = label1.Text + "\n :" + TotalMW;
                    //System.Console.WriteLine(plant.PowerMW);
              //  }
                               
            }

            // MessageBox.Show(String.Format("Polygon {0} with tag {1} was clicked", item.Name, item.Tag));
        }


        private void gMapControl1_MouseMove(object sender, MouseEventArgs e)
        {
          /*  if (drawRect == false)
            {
                drawRect = true;
            }

            mouseNewRect = new Rectangle(new Point(e.X, e.Y), new Size(100, 100));

            this.Invalidate();*/
        }

        private void Form1_Leave(object sender, EventArgs e)
        {
            //This will erase the rectangle when the mouse leaves Form1
           /* drawRect = false;

            this.Invalidate();*/
        }

        private void gMapControl1_Paint(object sender, PaintEventArgs e)
        {
         /*   if (drawRect)
            {
                e.Graphics.DrawRectangle(new Pen(Brushes.Chocolate), mouseNewRect);
            }*/
        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            using (Matrix m = new Matrix(1, 0, 0, 1, AutoScrollPosition.X, AutoScrollPosition.Y))
            {
                var sY = VerticalScroll.Value;
                var sH = ClientRectangle.Y;
                var w = ClientRectangle.Width - 2 - (VerticalScroll.Visible ? SystemInformation.VerticalScrollBarWidth : 0);
                var h = ClientRectangle.Height;
                var paintRect = new Rectangle(0, sY, w, h); //This will be your painting rectangle.
                var g = e.Graphics;

                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.Transform = m;
                g.Clear(BackColor);

              //  using (Pen pn = new Pen(Color.Black, 2))
              //     e.Graphics.DrawLine(pn, 100, 50, 100, 1000);

                sH += 1050; //Your line.y + line.height
                            //Likewise, you can increase the w to show the HorizontalScroll if you need that.
                AutoScrollMinSize = new Size(w, sH);
            }



            Image bmp = new Bitmap(2000, 2000);
            using (Graphics g2 = Graphics.FromImage(bmp))
            {


                Graphics g = e.Graphics;
           

            foreach (var Nut in allareas.features)
            {
                foreach (GraphicsPath path in Nut.paths)//go through every polygon in an region
                {
                    var brush = new SolidBrush(Color.Blue);
                    g.FillPath(brush, path);
                    g.DrawPath(Pens.Black, path);
                    g2.FillPath(brush, path);
                    g2.DrawPath(Pens.Black, path);
                    }

            }


            }
           // bmp.Save("test.bmp");

          //  Bitmap image1 = new Bitmap("test.bmp", true);
           // e.Graphics.DrawImage(image1, new Point(10, 10));
           // image1.Dispose();
           // pictureBox1.Image = bmp;
            //Graphics m_graphics = Graphics.FromImage(bmp);
            //m_graphics.DrawImage(bmp, 100, 100);
            
            
            /* GraphicsPath path2 = new GraphicsPath();

             List<Point> points = new List<Point>();
             points.Add(new Point(-50, -50));
             points.Add(new Point(200, 0));
             points.Add(new Point(2000, 2000));
             points.Add(new Point(50, 200));
             points.Add(new Point(-50, -50));


             points.Add(new Point(100, 50));
             points.Add(new Point(150, 50));
             points.Add(new Point(150, 150));
             points.Add(new Point(100, 150));
             points.Add(new Point(100, 50));*/

            //  path2.AddPolygon(points.ToArray());

            // var brushh = new SolidBrush(Color.NavajoWhite);

            // g.FillPath(brushh, path2);

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            System.Console.WriteLine(e.X + "," +e.Y);
            System.Console.WriteLine(path.IsVisible(new Point(e.X, e.Y)));
        }

        private void Form1_Scroll(object sender, ScrollEventArgs e)
        {
           /* this.Invalidate();
           // allareas.zoomArea(true);
            Graphics g = this.CreateGraphics();
            DoPaint dp = new DoPaint(Form1_Paint);
            dp.Invoke(this, new PaintEventArgs(g, new Rectangle(0, 0, this.Width, this.Height)));*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Invalidate();
            allareas.zoomArea(true);
            Graphics g1 = this.CreateGraphics();

            DoPaint dp1 = new DoPaint(Form1_Paint);

            dp1.Invoke(this, new PaintEventArgs(g1, new Rectangle(0, 0, this.Width, this.Height)));


            
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {


                /*

            Graphics g = e.Graphics;


            foreach (var Nut in allareas.features)
            {
                foreach (GraphicsPath path in Nut.paths)//go through every polygon in an region
                {
                    var brush = new SolidBrush(Color.Blue);
                    g.FillPath(brush, path);
                    g.DrawPath(Pens.Black, path);
                        //g2.FillPath(brush, path);
                       // g2.DrawPath(Pens.Black, path);
                    }

            }*/


        }

        private void panel1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            /*   using (Matrix m = new Matrix(1, 0, 0, 1, AutoScrollPosition.X, AutoScrollPosition.Y))
               {
                   var sY = VerticalScroll.Value;
                   var sH = ClientRectangle.Y;
                   var w = ClientRectangle.Width - 2 - (VerticalScroll.Visible ? SystemInformation.VerticalScrollBarWidth : 0);
                   var h = ClientRectangle.Height;
                   var paintRect = new Rectangle(0, sY, w, h); //This will be your painting rectangle.
                   var g = e.Graphics;

                   g.SmoothingMode = SmoothingMode.AntiAlias;
                   g.Transform = m;
                   g.Clear(BackColor);

                   //  using (Pen pn = new Pen(Color.Black, 2))
                   //     e.Graphics.DrawLine(pn, 100, 50, 100, 1000);

                   sH += 1050; //Your line.y + line.height
                               //Likewise, you can increase the w to show the HorizontalScroll if you need that.
                   AutoScrollMinSize = new Size(w, sH);




               //Image bmp = new Bitmap(2000, 2000);
            //   using (Graphics g2 = Graphics.FromImage(bmp))
           //   {


                   Graphics g1 = e.Graphics;


                   foreach (var Nut in allareas.features)
                   {
                       foreach (GraphicsPath path in Nut.paths)//go through every polygon in an region
                       {
                           var brush = new SolidBrush(Color.Blue);
                           g1.FillPath(brush, path);
                           g1.DrawPath(Pens.Black, path);
                         //  g2.FillPath(brush, path);
                          // g2.DrawPath(Pens.Black, path);
                       }

                   }

               }
               // }*/
        }
    }
        }
