﻿using System;
using GMap.NET;
using System.IO;
using Newtonsoft.Json;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace DataVis
{
    public static class RandomFuncs
    {
        public static List<Agent> parseXMLfile()
        {
            List <Agent> agents = new List<Agent>();

            XmlDocument doc = new XmlDocument();
            doc.Load(@"191025_UNB_AnlagenStammDaten2018.xml");

            XmlNode StudentListNode     = doc.SelectSingleNode("/root");
            XmlNodeList StudentNodeList = StudentListNode.SelectNodes("Agent");
            int currentID = 0;
            foreach (XmlNode node in StudentNodeList)
            {
              //  System.Console.WriteLine(Convert.ToInt16(node.Attributes.GetNamedItem("Id").Value));
                //  System.Console.WriteLine(node.Attributes.GetNamedItem("Type").Value);
                currentID = Convert.ToInt16(node.Attributes.GetNamedItem("Id").Value);
                agents.Add(new Agent(Convert.ToInt16(node.Attributes.GetNamedItem("Id").Value), node.Attributes.GetNamedItem("Type").Value, node.Attributes.GetNamedItem("TSO_Area").Value));

                XmlNodeList plants = (node.SelectSingleNode("Portfolio")).SelectNodes("Plant");
                 foreach (XmlNode plant in plants)
                 {
                    foreach (Agent agent in agents)
                    {
                        //System.Console.WriteLine(plant.Attributes.GetNamedItem("NUTS2").Value);
                        if (agent.ID == currentID)
                        {
                            //System.Console.WriteLine(plant.Attributes.GetNamedItem("PowerMW").Value.Replace('.',','));
                            agent.addPlant(new Plant(plant.Attributes.GetNamedItem("NUTS2").Value, plant.Attributes.GetNamedItem("Type").Value, plant.Attributes.GetNamedItem("SubType").Value, Convert.ToInt16(plant.Attributes.GetNamedItem("Count").Value), Convert.ToDouble(plant.Attributes.GetNamedItem("PowerMW").Value.Replace('.', ',')) ));
                        }
                    }
                    // System.Console.WriteLine(plant.Attributes.GetNamedItem("NUTS2").Value);
                }
            }


            /* foreach (Agent agent in agents)
             {
                 System.Console.WriteLine(agent.ID);
                 foreach (Plant plant in agent.plants)
                 {
                     System.Console.WriteLine(plant.PowerMW);
                 }

             }*/
            return agents;
        }


        public static byte[] GetFileViaHttp(string url)
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadData(url);
            }
        }




        public static GeoJsonFile GetJsonFile(string URL)
        {
            var result = GetFileViaHttp(@URL);
            string str = Encoding.UTF8.GetString(result);

            JsonTextReader reader = new JsonTextReader(new StringReader(str));////enable that for loading from te online file


          // JsonTextReader reader = new JsonTextReader(new StringReader(System.IO.File.ReadAllText(@"test.json")));//this loads from a spesefix textfile
            bool startOfAreasFound = false;
            bool NeedProperties = false;
            bool readingpoints = false;

            int subregionNo=0;
            string IDtemp = "";
            string naTemp = "";

            GeoJsonFile DATA = new GeoJsonFile("FeatureCollection");
            //  DATA.type = "FeatureCollection";

         /*   while (reader.Read())
            {
                if (reader.Value != null)
                {
                    Console.WriteLine("a");
                    Console.WriteLine(reader.TokenType + "->" + reader.Value);
                }else
                {
                    Console.WriteLine("b");
                    Console.WriteLine(reader.TokenType);
                }

            }*/


            while (reader.Read())
            {
                if ((reader.TokenType).ToString().Equals("StartArray") && (!startOfAreasFound)) // if we start the array of nuts2 areas
                {
                    NeedProperties = true;
                    startOfAreasFound = true;
                }

                if (NeedProperties && (reader.Value != null))//if we are looking for the start of a new area
                {
                    if ((reader.Value).ToString().Equals("id"))//read in the start of new area
                    {
                        reader.Read();
                        IDtemp = (string)reader.Value;
                        reader.Read();
                        reader.Read();
                        naTemp = (string)reader.Value;

                        Feature f = new Feature(IDtemp, naTemp);
                        // MessageBox.Show(f.na + " " + f.id);

                        (DATA.features).Add(f);

                        NeedProperties = false;
                        readingpoints = true;
                    }
                }
                if (readingpoints)//if we are reading points 
                {
                    double lat11 = 0; double lon11 = 0;
                    if (reader.Value != null)
                        if ((reader.Value).ToString().Equals("coordinates"))
                        {
                            Boolean done = false;
                            while (done == false)
                            {
                                reader.Read();
                                if ((reader.TokenType).ToString().Equals("Float"))//if a point is found add it
                                {
                                    lon11 = Convert.ToDouble((reader.Value).ToString());
                                    reader.Read();
                                    lat11 = Convert.ToDouble((reader.Value).ToString());

                                    foreach (var feature in DATA.features)
                                    {
                                        if (feature.id.Equals(IDtemp))
                                        {
                                            //  foreach (var subarea in feature.coordinates)
                                            //   if (subregionNo == 0)

                                            if (feature.regions.Count <=  subregionNo)//if we dont have that subregion yet, add one
                                                feature.regions.Add(new List<PointLatLng>());

                                            feature.regions.ElementAt(subregionNo).Add(new PointLatLng(lat11, lon11));//add a point to the nth subregion

                                        }
                                    }


                                }
                                else if ((reader.TokenType).ToString().Equals("EndObject") && (reader.Value == null))
                                {
                                    done = true;
                                    readingpoints = false;
                                    NeedProperties = true;
                                    subregionNo = 0;
                                }
                                else if ((reader.TokenType).ToString().Equals("EndArray") && (reader.Value == null))
                                {
                                    reader.Read();
                                    if ((reader.TokenType).ToString().Equals("EndArray"))//if there are 2 end arrays, meaning theres hole in the area
                                    {
                                        reader.Read();
                                        if ((reader.TokenType).ToString().Equals("EndArray"))//if there are 3 end arrays, meaning theres a subarea
                                            subregionNo++;
                                    }
                                }
                            }
                        }
                }

            }
            foreach (var Nut in DATA.features)
                Nut.MakePaths();
            return DATA;
        }
    }
}
