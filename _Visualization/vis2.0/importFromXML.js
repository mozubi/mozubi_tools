parser = new DOMParser();
xmlDoc = parser.parseFromString(xmlTEXT,"text/xml");
var agents = [];
var NUTS2RegionNames = [];
var NUTS2_Regions = [];
var count =-1;
// traverse agents
for (let agent of xmlDoc.getElementsByTagName("Agent"))
{
	count++;
   // console.log(agent.attributes);
	agents.push(new Agent(agent.attributes['Id'].value,agent.attributes['Type'].value,agent.attributes['TSO_Area'].value));
	
	//console.log(agent.attributes['TSO_Area'].value);
    // traverse plants
	for (let plant of agent.getElementsByTagName("Plant"))
	{
		if (!NUTS2RegionNames.includes(plant.attributes['NUTS2'].value))
		{
			NUTS2RegionNames.push(plant.attributes['NUTS2'].value);
		}
		
		
		agents[count].AddPLant(new Plant(plant.attributes['Type'].value,plant.attributes['SubType'].value,plant.attributes['NUTS2'].value,plant.attributes['Count'].value,plant.attributes['PowerMW'].value));	
		//if (plant.attributes['PowerMW'].value > 500)
		//	console.log('agent:', agent.attributes, 'plant:', plant.attributes['PowerMW']);
			
		//console.log('agent:', agent.attributes, 'plant:', plant.attributes);
    }
}
//console.log(NUTS2RegionNames);
//////////////////////////
//for (let agent of agents)
//{
//	console.log(agent.plants);
//}

for (let NUTS2Name of NUTS2RegionNames)
{
	//console.log(NUTS2Name);
	NUTS2_Regions.push(new NUTS2region(NUTS2Name,agents));
}
///////////////////////////////////////////////////////////
var MaxPowerInARegion = 0;
for (let region of NUTS2_Regions)
{
	//console.log("a");
	if (parseFloat(region.powerMW) > parseFloat(MaxPowerInARegion))
		MaxPowerInARegion = parseFloat(region.powerMW);
	//console.log(parseFloat(region.powerMW));
	//console.log(MaxPowerInARegion	);
}


//////////////////////////////////////////////////////////

//Get total power in system
for (let agent of agents)
{
	var TotalPower = 0;
	TotalPower += parseFloat(agent.TotalMW());
} 
//////////////////////////////////////////////

