/*------------------add the agents to its dropdown---*/
	    var opt = document.createElement("option");        

		// Assign text and value to Option object
		opt.text = "all agents";
		opt.value = "ALL";

		// Add an Option object to Drop Down List Box
		document.getElementById('selectAgent').options.add(opt);
		
	for (let agent of agents)
	{	
	    var opt = document.createElement("option");        

		// Assign text and value to Option object
		opt.text = agent.TSO_Area;
		opt.value = agent.TSO_Area;

		// Add an Option object to Drop Down List Box
		document.getElementById('selectAgent').options.add(opt);
	}
	
	
/*------------------add the PlantTypes to its dropdown---*/
		    var opt = document.createElement("option");        

		// Assign text and value to Option object
	opt.text = "all types";
	opt.value = "ALL";

		// Add an Option object to Drop Down List Box
	document.getElementById('selectPlantType').options.add(opt);
		
	var plant_Types = [];
	for (let agent of agents)
	{
		//console.log(agent.id + "power: " +agent.TotalMWNUTS2("DE30"));
		for(let plant of agent.plants)
		{
			if(!plant_Types.includes(plant.type))
			{
				plant_Types.push(plant.type);
				var opt1 = document.createElement("option");        

				// Assign text and value to Option object
				opt1.text = plant.type;
				opt1.value = plant.type;

				// Add an Option object to Drop Down List Box
				document.getElementById('selectPlantType').options.add(opt1);
			}

		}
	}
	//////////////////////////////////////////////////////////////
function TypeSelected()
{
	document.getElementById('selectPlantSubType').style.visibility = "visible";
	document.getElementById('selectPlantSubType').options.length = 0;
	var opt = document.createElement("option");        

	// Assign text and value to Option object
	opt.text = "all subtypes";
	opt.value = "ALL";

	// Add an Option object to Drop Down List Box
	document.getElementById('selectPlantSubType').options.add(opt);
	//document.getElementById('selectPlantType').clear();
	var plant_SubTypes = [];
	for (let agent of agents)
	{
		//console.log(agent.id + "power: " +agent.TotalMWNUTS2("DE30"));
		for(let plant of agent.plants)
		{
			
			if ((plant.type == document.getElementById('selectPlantType').value) && (plant.subtype != "") && (!plant_SubTypes.includes(plant.subtype)))
			{
				plant_SubTypes.push(plant.subtype);
			//	console.log(plant.subtype);
				var opt = document.createElement("option");        

				// Assign text and value to Option object
				opt.text = plant.subtype;
				opt.value = plant.subtype;
				// Add an Option object to Drop Down List Box
				document.getElementById('selectPlantSubType').options.add(opt);
			}
		}
	}	
}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	function GetPowerofPlantTypeInRegion(_PlantType,_region)//returns the amount of power a certain plant type has in a region
	{
		var TotalP = 0;
		//for (plant_Type of plant_Types)
		//{
			for (let agent of agents)
			{
				//console.log(agent.plants)
				for (let plant of agent.plants)
				{
					//console.log(plant.NUTS2);
					//console.log(_PlantType);
					//console.log(plant_Type);
					if((plant.NUTS2 == _region) && (plant.type == _PlantType)) 
					{
						TotalP += parseFloat(plant.powerMW);
					}
				}
			}
		//}
		return TotalP;
	}
/////////////////////////////////////////////////////////////
function GetMAXPowerofPlantTypeInRegion(_PlantType) 
{
	//console.log('b');
	var max = 0;
	for (let region of NUTS2_Regions)
	{
		//console.log(GetPowerofPlantTypeInRegion(_PlantType,region.name));
		var temp = GetPowerofPlantTypeInRegion(_PlantType,region.name);
		if (temp > max)
			max = temp;
	}
	//console.log(max);
	return max;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
	function GetPowerofPlantTypeAndAgentInRegion(_PlantType,_PlantSubType,_region,_AgentName)//returns the amount of power a certain plant type has in a region
	{
		var TotalP = 0;
		//for (plant_Type of plant_Types)
		//{
			for (let agent of agents)
			{
				
				if ((agent.TSO_Area == _AgentName) ||(_AgentName == "ALL"))
				{	for (let plant of agent.plants)
					{
						if((plant.NUTS2 == _region) && (plant.type == _PlantType) && ((plant.subtype == _PlantSubType) || (_PlantSubType == "ALL")) ) 
						{
							TotalP += parseFloat(plant.powerMW);
						}
					}
				}
			}
		//}
		return TotalP;
	}
/////////////////////////////////////////////////////////////
function GetMAXPowerofPlantTypeAndAgentInRegion(_PlantType,_PlantSubType,_AgentName) 
{
	//console.log(_AgentName);
	var max = 0;
	for (let region of NUTS2_Regions)
	{
		//console.log(GetPowerofPlantTypeInRegion(_PlantType,region.name));
		var temp = GetPowerofPlantTypeInRegion(_PlantType,region.name,_AgentName);
		if (temp > max)
			max = temp;
	}
	//console.log(max);
	return max;
}