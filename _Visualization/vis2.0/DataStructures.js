class NUTSregion
{
	
  constructor(_id,_name)
  {
    this.id = _id;
	this.name = _name;
  }
}

class NUTS2region//this class in kinda dumb, but every Agent in it does not neccerily own plants in this spesefic region. 
{
	constructor(_name,_agents)
	{

		this.name = _name;
		this.agents = _agents;
		this.powerMW = this.getPowerMWtot();
	}	
	
	getPowerMWtot()
	{
		//console.log(this.name);
		var powerMW =0;
		for (let agent of this.agents)
		{
			for (let plant of agent.plants)
			{
				if (plant.NUTS2 == this.name)
				{
					powerMW += parseFloat(plant.powerMW);
				}
			}
		}
		return powerMW;
	}
}


class Agent
{
	
	constructor(_id,_type,_TSO_Area)
	{
		this.id = _id;
		this.TSO_Area = _TSO_Area;
		this.type = _type;
		this.plants = [];
	}
	
	AddPLant(_plant)
	{
		this.plants.push(_plant);
		return this.plants;
	}
	
	
	TotalMW()
	{
		var total = 0;
		for (let plant of this.plants)
		{
			
				total+= parseFloat(plant.powerMW);
		}
		return total;
	}
	
	//returns the max power this agent has of all its regions
	MaxPowerInARegion()
	{
		var MaxP = 0;
		//console.log(NUTS2_Regions);
		for (let NUTS2_Region of NUTS2_Regions) 
		{
			//console.log(NUTS2_Region.name);
			var temp1 = parseFloat(this.TotalMWNUTS2(NUTS2_Region.name));
			if (temp1 > MaxP)
				MaxP = temp1;
			//for (let plant of this.plants)
			//{
			//	if (parseFloat(plant.powerMW) > MaxP)
			//		MaxP = parseFloat(plant.powerMW);
			//}
		}
		
		return MaxP;
	}
	//return the total power an agent has in a certain region
	TotalMWNUTS2(_region)
	{
		//console.log("a: " +_region);
		var total = 0;
		for (let plant of this.plants)
		{
			if (plant.NUTS2 == _region)
			{
				total+= parseFloat(plant.powerMW);
			}
		}
		return total;
	}
}
class Plant
{

	constructor(_type,_SubType,_NUTS2,_Count,_PowerMW)
	{
		this.type = _type;
		this.subtype = _SubType;
		this.NUTS2 = _NUTS2;
		this.count = _Count;
		this.powerMW = _PowerMW;
	}
}