function ChangePowerPerNuts(e,feature)
{
	//window.location.reload(true);
	_colours = [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
				'rgba(255, 159, 64, 1)'
            ];
	
/*--------------------------this is the code for the Power per NUTS2 region-------------*/	

	document.getElementById('headpowerpernuts').style.display = "block";
	document.getElementById('myChartPowerNuts').style.display = "block";
	
	//create the chart 
	
	//var ctx = document.getElementById('myChartPowerNuts');

	
	_labels1 = [];
	_data1 = [];

	for (let agent of agents)
	{
		//console.log(agent.id + "power: " +agent.TotalMWNUTS2("DE30"));
		_labels1.push(agent.TSO_Area + "->Power: " + agent.TotalMWNUTS2(feature.properties.id).toFixed()+"MW  ");
		_data1.push(agent.TotalMWNUTS2(feature.properties.id));
	}
	
RemoveZeroData(_labels1,_data1);


PowerPerNutsChart.data.labels = _labels1;
PowerPerNutsChart.data.datasets = [{
            data: _data1,
            backgroundColor: _colours,
            borderColor: _colours,
            borderWidth: 1
        }];
PowerPerNutsChart.update();



/*	
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: _labels1,
        datasets: [{
            data: _data1,
            backgroundColor: _colours,
            borderColor: _colours,
            borderWidth: 1
        }]
    }
});*/

/*--------------------------this is the code for the different techonolgies per NUTS2 region-------------*/	
	document.getElementById('headtechnologypernuts').style.display = "block";
	document.getElementById('myChartTechonolgyNuts').style.display = "block";
	
	//create the chart 
var ctx2 = document.getElementById('myChartTechonolgyNuts');

_labels2 = [];
_data2 = [];

//var fruits = ["Banana", "Orange", "Apple", "Mango"];
//var n = fruits.includes("Mango");
//console.log(n);

for (let agent of agents)
{
		//console.log(agent.id + "power: " +agent.TotalMWNUTS2("DE30"));
		for(let plant of agent.plants)
		{
			var PlantDescriptor = plant.type + "->"+ plant.subtype;
			if (!_labels2.includes(PlantDescriptor))
			{
				_labels2.push(PlantDescriptor);
				_data2.push(0);
			}
			if(plant.NUTS2 == feature.properties.id)		
				_data2[_labels2.indexOf(PlantDescriptor)] += parseFloat(plant.powerMW);
		}
}
	var i;
for (i=0;i<_labels2.length;i++)
{
	_labels2[i]=_labels2[i] + "->Power: " + _data2[i].toFixed(3)+'MW  ';
}
	
/*_labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];
_data = [123, 19, 3, 50, 2, 3];*/
RemoveZeroData(_labels2,_data2);
	
	PlantTypePerNutsChart.data.labels = _labels2;
	PlantTypePerNutsChart.data.datasets = [{
            data: _data2,
            backgroundColor: GetColoursFromPlantType(_labels2),
            borderColor: GetColoursFromPlantType(_labels2),
            borderWidth: 1
        }];
	PlantTypePerNutsChart.update();


}

function GetColoursFromPlantType(_labels)
{
	var colours = [];
	for (let label of _labels)
	{
		if(label.includes("bhkw"))
		{
			colours.push('rgba(87, 254, 40, 1)'); //green
		}
		else if(label.includes("Gas"))
		{
			//console.log(label);
			colours.push('rgba(117, 117, 117, 1)');//grey 
		}
		else if(label.includes("Oil")||label.includes("Coal"))
		{
			colours.push('rgba(0, 0, 0, 1)'); //black
		}
		else if(label.includes("Nuclear") || label.includes("pv_roof"))
		{
			colours.push('rgba(237, 216, 59, 1)'); //geel
		}
		else if(label.includes("Ignite"))
		{
			colours.push('rgba(181, 101, 29, 1)'); //brown
		}
		else if(label.includes("plant_hydro"))
		{
			colours.push('rgba(35, 255, 167, 1)'); //greenish blue
		}
		else if(label.includes("plant_wind"))
		{
			colours.push('rgba(0, 0, 255, 1)'); //dark blue
		}
		else if(label.includes("geoThermal"))
		{
			colours.push('rgba(255, 191, 0, 1)'); //oranja
		}
		else 
		{
			colours.push('rgba(255, 255, 132, 1)');
		}
	}
//console.log(colours);
	return colours;
}

function RemoveZeroData(_labels,_data)
{
	/*var i;
	for (i=0;i<_data.length;i++)
	{
		if(_data[i] == 0)
		{
			_data[i] = 999999999999999;
			//console.log("0 found");
		}
	}
	return _data;*/
	while (_data.indexOf(0) != -1)
	{
		console.log("d");
		_labels.splice(_data.indexOf(0), 1 );
		_data.splice(_data.indexOf(0), 1 );
		
	}

}