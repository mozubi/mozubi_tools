﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Geocoding.Microsoft;
using System.Xml.Linq;

namespace NUTS2xml
{
    class Program
    {
        static void Main(string[] args)
        {
            //string bingKey = "AptKTzecFvA-B3iIN8hxRRAwU_X8o63rD2NeqYCSNWDxWYu23PcsFmRFTGs2US20";
            string bingKey = "6dplfaxo9gFNLoAC86F7~n8kDnsPC2d9vSgQ2myB0Ew~AmHimCphFbiYZPMIn5gfz9VoyZ5McNjjiWYAZ0zh32TWtrSK8A7D2JLNQaPalHta";
            string filePath = $@"C:\Users\herr\source\repos\MozuBi_Tools\NUTS2xml\pc2018_de_NUTS-2016_v1.0.csv";
            string outputXml = $@"C:\Users\herr\source\repos\MozuBi_Tools\NUTS2xml\NUTS_DE_PLZ_COORDINATES.xml";
            string batchXml = $@"C:\Users\herr\source\repos\MozuBi_Tools\NUTS2xml\batch.xml";
            string outputCsv = $@"C:\Users\herr\source\repos\MozuBi_Tools\NUTS2xml\NUTS_DE_PLZ_COORDINATES.csv";
            BingMapsGeocoder bing = new BingMapsGeocoder(bingKey);
            bing.Culture = "de-de";

            List<(string nuts3, int plz)> nuts3plz = new List<(string nuts3, int plz)>();

            var file = File.ReadAllLines(filePath).ToList();
            file.RemoveAt(0);

            file.ForEach(str =>
            {
                var split = str.Replace("'", "").Split(';');

                nuts3plz.Add((split[0].ToString(), Convert.ToInt32(split[1])));
            });

            var NUTSComplete = new List<(string NUTS1, string NUTS2, string NUTS3, string Municipality, int Postcode, double Lat, double Long)>();


            #region Batch-GeoCodeAPI
            //List<XElement> elements = new List<XElement>();
            //XNamespace xNamespace = "http://schemas.microsoft.com/search/local/2010/5/geocode";
            //XElement rootbatch = new XElement(xNamespace + "GeocodeFeed", new XAttribute("xmlns", xNamespace.NamespaceName), new XAttribute("Version", "2.0"));

            //int id = 1;
            //foreach (var nut in nuts3plz)
            //{
            //    XElement el = new XElement(xNamespace + "GeocodeEntity", new XAttribute("Id", id), new XAttribute("xmlns", $"http://schemas.microsoft.com/search/local/2010/5/geocode"),
            //        new XElement("GeocodeRequest", new XAttribute("Culture", "de-de"),
            //            new XElement("Address", new XAttribute("PostalCode", nut.plz.ToString()), new XAttribute("CountryRegion", "Germany"))));
            //    elements.Add(el);
            //    id++;
            //}
            //rootbatch.Add(elements);

            //XDocument doc = new XDocument();
            //doc.Add(rootbatch);
            //doc.Save(batchXml);

            //GeoCoderBatchAPI.Downloader.Run(batchXml, "xml", bingKey);

            //GeoCoderBatchAPI.CreateJob(batchXml, "xml", bingKey, "plz");
            #endregion


            nuts3plz.ForEach(nut =>
             {
                 BingAddress location;

                 try
                 {
                     location = bing.Geocode("", "", "", nut.plz.ToString(), "Deutschland").First();
                     string nuts3 = nut.nuts3;
                     string nuts2 = nuts3.Remove(nuts3.Count() - 1);
                     string nuts1 = nuts3.Remove(nuts2.Count() - 1);
                     string muni = location.Locality;
                     int postcode = nut.plz;
                     double lat = location.Coordinates.Latitude;
                     double lon = location.Coordinates.Longitude;

                     NUTSComplete.Add((nuts1, nuts2, nuts3, muni, postcode, lat, lon));
                 }
                 catch (Exception)
                 {
                     Console.WriteLine("Pausing 60 seconds");
                     System.Threading.Thread.Sleep(60 * 1000);

                     location = bing.Geocode("", "", "", nut.plz.ToString(), "Deutschland").First();
                     string nuts3 = nut.nuts3;
                     string nuts2 = nuts3.Remove(nuts3.Count() - 1);
                     string nuts1 = nuts3.Remove(nuts2.Count() - 1);
                     string muni = location.Locality;
                     int postcode = nut.plz;
                     double lat = location.Coordinates.Latitude;
                     double lon = location.Coordinates.Longitude;

                     NUTSComplete.Add((nuts1, nuts2, nuts3, muni, postcode, lat, lon));
                 }


                 //if (NUTSComplete.Count > 100)
                 //state.Break();

                 if (NUTSComplete.Count % 100 == 0)
                     Console.WriteLine($"{((double)NUTSComplete.Count / (double)nuts3plz.Count) * 100} Percent");
             });

            NUTSComplete = NUTSComplete.OrderBy(el => el.NUTS1).ThenBy(el => el.NUTS2).ThenBy(el => el.NUTS3).ToList();

            #region Write XML
            XDocument xmlDoc = new XDocument();
            XElement root = new XElement("Root");

            NUTSComplete.ForEach(nut =>
            {
                root.Add(new XElement("NUTS",
                    new XAttribute("NUTS1", nut.NUTS1),
                    new XAttribute("NUTS2", nut.NUTS2),
                    new XAttribute("NUTS3", nut.NUTS3),
                    new XAttribute("Municipality", nut.Municipality),
                    new XAttribute("Postcode", nut.Postcode),
                    new XAttribute("Lat", nut.Lat),
                    new XAttribute("Lon", nut.Long)));
            });

            xmlDoc.Add(root);

            xmlDoc.Save(outputXml);
            #endregion

            #region Write CSV
            var NUTSCompleteCSV = new List<string>();
            NUTSCompleteCSV.Add("NUTS1;NUTS2;NUTS3;Municipality;Postcode;Latitude;Longitude;");
            NUTSComplete.ForEach(entry => NUTSCompleteCSV.Add($"{entry.NUTS1};{entry.NUTS2};{entry.NUTS3};{entry.Municipality};{entry.Postcode};{entry.Lat};{entry.Long};"));
            File.WriteAllLines(outputCsv, NUTSCompleteCSV, Encoding.GetEncoding(1252));
            #endregion


            Console.ReadKey();
        }
    }
}
