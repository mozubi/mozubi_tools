﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EEG_Asset_Aggregation.Data;

namespace EEG_Asset_Aggregation
{
    public static class Aggregator
    {
        public enum TSO { TransnetBW, Amprion, Hertz, TenneT };

        public static List<List<Plant>> GetAllPlantsByTSO(IPlantDataSource dataSource, string tsoSelection = "", string technologySelection ="")
        {
            List<List<Plant>> tsoPlants = new List<List<Plant>>();

            foreach (var tso in dataSource.GetAllTSO().Where(tso => tso.Contains(tsoSelection)))
            {
                foreach (var tech in dataSource.GetAllTechnologiesFromData().Where(tech => tech.Contains(technologySelection)))
                {
                    tsoPlants.Add(dataSource.GetPlants(tso, tech));
                }
            }

            tsoPlants.RemoveAll(el => el.Count == 0);
            return tsoPlants;
        }

        public static List<List<Plant>> AssignNuts2Plants(List<List<Plant>> tsoPlants, List<(string nuts2, List<int> postcodes)> nutsPostcodes)
        {
            Parallel.ForEach(tsoPlants, plantList =>
            {
                Parallel.ForEach(nutsPostcodes, combi =>
                {
                    combi.postcodes.ForEach(plz => plantList.Where(el => el.Postcode == plz).ToList().ForEach(el => el.NUTS2 = combi.nuts2));
                });
            });

            Parallel.ForEach(tsoPlants, lst =>
            {
                var sel = lst.Where(el => el.NUTS2 == "").ToList();
                
                if (sel.Count > 0)
                {
                    Console.WriteLine($"Assigning {sel.Count} undefined NUTS to Plants");
                    sel.ForEach(empty => empty.NUTS2 = XML.GetNearestNUTS2(empty.Postcode));
                }
            });

            return tsoPlants;
        }

        public static List<List<Plant>> AggregateByNUTS2(List<List<Plant>> tsoPlants, List<(string nuts2, List<int> postcodes)> nutsPostcodes)
        {
            List<List<Plant>> aggregate = new List<List<Plant>>();

            Parallel.ForEach(tsoPlants, plantList =>
            {
                List<Plant> singleAggregate = new List<Plant>();
                nutsPostcodes.ForEach(nut =>
                {
                    var plantSelection = plantList.Where(plant => plant.NUTS2 == nut.nuts2).Select(plant => plant.Power);
                    double sumPower = 0;

                    if (plantSelection.Count() > 0)
                        sumPower = plantSelection.Sum();

                    singleAggregate.Add(new Plant(nut.nuts2, 00000, plantList.First().TSO, plantList.First().Technology, sumPower));
                });
                aggregate.Add(singleAggregate);
            });
            aggregate.ForEach(lst => lst.RemoveAll(el => el.Power == 0));
            aggregate = aggregate.OrderBy(el => el.First().TSO).ThenBy(el => el.First().Technology).ToList();

            return aggregate;
        }
    }
}
