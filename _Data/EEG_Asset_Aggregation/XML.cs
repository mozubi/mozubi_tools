﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace EEG_Asset_Aggregation
{
    public static class XML
    {
        public static XDocument NUTSDoc = Load_Xml_File("EEG_Asset_Aggregation.Data.NUTS_DE_PLZ_COORDINATES.xml", System.Reflection.Assembly.GetExecutingAssembly());
        static string filename = @"C:\TEMP\UNB.xml";
        static string currentDirectory = Directory.GetCurrentDirectory();
        static string outPutFileName = Path.Combine(currentDirectory, filename);
        static int idCounter = 1;

        static XDocument xmlOutputDocument = new XDocument();

        public static void ResetXMLOutput()
        {
            xmlOutputDocument = new XDocument();
        }

        public static XDocument Load_Xml_File(string resourceName, Assembly assembly)
        {
            var stream = assembly.GetManifestResourceStream(resourceName);
            return XDocument.Load(stream);
        }


        public class Location
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public int Postcode { get; set; }
        }

        public static List<string> GetNUTS2()
        {
            var query = (from el in NUTSDoc.Descendants("NUTS")
                         select el.Attribute("NUTS2").Value
                         ).Distinct().ToList();
            return query;
        }

        public static string GetNearestNUTS2(int postcode2lookfor)
        {
            var postcodes = (from el in NUTSDoc.Descendants("NUTS")
                             select Convert.ToInt32(el.Attribute("Postcode").Value)
                         ).ToList();

            var diff = postcodes.Select(el => Math.Abs(el - postcode2lookfor)).ToList();

            int postcode = postcodes[diff.FindIndex(el => el == diff.Min())];


            return GetNUTSbyPostcode( postcode.ToString());
        }

        public static string GetNUTSbyPostcode( string postCode)
        {
            var query = (from el in NUTSDoc.Descendants("NUTS")
                         where el.Attribute("Postcode").Value.Equals(postCode)
                         select el.Attribute("NUTS2").Value
                         ).SingleOrDefault();
            return query;
        }

        public static List<int> GetPostcodes( string nuts2)
        {
            var query = NUTSDoc.Descendants("NUTS").Where(el => el.Attribute("NUTS2").Value.Equals(nuts2)).Select(el => Int32.Parse(el.Attribute("Postcode").Value)).ToList();

            return query;
        }

        internal static void WritePlantsToXML(List<List<Plant>> plantLists)
        {
            XElement root = new XElement("root");

            List<XElement> tsoPlantElements = new List<XElement>();
            for (int i = 0; i < plantLists.Count; i++)
            {
                foreach (var plant in plantLists[i])
                {
                    tsoPlantElements.Add(CreatePlantElement(plant));
                }

                bool addAgent;
                if (i < plantLists.Count - 1)
                {
                    addAgent = plantLists[i].First().TSO != plantLists[i + 1].First().TSO;
                }
                else
                {
                    addAgent = true;
                }

                if (addAgent)
                {
                    root.Add(GetAgentElement(plantLists[i].First().TSO, tsoPlantElements));
                    tsoPlantElements = new List<XElement>();
                }
            }

            SaveXMLOutput(root);
        }

        private static XElement GetAgentElement(string prevTso, List<XElement> tsoPlantElements)
        {
            XElement Agent = new XElement("Agent", new XAttribute("Id", idCounter), new XAttribute("Type", "UNB"), new XAttribute("TSO_Area", prevTso));
            idCounter++;
            XElement Portfolio = new XElement("Portfolio");

            foreach (var element in tsoPlantElements)
            {
                Portfolio.Add(element);
            }
            Agent.Add(Portfolio);

            return Agent;
        }

        private static void SaveXMLOutput(XElement root)
        {
            xmlOutputDocument.Add(root);
            xmlOutputDocument.Save(outPutFileName);
        }

        private static XElement CreatePlantElement(Plant plant)
        {
            string subType = "";
            string type = plant.GetType(out subType);
            XElement plantElement = new XElement("Plant",
                new XAttribute("Type", type),
                new XAttribute("SubType", subType),
                new XAttribute("NUTS2", plant.NUTS2),
                new XAttribute("Count", 1),
                new XAttribute("PowerMW", plant.Power));

            return plantElement;
        }
    }
}

