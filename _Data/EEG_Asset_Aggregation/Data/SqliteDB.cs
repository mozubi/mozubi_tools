﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using static EEG_Asset_Aggregation.Aggregator;

namespace EEG_Asset_Aggregation.Data
{
    public class SqliteDB : IPlantDataSource
    {
        string dbConn = @"Data Source=C:\TEMP\renewable_power_plants.sqlite";

        public List<string> TechnologyNames()
        {
            return new List<string>(){ "Run-of-river","Biomass and biogas" ,"Sewage and landfill gas"   ,"Onshore"  ,"Photovoltaics"    ,   "Other fossil fuels"    ,"Geothermal"   ,
            "Offshore"  ,"Photovoltaics ground" ,"Storage"  };
        }

        public List<string> GetAllTSO()
        {
            return new List<string>() { GetTSO(TSO.Amprion), GetTSO(TSO.Hertz), GetTSO(TSO.TenneT), GetTSO(TSO.TransnetBW) };
        }

        public List<string> GetAllTechnologiesFromData()
        {
            List<string> techs = new List<string>();
            using (SQLiteConnection cn = new SQLiteConnection(dbConn))
            {
                using (SQLiteCommand cmd = new SQLiteCommand("SELECT DISTINCT technology FROM [renewable_power_plants_DE]", cn))
                {
                    cmd.Connection.Open();
                    using (IDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            techs.Add(rdr["technology"].ToString());
                        }
                    }
                    cmd.Connection.Close();
                }
            }
            return techs;
        }

        public List<Plant> GetPlants(string tso, string tech)
        {
            List<Plant> plants = new List<Plant>();

            using (SQLiteConnection cn = new SQLiteConnection(dbConn))
            {

                // Query-String
                string qry = @"SELECT * FROM [data_weatherUTC] WHERE ROWID BETWEEN 1 AND 2";
                using (SQLiteCommand cmd = new SQLiteCommand($"SELECT * FROM [renewable_power_plants_DE] WHERE tso = '{tso}' AND technology ='{tech}'", cn))
                {
                    cmd.Connection.Open();
                    using (IDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            plants.Add(new Plant("", Convert.ToInt32(rdr["postcode"].ToString().Replace(".0", "")), rdr["tso"].ToString(), rdr["technology"].ToString(), Convert.ToDouble(rdr["electrical_capacity"])));
                        }
                    }

                    cmd.Connection.Close();
                }

                return plants;
            }
        }

        private static string GetTSO(TSO tso)
        {
            switch (tso)
            {
                case TSO.TransnetBW:
                    return "TransnetBW";
                case TSO.Amprion:
                    return "Amprion";
                case TSO.Hertz:
                    return "50Hertz";
                case TSO.TenneT:
                    return "TenneT";
                default:
                    return "";
            }
        }
    }
}
