﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EEG_Asset_Aggregation.Data
{
    public interface IPlantDataSource
    {
        List<string> GetAllTSO();

        List<string> GetAllTechnologiesFromData();

        List<string> TechnologyNames();

        List<Plant> GetPlants(string tso, string tech);

    }
}
