﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GenericParsing;
using System.Data;

namespace EEG_Asset_Aggregation.Data
{
    public class AnlStDaten : IPlantDataSource
    {
        List<string> files = new List<string>();
        List<(string TSO, DataTable Data)> DataSets = new List<(string TSO, DataTable Data)>();

        public AnlStDaten(string stammdatenFolderPath = @"C:\Users\herr\source\repos\MozuBi_Tools\EEG_Asset_Aggregation\Data\Anlagenstammdaten\")
        {
            Console.WriteLine("Creating AnlagenStammDaten");
            files = LoadFilePaths(stammdatenFolderPath).ToList();

            InitializeFiles();
            var plants = GetPlants("50Hertz", "Solar");
        }

        private void InitializeFiles()
        {
            Parallel.ForEach(files, file =>
            {
                var adapter = new GenericParserAdapter(file, Encoding.GetEncoding(1252));
                adapter.FirstRowHasHeader = true;
                adapter.ColumnDelimiter = ';';

                var dt = adapter.GetDataTable();
                string tso = dt.Rows[0].Field<string>("TSO");

                DataSets.Add((tso, dt));
            });
        }

        private IEnumerable<string> LoadFilePaths(string stammdatenFolderPath)
        {
            return Directory.EnumerateFiles(stammdatenFolderPath);
        }

        #region Interface-Members
        public List<string> GetAllTechnologiesFromData()
        {
            List<string> techs = new List<string>();
            foreach (var dataSet in this.DataSets)
            {
                techs.AddRange(dataSet.Data.AsEnumerable().Select(el => el.Field<string>("Energietraeger")).Distinct().ToList());
            }

            return techs.Distinct().ToList();
        }

        public List<string> GetAllTSO()
        {
            return this.DataSets.Select(el => el.TSO).ToList();
        }

        public List<Plant> GetPlants(string tso, string tech)
        {
            var dataSet = this.DataSets.Where(el => el.TSO == tso).Select(el => el.Data).Single();

            return dataSet.AsEnumerable().Where(el => el.Field<string>("Energietraeger") == tech).Where(el => !el.Field<string>("PLZ").Contains('-')).Select(el =>
                            new Plant(nUTS2: "", Convert.ToInt32(el.Field<string>("PLZ")), tso, tech, (Convert.ToDouble(el.Field<string>("Installierte_Leistung")) / 1000))).ToList();
        }

        public List<string> TechnologyNames()
        {
            throw new NotImplementedException();
        }

        public void ClearAllDataSets()
        {
            this.DataSets = new List<(string TSO, DataTable Data)>();
        }
        #endregion  
    }
}
