﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EEG_Asset_Aggregation
{
    public class Plant
    {
        public Plant(string nUTS2, int postcode, string tSO, string technology, double power)
        {
            this.NUTS2 = nUTS2;
            this.Postcode = postcode;
            this.TSO = tSO;
            this.Technology = technology;
            this.Power = power;
        }

        public string NUTS2 { get; set; }
        public int Postcode { get; set; }
        public string TSO { get; set; }
        public string Technology { get; set; }
        public double Power { get; set; }

        public string GetType(out string subtype)
        {
            switch (this.Technology)
            {
                case "Wasser":
                case "Run-of-river":
                    subtype = "";
                    return "plant_hydro";

                case "Biomasse":
                case "Biomass and biogas":
                    subtype = "bhkw";
                    return "plant_thermal";

                case "Klärgas":
                case "Deponiegas":
                case "Grubengas":
                case "Sewage and landfill gas":
                    subtype = "sewageLandfillGas";
                    return "plant_thermal";

                case "Wind an Land":
                case "Onshore":
                    subtype = "onshore";
                    return "plant_wind";

                case "Solar":
                case "Photovoltaics":
                    subtype = "pv_roof";
                    return "plant_pv";

                case "Other fossil fuels":
                    subtype = "otherFossilFuels";
                    return "plant_thermal";

                case "Geothermie":
                case "Geothermal":
                    subtype = "geoThermal";
                    return "plant_thermal";

                case "Wind auf See":
                case "Offshore":
                    subtype = "offshore";
                    return "plant_wind";

                case "Photovoltaics ground":
                    subtype = "pv_area";
                    return "plant_pv";

                case "Storage":
                    subtype = "storage";
                    return "plant_storage";

                default:
                    subtype = "";
                    return "";
            }
        }
    }
}
