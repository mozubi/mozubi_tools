﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EEG_Asset_Aggregation.Data;

namespace EEG_Asset_Aggregation
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Extract NUTS & Postcodes
            Console.WriteLine("Extracting NUTS-Regions");
            var nutsPostcodes = new List<(string nuts2, List<int> postcodes)>();
            XML.GetNUTS2().ForEach(nut => nutsPostcodes.Add((nut, XML.GetPostcodes(nut))));
            #endregion

            IPlantDataSource data = new AnlStDaten();
            //IDataSource sqliteDB = new SqliteDB();
            var techs = data.GetAllTechnologiesFromData().ToList();


            // Get all plants from DB for technology and tso
            Console.WriteLine("Getting all plants by TSO");
            List<List<Plant>> tsoPlants = Aggregator.GetAllPlantsByTSO(data);
            //anlSt.ClearAllDataSets();

            // Assign NUTS2-Region by postcode for each plant
            Console.WriteLine("Assigning NUTS-Regions to plants by postcode");
            tsoPlants = Aggregator.AssignNuts2Plants(tsoPlants, nutsPostcodes);

            // Aggregate plants for tso by nuts2-region in a single plant
            Console.WriteLine("Aggregating plants by NUTS-Regions");
            var aggregate = Aggregator.AggregateByNUTS2(tsoPlants, nutsPostcodes);

            XML.WritePlantsToXML(aggregate);

            Console.ReadKey();
        }


    }
}
