﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaStR_Processor;
using System.Xml.Linq;
using System.Threading;
using System.Windows.Threading;

namespace MaStR_Processor_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        ObservableCollection<string> Tables;
        ObservableCollection<string> DumpFiles = new ObservableCollection<string>();

        ObservableCollection<string> statusTextCollection;


        ListBox dragSource = null;
        ListBox dragSourceDump = null;

        MaStRDumpDataConverter MastrConverter = new MaStRDumpDataConverter();


        public MainWindow()
        {
            statusTextCollection = new ObservableCollection<string>();
            this.MastrConverter.UpdateStatus += UpdateStatusTextBox;

            InitializeComponent();

            this.Status.ItemsSource = statusTextCollection;

            DataContext = this;

            SetTables();

            LoadDefaultFolders(out string dumpFolderDefault, out string outputFolderDefault);
            if (dumpFolderDefault != "") this.FolderPath.Text = dumpFolderDefault;
            if (outputFolderDefault != "") this.OutputFolderPath.Text = outputFolderDefault;

        }

        private void LoadDefaultFolders(out string dumpFolderPath, out string outputFolderPath)
        {
            XDocument xDoc = System.Xml.Linq.XDocument.Load(Assembly.GetEntryAssembly().GetManifestResourceStream("MaStR_Processor_GUI.Defaults.xml"));

            dumpFolderPath = xDoc.Element("root").Element("DefaultDumpFolder").Attribute("Path").Value.ToString();
            outputFolderPath = xDoc.Element("root").Element("DefaultOutputFolder").Attribute("Path").Value.ToString();
        }

        private void SetDefaultDumpFolder(string dumpFolderPath)
        {
            XDocument xDoc = System.Xml.Linq.XDocument.Load(Assembly.GetEntryAssembly().GetManifestResourceStream("MaStR_Processor_GUI.Defaults.xml"));

            xDoc.Element("root").Element("DefaultDumpFolder").Attribute("Path").Value = dumpFolderPath;
        }

        private void SetDefaultOutputFolder(string outputFolderPath)
        {
            XDocument xDoc = System.Xml.Linq.XDocument.Load(Assembly.GetEntryAssembly().GetManifestResourceStream("MaStR_Processor_GUI.Defaults.xml"));

            xDoc.Element("root").Element("DefaultOutputFolder").Attribute("Path").Value = outputFolderPath;
        }

        private void SetTables()
        {
            Tables = new ObservableCollection<string>();
            foreach (var sheetName in Enum.GetValues(typeof(MaStRHelper.TableSheetNames)).Cast<MaStRHelper.TableSheetNames>().ToList())
            {
                Tables.Add(sheetName.ToString());
            }
            Lb_TableChoose.ItemsSource = Tables;
        }

        private void SetDumpfiles()
        {
            DumpFiles = new ObservableCollection<string>();

            try
            {
                foreach (var file in Directory.EnumerateFiles(FolderPath.Text).Where(el => el.Contains("xlsx")))
                {
                    DumpFiles.Add(file);
                }
                LB_DumpChoose.ItemsSource = DumpFiles;
            }
            catch (DirectoryNotFoundException)
            {
                if (!FolderPath.Text.Contains("Click search"))
                    MessageBox.Show("Error: MaStR-Dump-Directory not found!");
            }

        }

        #region Click-Events
        private void SearchClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.Description = "Set path to folder containing MaStR-Dumpfiles (.xlsx).";
                dialog.ShowNewFolderButton = false;
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                FolderPath.Text = dialog.SelectedPath.ToString();
            }

            SetDefaultDumpFolder(FolderPath.Text);
        }

        private void SearchClickOutput(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.Description = "Set desired path to folder for the output SQLite-Database.";
                dialog.ShowNewFolderButton = false;
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                OutputFolderPath.Text = dialog.SelectedPath.ToString();
            }
            SetDefaultOutputFolder(OutputFolderPath.Text);
        }
        #endregion

        #region Drag&Drop - Tables
        private void LB_TableSelection_Drop(object sender, DragEventArgs e)
        {
            ListBox parent = (ListBox)sender;
            string data = e.Data.GetData(typeof(string)).ToString();
            ((IList<string>)dragSource.ItemsSource).Remove(data);
            parent.Items.Add(data);
        }

        private void Lb_TableChoose_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListBox parent = (ListBox)sender;
            dragSource = parent;
            object data = GetDataFromListBox(dragSource, e.GetPosition(parent));

            if (data != null)
            {
                DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
            }
        }


        private static object GetDataFromListBox(ListBox source, Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = source.ItemContainerGenerator.ItemFromContainer(element);

                    if (data == DependencyProperty.UnsetValue)
                    {
                        element = VisualTreeHelper.GetParent(element) as UIElement;
                    }

                    if (element == source)
                    {
                        return null;
                    }
                }

                if (data != DependencyProperty.UnsetValue)
                {
                    return data;
                }
            }

            return null;
        }

        #endregion

        #region Drag&Drop - DumpFiles
        private void LB_DumpSelection_Drop(object sender, DragEventArgs e)
        {
            ListBox parent = (ListBox)sender;
            string data = e.Data.GetData(typeof(string)).ToString();
            ((IList<string>)dragSourceDump.ItemsSource).Remove(data);
            parent.Items.Add(data);
        }

        private void LB_DumpChoose_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListBox parent = (ListBox)sender;
            dragSourceDump = parent;
            object data = GetDataFromListBox(dragSourceDump, e.GetPosition(parent));

            if (data != null)
            {
                DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
            }
        }
        #endregion

        private void LB_Table_Reset_Click(object sender, RoutedEventArgs e)
        {
            LB_TableSelection.Items.Clear();
            SetTables();
        }

        private void ListBox_Dump_Reset_Click(object sender, RoutedEventArgs e)
        {
            LB_DumpSelection.Items.Clear();
            SetDumpfiles();
        }

        private void TB_Remove_Selection(object sender, RoutedEventArgs e)
        {
            if (LB_DumpChoose.SelectedItem != null)
                DumpFiles.Remove(LB_DumpChoose.SelectedItem.ToString());
            LB_DumpChoose.Items.Refresh();

            if (LB_DumpSelection.SelectedItem != null)
                DumpFiles.Remove(LB_DumpSelection.ToString());
            LB_DumpSelection.Items.Refresh();
        }

        private async void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            this.MastrConverter.Initialize(OutputFolderPath.Text, LB_DumpSelection.Items.Cast<string>().ToList());
            int startRow = 0;
            int rowCount = 25000;
            foreach (var tableId in LB_TableSelection.Items.Cast<string>().ToList())
            {
                statusTextCollection.Add($"Current Table: {tableId}, Time: {DateTime.Now.ToString()}");
                for (int i = 0; i < LB_DumpSelection.Items.Count; i++)
                {
                    statusTextCollection.Add($"Current Dumpfile: {LB_DumpSelection.Items.Cast<string>().ToList()[i]}, Time: {DateTime.Now.ToString()}");

                    if (i == 0)
                        SQLiteTableCreator.DropTable(MastrConverter.dbFile, tableId);

                    if (LB_DumpSelection.Items.Cast<string>().ToList()[i].Contains("MIGRATION") && tableId.Equals(MaStRHelper.TableSheetNames.Tabelle_MAK))
                        continue;

                    await Task.Run(() => RunDBOp(LB_DumpSelection.Items.Cast<string>().ToList()[i], tableId, startRow, rowCount));
                }
            }
            statusTextCollection.Add($"Complete., Time: {DateTime.Now.ToString()}");
        }

        private async Task RunDBOp(string xlFile, string tableId, int startRow, int rowCount)
        {
            this.MastrConverter.FilesToDatabase(xlFile, tableId, startRow, rowCount);
        }

        private void UpdateStatusTextBox(object sender, string status)
        {
            this.statusTextCollection.Add(status);
            Status.SelectedIndex = Status.Items.Count - 1;
            Status.ScrollIntoView(Status.SelectedItem);
        }

        private void DumpFolderChanged(object sender, TextChangedEventArgs e)
        {
            SetDumpfiles();
        }
    }
}
