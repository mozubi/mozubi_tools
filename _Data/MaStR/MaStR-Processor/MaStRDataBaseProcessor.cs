﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Data;

namespace MaStR_Processor
{
    public class MaStRDataBaseProcessor
    {
        string dbFile;
        public MaStRDataBaseProcessor(string dbFilePath = @"C:\TEMP\MaStR\mastrDb.sqlite")
        {
            this.dbFile = dbFilePath;

            var nonDistinct = (from dat in GetNonDistinct(MaStRHelper.TableSheetNames.Tabelle_ENH, enhType: MaStRHelper.ENH_Einheitentypen.Gasspeichereinheit).AsEnumerable()
                               group dat by dat.Table.Columns[0] into grouped
                               where grouped.Count() > 1
                               select grouped).ToList();
        }

        public DataTable GetNonDistinct(MaStRHelper.TableSheetNames table, MaStRHelper.ENH_Einheitentypen? enhType = null)
        {
            string whereClause="";
            if (enhType.HasValue)
            {
                whereClause = $"WHERE ENH_Einheitentyp LIKE '{MaStRHelper.GetENH_EinheitentypName((MaStRHelper.ENH_Einheitentypen)enhType)}'";
            }
            var query = $"SELECT ENH_MastrID,ENH_LetzteAktualisierung,ROWID FROM {MaStRHelper.GetTableName(table)} {whereClause} " + $"GROUP BY ENH_MastrID";

            return RunQuery(query);
        }

        public dynamic GetByMaStRId(MaStRHelper.TableSheetNames table, int mastrId)
        {
            string query = $"SELECT * FROM { MaStRHelper.GetTableName(table)} WHERE ENH_MastrID = '{mastrId}'";
            return RunQuery(query);
        }

        public DataTable RunQuery(string query)
        {
            DataTable dt = new DataTable();
            using (SQLiteConnection conn = new SQLiteConnection(GetConnString()))
            {
                try
                {
                    conn.Open();

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, conn);
                    adapter.Fill(dt);
                }
                finally
                {
                    conn.Close();
                }
            }
            return dt;
        }


        private string GetConnString()
        {
            return $"Data Source={dbFile}";
        }
    }
}
