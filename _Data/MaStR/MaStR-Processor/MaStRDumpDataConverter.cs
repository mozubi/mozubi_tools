﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SQLite;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;

namespace MaStR_Processor
{
    public class MaStRDumpDataConverter
    {

        public EventHandler<string> UpdateStatus;

        List<string> dumpTableSelection;

        public string dbFile { get; private set; }

        public MaStRDumpDataConverter()
        {
        }

        public void Initialize(string outputFolderPath, List<string> dumpTableSelection)
        {
            this.dumpTableSelection = new List<string>(dumpTableSelection);
            this.dbFile = $@"{outputFolderPath}\mastrDb.sqlite";

            UpdateStatus?.Invoke(this, "Dataconverter created");
        }
        public void FilesToDatabase(string xlFile, string tableId, int startRow, int rowCount)
        {
            DataTable table;

            if (!File.Exists(dbFile))
                SQLiteConnection.CreateFile(dbFile);

            SQLiteTableCreator creator = new SQLiteTableCreator(new SQLiteConnection($"Data Source={dbFile}"));
            
            do
            {
                table = ConvertExcelToDataTable(xlFile, tableId, startRow, rowCount);
                creator.CreateFromDataTable(table);
                InsertDataTable(dbFile, table);
                startRow += rowCount;
            } while (table.Rows.Count > 0);
        }
        
        public static DataTable ConvertExcelToDataTable(string FileName, string tableName, int fromRow, int rowCount)
        {
            DataTable dtResult = null;
            int totalSheet = 7; //No of sheets on excel file  
            using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + tableName + "$]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, fromRow, rowCount, tableName);
                dtResult = ds.Tables[tableName];
                objConn.Close();
                return dtResult; //Returning DataTable  
            }
        }

        private void InsertDataTable(string dbFile, DataTable dt)
        {
            using (var conn = new SQLiteConnection($"Data Source={dbFile}"))
            {
                SQLiteBulkInsert bulkInsert = new SQLiteBulkInsert(conn, dt.TableName);
                bulkInsert.CommitMax = (uint)dt.Rows.Count;

                foreach (DataColumn column in dt.Columns)
                {

                    bulkInsert.AddParameter(column.ColumnName, SqlHelper.GetDbType(column.DataType));
                }


                conn.Open();

                foreach (DataRow row in dt.Rows)
                {
                    bulkInsert.Insert(row.ItemArray);
                }

                conn.Close();
            }
        }
        
        public static void ResetDataBase(string dbFile)
        {
            SQLiteConnection.CreateFile(dbFile);
        }

    }
}
