﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaStR_Processor
{
    public static class MaStRHelper
    {
        public enum TableSheetNames { Tabelle_IDX, Tabelle_ENH, Tabelle_EEG, Tabelle_KWK, Tabelle_SGE, Tabelle_MAK };

        public static string GetTableName(TableSheetNames table)
        {
            switch (table)
            {
                case TableSheetNames.Tabelle_IDX:
                    return "Tabelle_IDX";
                case TableSheetNames.Tabelle_ENH:
                    return "Tabelle_ENH";
                case TableSheetNames.Tabelle_EEG:
                    return "Tabelle_EEG";
                case TableSheetNames.Tabelle_KWK:
                    return "Tabelle_KWK";
                case TableSheetNames.Tabelle_SGE:
                    return "Tabelle_SGE";
                case TableSheetNames.Tabelle_MAK:
                    return "Tabelle_MAK";
                default:
                    return "";
            }
        }


        public enum ENH_Einheitentypen
        {
            Solareinheit, Verbrennung, Windeinheit, Stromspeichereinheit, Biomasse, Wasser, Stromverbrauchseinheit,
            Gasspeichereinheit, Gasverbrauchseinheit, Geothermie, Gaserzeugungseinheit, Kernenergie
        }

        public static string GetENH_EinheitentypName(ENH_Einheitentypen typ)
        {
            return Enum.GetName(typeof(ENH_Einheitentypen), typ);
        }
    }
}
