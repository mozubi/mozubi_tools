﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaStR_Processor.MaStrClasses
{
    public static class MaStRClassHelper
    {
        public static AMaStrClass GetMaStRObjectByTableName(string tableName, object[] itemArray)
        {
            switch (tableName)
            {
                case "Tabelle_IDX":
                    return new IDX(itemArray);
                case "Tabelle_ENH":
                    return new ENH(itemArray);
                case "Tabelle_EEG":
                case "Tabelle_KWK":
                case "Tabelle_SGE":
                case "Tabelle_MAK":
                default:
                    return null;
            }
        }

        public static int GetMaStRClassFieldCount(string tableName)
        {
            switch (tableName)
            {
                case "Tabelle_IDX":
                    return typeof(IDX).GetFields().Length;
                case "Tabelle_ENH":
                    return typeof(ENH).GetFields().Length;
                case "Tabelle_EEG":
                case "Tabelle_KWK":
                case "Tabelle_SGE":
                case "Tabelle_MAK":
                default:
                    return 0;
            }
        }
    }
}
