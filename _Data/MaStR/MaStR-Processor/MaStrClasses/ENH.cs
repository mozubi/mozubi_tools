﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace MaStR_Processor.MaStrClasses
{
    [Table("Tabelle_ENH")]
    public class ENH : AMaStrClass
    {
        public string ENH_MastrID, ENH_MastrNummer, ENH_Systemstatus, ENH_Betriebsstatus, ENH_EinheitenTyp, ENH_NameEinheit, ENH_NameWindpark, ENH_NameKraftwerk, ENH_NameKraftwerksblock,
            ENH_VoruebergehendeStilllegungBeginn, ENH_WiederInbetriebnahmeDatum, ENH_GeplantesInbetriebnahmeDatum, ENH_InbetriebnahmeDatum, ENH_EndgueltigeStilllegungDatum, ENH_Meldedatum,
            ENH_LetzteAktualisierung, ENH_Land, ENH_Lage, ENH_Plz, ENH_Ort, ENH_Standortangabe, ENH_Strasse, ENH_Hausnummer, ENH_Adresszusatz, ENH_Gemarkung, ENH_Flurstueck,
            ENH_Bundesland, ENH_Landkreis, ENH_Gemeinde, ENH_Gemeindeschluessel, ENH_Seelage, ENH_ClusterOstsee, ENH_ClusterNordsee, ENH_Kuestenentfernung, ENH_Wassertiefe, ENH_Breitengrad,
            ENH_Laengengrad, ENH_AnzahlModule, ENH_Bruttoleistung, ENH_Nettonennleistung, ENH_Wechselrichterleistung, ENH_Wechselrichter, ENH_Leistungsbegrenzung,
            ENH_IsGrenzkraftwerk, ENH_NettonennleistungDeutschland, ENH_IsKombibetrieb, ENH_NettoleistungSteigerungDurchKombibetrieb, ENH_KombibetriebMastrNummer, ENH_SolarLage,
            ENH_Nutzungsbereich, ENH_EinheitlicheAusrichtungUndNeigungswinkel, ENH_HauptAusrichtung, ENH_HauptNeigungswinkel, ENH_NebenAusrichtung, ENH_NebenNeigungswinkel, ENH_InAnspruchgenommeneFlaeche,
            ENH_ArtDerFlaeche, ENH_InAnspruchgenommeneAckerflaeche, ENH_Technologie, ENH_Nabenhoehe, ENH_Rotordurchmesser, ENH_AuflagenZuAbschaltungBzwLeistungsgrenzen, ENH_Hersteller, ENH_Typenbezeichner,
            ENH_Energietraeger, ENH_Hauptbrennstoff, ENH_HauptbrennstoffWeiterer, ENH_NetzreserveDatum, ENH_SicherheitsbereitschaftDatum, ENH_Baubeginn, ENH_Biomasseart, ENH_ArtDerWasserkraftanlage,
            ENH_ZuflussArt, ENH_CanStromerzeugungMindern, ENH_MarktpartnerNummer, ENH_IsFernsteuerbarNetzbetreiber, ENH_IsFernsteuerbarDirektvermarkter, ENH_IsFernsteuerbarDritte,
            ENH_IsAngeschlossenAnHoechstOderHochspannungsnetz, ENH_EinspeisungsArt, ENH_AnteiligNutzungsberechtigte, ENH_Einsatzort, ENH_MaStRNummerNetzbetreiber, ENH_IsStilllegungNachParagraph13b,
            ENH_ArtDerStilllegungNachParagraph13b, ENH_BeginnDerStilllegungNachParagraph13b, ENH_EndeDerStilllegungNachParagraph13b, ENH_WCode, ENH_WCodeDisplayname, ENH_Kraftwerksnummer,
            ENH_ArtAbschaltbareLast, ENH_AnteilBeeinflussbarerLast, ENH_AnzahlAngeschlossenerStromverbrauchseinheitenUeber50MegaWatt, ENH_IsPraequalifiziertAbschaltbareLast, ENH_Erzeugungsleistung,
            ENH_IsStromerzeugung, ENH_MastrNummernDerZugehoerigenStromerzeugungseinheiten, ENH_MaximaleGasbezugsleistung, ENH_Kopplung, ENH_Batterietechnologie, ENH_LeistungsaufnahmePumpbetrieb,
            ENH_IsRegelbetriebPumpbetrieb, ENH_IsNotstromaggregat, ENH_NBP_Pruefungsdatum, ENH_NBP_Netzbetreiberpruefungsstatus;

        public ENH(object[] itemArray)
        {
            var fields = this.GetType().GetFields();

            for (int i = 0; i < fields.Length; i++)
            {
                fields[i].SetValue(this, itemArray[i].ToString());
            }
        }

        public override string TableName()
        {
            return "Tabelle_ENH";
        }
    }
}
