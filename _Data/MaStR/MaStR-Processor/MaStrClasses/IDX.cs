﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;

namespace MaStR_Processor.MaStrClasses
{
    [Table("Tabelle_IDX")]
    public class IDX : AMaStrClass
    {
        public string IDX_ENH_MastrID, IDX_EEG_MastrID, IDX_SGE_MastrID, IDX_MAK_MastrID, IDX_KWK_MastrID;

        public IDX(object[] itemArray)
        {
            IDX_ENH_MastrID = itemArray[0].ToString();
            IDX_EEG_MastrID = itemArray[1].ToString();
            IDX_SGE_MastrID = itemArray[2].ToString();
            IDX_MAK_MastrID = itemArray[3].ToString();
            IDX_KWK_MastrID = itemArray[4].ToString();

        }

        public override string TableName()
        {
            return "Tabelle_IDX";
        }

    }
}
