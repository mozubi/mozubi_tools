import datetime
from os import error
from sqlite3.dbapi2 import Error
from sys import prefix
import warnings
from numpy.lib.shape_base import expand_dims
import pandas as pd
import numpy as np
import math
import time
#from sqlalchemy.sql.expression import true
import test
from datetime import timedelta

class PVPlantOpti(object):
    def CalcEtaWR(LoadFactor):
        #Koeffizienten zur Ermittlung de WR-Wirkungsgrades (nach Macedo und Zilles 2007 S.344)
        A,  B,  C = (0.0079, 0.0411, 0.05)

        etaWR = (LoadFactor - (A + B * LoadFactor + C * LoadFactor**2)) / LoadFactor

        return etaWR
    def GetSolar(dfProduction, start, end):
        import sqlite3
        cnx = sqlite3.connect('C:\\Users\\studt\\source\\repos\\program\\winMoz\\winMoz\\Data\\Files\\SQLITE\\mzdb_data.db')
        sqlstring = 'select * from data_weather where utc_timestamp >= "' + start.strftime('%Y-%m-%dT%H:%M:%SZ') + '" and utc_timestamp <= "' + end.strftime('%Y-%m-%dT%H:%M:%SZ') + '"'
        df = pd.read_sql_query(sqlstring,cnx )
        de_cols = ["utc_timestamp"] + [col for col in df.columns if "DE" in col and "wind" not in col]
        df = df.loc[:, de_cols]
        df["utc_timestamp"] = pd.to_datetime(df["utc_timestamp"])
        df.set_index("utc_timestamp", inplace = True)
        # make sure indices are matched
        #df = df.loc[dfProduction.index[0]:]
        df = df.tz_localize(None)
        df = df.loc[~df.index.duplicated()]
        dfProduction = dfProduction.loc[~dfProduction.index.duplicated()]
        df = df.join(dfProduction, how = "inner").drop(dfProduction.columns, axis = 1)
        dfProduction = df.join(dfProduction, how = "inner").drop(df.columns, axis = 1)
        assert(len(dfProduction) == len(df))

        # nuts2 split
        nuts2list  = list(set([col.split("_",1)[0] for col in df.columns]))
        #nuts2list.remove("DE")
        dfs = dict([(nuts, df.filter(like = nuts)) for nuts in nuts2list])
        for df1 in dfs:
            dfs[df1].columns = dfs[df1].columns.str.strip(df1+"_")
            dfs[df1].rename(mapper= {"temperature": "T_env", "radiation_direct_horizontal": "RadDirHor", "radiation_diffuse_horizontal": "RadDiffHor"}, axis = 1, inplace= True)
        return  dfProduction, dfs
    def GetSolar2(dfProduction, start, end):
        import sqlite3
        cnx = sqlite3.connect('C:\\Users\\studt\\source\\repos\\mozubi_tools\\_Data\\PV_Aggregation\\weather_data.sqlite')
        sqlstring = 'select * from weather_data where utc_timestamp >= "' + start.strftime('%Y-%m-%dT%H:%M:%SZ') + '" and utc_timestamp <= "' + end.strftime('%Y-%m-%dT%H:%M:%SZ') + '"'
        df = pd.read_sql_query(sqlstring,cnx )
        de_cols = ["utc_timestamp"] + [col for col in df.columns if "DE" in col and "wind" not in col]
        df = df.loc[:, de_cols]
        df["utc_timestamp"] = pd.to_datetime(df["utc_timestamp"])
        from datetime import timedelta
        #df["utc_timestamp"] = df["utc_timestamp"] + timedelta(hours= 1)
        df.set_index("utc_timestamp", inplace = True)
        # make sure indices are matched
        #df = df.loc[dfProduction.index[0]:]
        df = df.tz_localize(None)
        df = df.loc[~df.index.duplicated()]
        dfProduction = dfProduction.loc[~dfProduction.index.duplicated()]
        df = df.join(dfProduction, how = "inner").drop(dfProduction.columns, axis = 1)
        dfProduction = df.join(dfProduction, how = "inner").drop(df.columns, axis = 1)
        assert(len(dfProduction) == len(df))

        # nuts2 split
        nuts2list  = list(set([col.split("_",1)[0] for col in df.columns]))
        #nuts2list.remove("DE")
        dfs = dict([(nuts, df.filter(like = nuts)) for nuts in nuts2list])
        for df1 in dfs:
            dfs[df1].columns = dfs[df1].columns.str.strip(df1+"_")
            dfs[df1].rename(mapper= {"temperature": "T_env", "radiation_direct_horizontal": "RadDirHor", "radiation_diffuse_horizontal": "RadDiffHor"}, axis = 1, inplace= True)
        return  dfProduction, dfs
    def getPVPlants():
        df = pd.read_csv("C:\\Users\\studt\\source\\repos\\mozubi_tools\\_Data\\PV_Aggregation\\20210602 PV Cluster NUts2.csv", skiprows=2, sep = ";", usecols=[1,2,3,4,5], names = ["NUTS2", "Winkel",  "Ost_MW", "Sued_MW", "West_MW"])
        dfLocations = pd.read_csv("C:\\Users\\studt\\source\\repos\\mozubi_tools\\_Data\\PV_Aggregation\\Locations.csv", decimal = ',')
        dfLocations.drop(["NUTS1", "NUTS3", "Municipality", "Postcode"], axis = 1, inplace = True)
        dfLocations = dfLocations.groupby("NUTS2").mean()
        df.drop("Winkel", axis = 1, inplace= True)
        df = df.groupby("NUTS2").sum()
        totalsum = df.loc[:,["Ost_MW", "Sued_MW", "West_MW"]].sum().sum()
        df.loc[:,["Ost_MW", "Sued_MW", "West_MW"]] = df.loc[:,["Ost_MW", "Sued_MW", "West_MW"]] / totalsum
        df = df.loc[df["Ost_MW"]+ df["Sued_MW"] + df["West_MW"] > 0.01]
        df = df.join(dfLocations)
        return df
    def getSolarProduction():
        def option1():
            import sqlite3
            cnx = sqlite3.connect('C:\\Users\\studt\\source\\repos\\program\\winMoz\\winMoz\\Data\\Files\\SQLITE\\mzdb_prognosedaten.db')
            df = pd.read_sql_query('select TimeStamp, PV_Ante, PV_Post from RE',cnx)
            df["TimeStamp"] = pd.to_datetime(df["TimeStamp"])
            df.set_index("TimeStamp", inplace = True)
            df = df.resample("1H").sum()
            return df[["PV_Ante"]]
        df = option1()
        def option2():
            from datetime import datetime
            dateparse = lambda x,y: datetime.strptime(x + y, '%d.%m.%Y%H:%M')
            df = pd.read_csv('C:\\Users\\studt\\source\\repos\\mozubi_tools\\_Data\\PV_Aggregation\\20210607 nomierte Zeitreiehen.csv', sep = ";", decimal= ",", skiprows=5, parse_dates= {"TimeStamp": [0,1]}, date_parser= dateparse)
            df.set_index("TimeStamp", inplace = True)
            df.drop(["Photovoltaik[MWh]", "Relative Produktion"], inplace = True, axis = 1)
            df.rename(mapper = {"Normierte Produktion KW": "PV_Ante"}, axis = 1, inplace = True)
            return df
        df = option2()
        df= df.loc[~df.index.duplicated()]
        #df = df.loc[df.PV_Ante > 0.0]
        return df
    def CalcSolarAngle(time_index, Longitude, Latitude):
        '''
        nach DIN 5034-2 wie beschrieben in Regenerative Energiesysteme
        time_index: In UTC Time
        '''
        longArr = Longitude.values.reshape((1,-1)).astype(np.float64)
        latArr = Latitude.values.reshape((1,-1)).astype(np.float64)
        #
        J = 360 * time_index.dayofyear.values.reshape((-1,1)) / 365
        # Deklination der Sonne
        #delta = np.rad2deg(np.arcsin(np.sin(np.deg2rad(epsilon)) * np.sin(np.deg2rad(eklLength))))
        delta = 0.3948 - 23.25559 * np.cos(np.deg2rad(J + 9.1)) - 0.3915 * np.cos(np.deg2rad(2 * J + 5.4)) - 0.1764 * np.cos(np.deg2rad(3 * J + 26))
        delta = np.deg2rad(delta)
        
        # Zeitgleichung
        Zgl = 0.0066 + 7.3525 * np.cos(np.deg2rad(J + 85.9)) + 9.9359 * np.cos(np.deg2rad(2 * J + 108.9)) + 0.3387 * np.cos(np.deg2rad(3 * J + 105.2))
        # Mittlere Ortszeit (= Wahre Ortszeit)
        MOZ = time_index.hour.values.reshape((-1,1)) + 1 - (4 * (15 - longArr)) / 60
        WOZ = MOZ + Zgl /60

        # Berücksichtigung Sommerzeit/Winterzeit
        #if (time.IsDaylightSavingTime()):
        #    MOZ = time.hour - 2 + (4 * Longitude) / 60

        #double WOZ = MOZ + ZGL // hier : WOZ = MOZ 
        beta = np.deg2rad((12.0 - WOZ) * 15)
        
        # Sonnenhöhenwinkel ohne Berücksichtigung der Refraktion
        h = np.arcsin(np.cos(beta) * np.cos(np.deg2rad(latArr))* np.cos(delta)+ np.sin(np.deg2rad(latArr))* np.sin(delta))
       
        azimut = 0

        # Berechnung Sonnenazimut für Uhrzeit vor und nach Zenitzeit
        azimut = np.where(WOZ <=12.0,  180 - np.rad2deg(np.arccos(
                    ((np.sin(h) * np.sin(np.deg2rad(latArr)) - np.sin(delta)) /
                    (np.cos(h) * np.cos(np.deg2rad(latArr)))))), 
                        180 + np.rad2deg(np.arccos(
                    ((np.sin(h) * np.sin(np.deg2rad(latArr)) - np.sin(delta)) /
                    (np.cos(h) * np.cos(np.deg2rad(latArr)))))) 
                    )
        multiColumn = pd.MultiIndex.from_product([["sun_alt", "sun_azimuth"], range(0, len(Longitude))])
        return pd.DataFrame(np.hstack([h, np.deg2rad(azimut)]), index = time_index, columns = multiColumn)
    def CalcShadowing(solarAngle):
        return np.where(solarAngle > 17, 1, np.maximum(0.7, 0.7 + 0.3 * (solarAngle / 17)))

    def GetTcoeff():
        #if (SubType == SubTypes.PV_ROOF)
        #        return 0.036;
        return 0.02
        #TemperaturCoeff 

            #Wenn PV-Typ nicht definiert
            #throw new ArgumentException("Wrong PV-Type");

    def CalcEtaMod(Gmod, Tmod):
        # Koeffizienten zur Ermittlung des rel. Modulwirkungsgrades nach Huld 2010 (S.329) -> sa. Schubert 2012
        A, B,  C, D, E, F  = (-0.017162, -0.040289, -0.004681, 0.000148, 0.000169, 0.000005)

        
        #Wenn Globalstrahlung == 0 dann überspringe Berechnung
        etaMod = np.where(Gmod == 0, 0,  1 + A * np.log((Gmod / 1000)) + B * np.power(np.log((Gmod / 1000)), 2) + (C + D * np.log((Gmod / 1000)) + E * np.power(np.log((Gmod / 1000)), 2))*(Tmod - 25)+ F * np.power((Tmod - 25), 2))
        
        return np.maximum(0, etaMod)
    def getPVfromSolarSimple(x, pvPlant, SolarWeatherNUTS ):
        # x[0] = ModuleAngle
        # x[1] = PowerMW
        # x[2] = LoadFactor
        ModuleAngleRad = np.deg2rad(x[0])
        eta = x[2]

        tModule = None# Modultemperatur
        gModule = None# Globalstrahlung auf Modul
   
   
        #Berechnung der Sonnenstandsdaten (Höhe, Azimut) zum Zeitpunkt t
        df = PVPlantOpti.CalcSolarAngle(SolarWeatherNUTS.index, pvPlant["Longitude"], pvPlant["Latitude"])
        ################
        dni = (SolarWeatherNUTS.RadDirHor * (df["duration"] / 60)) / np.cos(df["sun_zenith"])
        #gammaSolar = np.rad2deg(df["sun_alt"])
        #Azimut = np.rad2deg(df["sun_azimuth"])
        def _incidence_fixed(sun_alt, tilt, azimuth, sun_azimuth):
            """Returns incidence angle for a fixed panel"""
            return np.arccos(
            np.sin(sun_alt) * np.cos(tilt)
            + np.cos(sun_alt) * np.sin(tilt) * np.cos(azimuth - sun_azimuth)
            )
        #albedo = 0.3
        ModuleAzimuteRad = np.deg2rad(pvPlant["ModuleAzimute"])
        incidence = _incidence_fixed( df["sun_alt"], ModuleAngleRad, ModuleAzimuteRad, df["sun_azimuth"])
        plane_direct = (dni * np.cos(incidence)).fillna(0).clip(lower=0)
        #plane_diffuse = (
        #  SolarWeatherNUTS.RadDiffHor * ((1 + np.cos(ModuleAngleRad)) / 2)
        #  + albedo * (SolarWeatherNUTS.RadDirHor + SolarWeatherNUTS.RadDiffHor) * ((1 - np.cos(ModuleAngleRad)) / 2)
        #).fillna(0)
        #################
        #Berechnung der Strahlungsanteile (Reflektion, Diffuse- und Direkte Strahlung) zum Zeitpunkt
        enReflModule = 0.5 * 0.2 * (SolarWeatherNUTS.RadDiffHor + SolarWeatherNUTS.RadDirHor) * (1 - np.cos(ModuleAngleRad)); #0.2 = Albedo
        enDiffModule = SolarWeatherNUTS.RadDiffHor * 0.5 * (1 + np.cos(ModuleAngleRad))
        enDirModule = np.maximum(0.9, np.cos(df["sun_azimuth"] - np.deg2rad(pvPlant["ModuleAzimute"]))) * SolarWeatherNUTS.RadDirHor

        enDirModule = plane_direct
        #Berechnung Globalstrahlung und Modultemperatur
        gModule = enDirModule * PVPlantOpti.CalcShadowing(df["sun_alt"]) + enDiffModule + enReflModule
        tModule = SolarWeatherNUTS.T_env + PVPlantOpti.GetTcoeff() * gModule

        #Berechnung Modulwirkungsgrad auf Basis der Globalstrahlung und Modultemperatur
        etaMod = PVPlantOpti.CalcEtaMod(gModule, tModule)

        #Schreiben des aktuellen Gesamt-Anlagenwirkungsgrades zur späteren Berechnung der Stromgestehungskosten
        Efficiency = eta * etaMod 

        #Berechnung der Erzeugungsleistung zum Zeitpunkt auf Basis der Einstrahlung und des Wirkungsgrades
        power = np.maximum(0, Efficiency * ((gModule * x[1]) / 1000))
        return np.minimum(power, x[1])
    def getForAllNuts(pvPlant, weather, angles = None):
        nuts2zones = set(pvPlant["NUTS2"])
        power = 0
        for nuts2 in nuts2zones:
            plantIdx = [('sun_alt', val) for val in pvPlant.loc[pvPlant["NUTS2"]== nuts2].index.values] + [('sun_azimuth', val) for val in pvPlant.loc[pvPlant["NUTS2"]== nuts2].index.values]
            power = power + PVPlantOpti.getPVfromSolarOnePlantAndNUTS(pvPlant.loc[pvPlant["NUTS2"]== nuts2], weather[nuts2], angles[plantIdx]).sum(axis=1)
        return power
    def getPVfromSolarOnePlantAndNUTS( pvPlant, SolarWeatherNUTS, df = None ):
        def _incidence_fixed(sun_alt, tilt, azimuth, sun_azimuth):
            """Returns incidence angle for a fixed panel"""
            return np.arccos(
            np.sin(sun_alt) * np.cos(tilt)
            + np.cos(sun_alt) * np.sin(tilt) * np.cos(azimuth - sun_azimuth)
            )
        # x[0] = ModuleAngle
        # x[1] = PowerMW
        # x[2] = LoadFactor
        ModuleAngleRad = np.deg2rad(pvPlant["ModuleAngle"].values).reshape((1,-1))
        ModuleAzimuthRad = np.deg2rad(pvPlant["ModuleAzimute"].values.astype(np.float64)).reshape((1,-1))
        RadDiffHor = SolarWeatherNUTS.RadDiffHor.values.reshape((-1,1))
        RadDirHor = SolarWeatherNUTS.RadDirHor.values.reshape((-1,1))
        T_env = SolarWeatherNUTS.T_env.values.reshape((-1,1))

        LoadFac =  pvPlant["Load_Factor"].values.reshape((1,-1))
        tModule = None# Modultemperatur
        gModule = None# Globalstrahlung auf Modul
        etaWR = PVPlantOpti.CalcEtaWR(LoadFactor = LoadFac) # Wechselrichterwirkungsgrad
   
   
        # Zusätzliche Systemverluste
        # [1]-Mismatching (in Modulwinkelstreuung bereits enthalten), [2]-Reflexionen, [3]-Verschmutzungen, [4]-Gleichstromverkabelung
        etaSys = 1 - (0.00 + 0.025 + 0.02 + 0.002)
        # Wetterdaten für den Folgetag -> werden bei Methodenaufruf und Listengenerierung direkt aus der Datenbank geladen

        idx = 0
        #Berechnung der Sonnenstandsdaten (Höhe, Azimut) zum Zeitpunkt t
        if df is None:
            df = PVPlantOpti.CalcSolarAngle(SolarWeatherNUTS.index, pvPlant["Longitude"], pvPlant["Latitude"])
        #################
        #import test
        #df = test.sun_angles(SolarWeatherNUTS.index, (pvPlant["Latitude"], pvPlant["Longitude"]))
        #dni = (SolarWeatherNUTS.RadDirHor * (df["duration"] / 60)) / np.cos(df["sun_zenith"])
        #gammaSolar = np.rad2deg(df["sun_alt"])
        #Azimut = np.rad2deg(df["sun_azimuth"])
        #def _incidence_fixed(sun_alt, tilt, azimuth, sun_azimuth):
        #    """Returns incidence angle for a fixed panel"""
        #    return np.arccos(
        #    np.sin(sun_alt) * np.cos(tilt)
        #    + np.cos(sun_alt) * np.sin(tilt) * np.cos(azimuth - sun_azimuth)
        #    )
        #albedo = 0.3
        #ModuleAzimuteRad = np.deg2rad(pvPlant["ModuleAzimute"])
        incidence = _incidence_fixed( df["sun_alt"], ModuleAngleRad, ModuleAzimuthRad, df["sun_azimuth"])
        plane_direct = (SolarWeatherNUTS.RadDirHor.values.reshape((-1,1)) * np.cos(incidence)).fillna(0).clip(lower=0)
        plane_diffuse = (
          SolarWeatherNUTS.RadDiffHor.values.reshape((-1,1)) * ((1 + np.cos(ModuleAngleRad)) / 2)
          + 0.2 * (SolarWeatherNUTS.RadDirHor.values.reshape((-1,1)) + SolarWeatherNUTS.RadDiffHor.values.reshape((-1,1))) * ((1 - np.cos(ModuleAngleRad)) / 2)
        )
        #################
        #Berechnung der Strahlungsanteile (Reflektion, Diffuse- und Direkte Strahlung) zum Zeitpunkt
        enDiffModule = RadDiffHor * 0.5 * (1 + np.cos(ModuleAngleRad))
        enDirModule = np.maximum(0.9, np.cos(df["sun_azimuth"].values - np.deg2rad(pvPlant["ModuleAzimute"].values.reshape((1,-1)).astype(np.float64)))) * RadDirHor

        #enDirModule = plane_direct
        #Berechnung Globalstrahlung und Modultemperatur
        gModule = plane_direct * PVPlantOpti.CalcShadowing(df["sun_alt"]) + plane_diffuse 
        tModule = T_env + PVPlantOpti.GetTcoeff() * gModule

        #Berechnung Modulwirkungsgrad auf Basis der Globalstrahlung und Modultemperatur
        etaMod = PVPlantOpti.CalcEtaMod(gModule, tModule)

        #Schreiben des aktuellen Gesamt-Anlagenwirkungsgrades zur späteren Berechnung der Stromgestehungskosten
        Efficiency = etaSys * etaMod * etaWR

        #Berechnung der Erzeugungsleistung zum Zeitpunkt auf Basis der Einstrahlung und des Wirkungsgrades
        power = np.maximum(0, Efficiency * ((gModule * pvPlant["PowerMW"].values.reshape((1,-1))) / 1000))
        return np.minimum(power, pvPlant["PowerMW"].values.reshape((1,-1)))
        #if (power > x[2]):
        #    Error("Berechnungsfehler - PV Leistung")
    
    def getSolarPower(x, power, pvPlant, SolarWeatherNUTS, angles = None, system_loss = 0.2, **kwargs):
        capacity = power
        # NB: aperture_irradiance expects azim/tilt in radians!
        irrad = test.aperture_irradiance(
            SolarWeatherNUTS.RadDirHor,
            SolarWeatherNUTS.RadDiffHor,
            (pvPlant["Latitude"], pvPlant["Longitude"]),
            azimuth=math.radians(pvPlant["ModuleAzimute"]),
            tilt=math.radians(x),
            angles=angles,
        )
        datetimes = irrad.index

        # Temperature, if it was given
        tamb = SolarWeatherNUTS.T_env


        # Set up the panel model
        # NB: panel efficiency is not used here, but we retain the possibility
        # to adjust both efficiency and panel size in case we want to emulate
        # specific panel types
        panel_efficiency = 0.1
        area_per_capacity = 0.001 / panel_efficiency

        panel = test.HuldCSiPanel(
            panel_aperture=capacity * area_per_capacity,
            panel_ref_efficiency=panel_efficiency,
            **kwargs,
        )

        # Run the panel model and return output
        irradiance = irrad.direct + irrad.diffuse
        output = panel.panel_power(irradiance, tamb)
        dc_out = pd.Series(output, index=datetimes).clip(upper=capacity)

        use_inverter = True
        inverter_capacity = capacity
        if use_inverter:
            inverter = test.Inverter(inverter_capacity)
            ac_out = dc_out.apply(inverter.ac_output).clip(lower=0)
            ac_out_final = ac_out * (1 - system_loss)
        else:
            ac_out_final = dc_out * (1 - system_loss)

        
        return ac_out_final

    def getPVfromSolarOnePlantFixedPower(x, power, pvPlant, SolarWeatherNUTS):
        y = np.array([x, power])
        return PVPlantOpti.getPVfromSolarOnePlantAndNUTS(y, pvPlant, SolarWeatherNUTS)

    def test_function():
        pvPlant = {"Latitude": 52.5170365, "Longitude": 13.3888599 , "ModuleAzimute": 180, "Tilt": 35 }
        x = np.array([35, 100])
        df = pd.read_csv("C:\\Users\\studt\\source\\repos\\mozubi_tools\\_Data\\PV_Aggregation\\ninja_pv_52.5170_13.3889_corrected.csv", skiprows=3, parse_dates= [0], index_col= 0 )
        #df.rename(mapper = {"electricity": "PV_Ante", "irradiance_direct":"RadDirHor", "irradiance_diffuse": "RadDiffHor", "temperature": "T_env" }, axis = 1, inplace = True)
        df , SolarWeatherdf = PVPlantOpti.GetSolar2(df, df.index[0], df.index[-1])
        #df[["RadDirHor", "RadDiffHor"]] = df[["RadDirHor", "RadDiffHor"]] * 1000
        dfused = SolarWeatherdf["DE"].join(df)
        #dfused[["RadDirHor", "RadDiffHor"]] = dfused[["RadDirHor", "RadDiffHor"]] * 1000
        yd = PVPlantOpti.getSolarPower(35, 100*1000, pvPlant, dfused )/1000
        print((yd -df["electricity"]).iloc[:24])

    def build_func(self, x, pv_Plants, SolarWeatherDA):
        error("Implementation in derived class")
    def errorfunc(self, x, y, pv_Plants, SolarWeatherDA):
        err = self.build_func(x, pv_Plants, SolarWeatherDA) - y
        return err
    def setup(self, pvProduction,pv_Plants, SolarWeatherdf):
        error("Implementation in derived class")
    def getData():
        pv_Plants = PVPlantOpti.getPVPlants()
        pvProduction = PVPlantOpti.getSolarProduction()
        pvProduction , SolarWeatherdf = PVPlantOpti.GetSolar(pvProduction, pvProduction.index[0], pvProduction.index[-1])
        return pv_Plants, pvProduction, SolarWeatherdf
    def optimization(self):
        pv_Plants, pvProduction, SolarWeatherdf = PVPlantOpti.getData()
        x0, lowerBounds, upperBounds, y, SolarWeatherdf = self.setup(pvProduction,pv_Plants, SolarWeatherdf)
        self.res = least_squares(self.errorfunc, x0,verbose=2, bounds = (lowerBounds, upperBounds), args = (y, pv_Plants, SolarWeatherdf) )
        print(np.sqrt(np.average((self.res.fun)**2)))
        return self.res
class AzimuthAndAngle(PVPlantOpti):
    def build_func(self,x, pv_Plants, SolarWeatherDA):
        np.seterr(divide = "ignore")
        pos = pv_Plants.mean()
        idxcounter = 0
        targetfunc = 0
        power = x[0]*np.array([0.063, 0.042, 0.011, 0.212, 0.424, 0.069, 0.070, 0.059, 0.017])
        pvPlant = {"Longitude": pos["Longitude"], "Latitude": pos["Latitude"]}
        for azimut in [90, 180, 270]:
            pvPlant["ModuleAzimute"] = azimut
            for i in range(0,3):
                targetfunc = targetfunc + PVPlantOpti.getPVfromSolarOnePlantFixedPower(x[idxcounter+1], power[idxcounter], pvPlant, SolarWeatherDA)
                idxcounter = idxcounter + 1
        return targetfunc
    def setup(self, pvProduction,pv_Plants, SolarWeatherdf):
        x0 = np.tile(np.array([10.0, 30.0, 50.0]), 3)
        x0 = np.insert(x0, 0, 10000.0)
        lowerBounds = np.tile(np.array([0.0, 20.0, 40.0]), 3) 
        lowerBounds = np.insert(lowerBounds, 0, 0.0)
        upperBounds = np.tile(np.array([20.0,40.0, 60.0]), 3) 
        upperBounds = np.insert(upperBounds, 0, np.inf)
        y = pvProduction["PV_Ante"].values
        return x0, lowerBounds, upperBounds, y, SolarWeatherdf["DE"]
class FixedPower(PVPlantOpti):
    def build_func(self,x, pv_Plants, SolarWeatherDA):
        np.seterr(divide = "ignore")
        pos = pv_Plants.mean()
        idxcounter = 0
        targetfunc = 0
        power =10000*np.array([0.063, 0.042, 0.011, 0.212, 0.424, 0.069, 0.070, 0.059, 0.017])
        xIn = np.zeros((9,3))
        xIn[:,0] = x[1:]
        xIn[:,1] = power
        xIn[:,2] = np.ones((9,))*x[0]
        pvPlant = {"Longitude": pos["Longitude"], "Latitude": pos["Latitude"]}
        xIn = xIn.flatten()
        for azimut in [90, 180, 270]:
            pvPlant["ModuleAzimute"] = azimut
            for i in range(0,3):
                targetfunc = targetfunc + PVPlantOpti.getPVfromSolarSimple(xIn[idxcounter:idxcounter+3], pvPlant, SolarWeatherDA)
                idxcounter = idxcounter + 3
        return targetfunc
    def setup(self, pvProduction,pv_Plants, SolarWeatherdf):
        x0 = np.tile(np.array([10.0, 30.0, 50.0]), 3)
        x0 = np.insert(x0, 0, 0.8)
        lowerBounds = np.tile(np.array([0.0, 20.0, 40.0]), 3) 
        lowerBounds = np.insert(lowerBounds, 0, 0.0)
        upperBounds = np.tile(np.array([20.0,40.0, 60.0]), 3) 
        upperBounds = np.insert(upperBounds, 0, 1.0)
        y = pvProduction["PV_Ante"].values
        return x0, lowerBounds, upperBounds, y, SolarWeatherdf["DE"]
class GSEEFunction(PVPlantOpti):
    def build_func(self,x, pv_Plants, SolarWeatherDA, angles = None):
        np.seterr(divide = "ignore")
        pos = pv_Plants.mean()
        idxcounter = 0
        targetfunc = 0
        #x[0] = 0.0
        power =10000*np.array([0.063, 0.042, 0.011, 0.212, 0.424, 0.069, 0.070, 0.059, 0.017])
        pvPlant = {"Longitude": pos["Longitude"], "Latitude": pos["Latitude"]}
        for azimut in [90, 180, 270]:
            pvPlant["ModuleAzimute"] = azimut
            for i in range(0,3):
                targetfunc = targetfunc + PVPlantOpti.getSolarPower(x[idxcounter], power[idxcounter], pvPlant, SolarWeatherDA, angles, 0.0)
                idxcounter = idxcounter +1
        return targetfunc
    def setup(self, pvProduction,pv_Plants, SolarWeatherdf):
        x0 = np.tile(np.array([10.0, 30.0, 50.0]), 3)
        #x0 = np.insert(x0, 0, 0.1)
        lowerBounds = np.tile(np.array([0.0, 20.0, 40.0]), 3)
        #lowerBounds = np.insert(lowerBounds, 0, 0.0) 
        upperBounds = np.tile(np.array([20.0,40.0, 60.0]), 3) 
        #upperBounds = np.insert(upperBounds, 0, 1.0)
        y = pvProduction["PV_Ante"].values
        pos = pv_Plants.mean()
        sunrise_set_times = test.sun_rise_set_times(pvProduction.index, (pos["Latitude"], pos["Longitude"]))
        angles = test.sun_angles(pvProduction.index, (pos["Latitude"], pos["Longitude"]), sunrise_set_times)
        return x0, lowerBounds, upperBounds, y, SolarWeatherdf["DE"], angles
    def errorfunc(self, x, y, pv_Plants, SolarWeatherDA, angles):
        err = self.build_func(x, pv_Plants, SolarWeatherDA, angles) - y
        return err
    def optimization(self):
        pv_Plants, pvProduction, SolarWeatherdf = PVPlantOpti.getData()
        x0, lowerBounds, upperBounds, y, SolarWeatherdf, angles = self.setup(pvProduction,pv_Plants, SolarWeatherdf)
        self.res = least_squares(self.errorfunc, x0,verbose=2, bounds = (lowerBounds, upperBounds), args = (y, pv_Plants, SolarWeatherdf, angles) )
        print(np.sqrt(np.average((self.res.fun)**2)))
        return self.res
class AllNuts(PVPlantOpti):
    def errorfunc(self, x, y, pv_Plants, SolarWeatherDA, angles):
        err = self.build_func(x, pv_Plants, SolarWeatherDA, angles) - y
        return err
    def build_func(self, x, pv_Plants, SolarWeatherDA, angles= None):
        idxcounter = 0
        targetfunc = 0
        power = (pv_Plants.values / pv_Plants.sum().sum()).flatten()
        power = x[0]*power
        np.seterr(divide = 'ignore')
        for nuts2 in pv_Plants.index:
            pvPlant = {"Longitude": pv_Plants.loc[nuts2, "Longitude"]
            , "Latitude": pv_Plants.loc[nuts2, "Latitude"] }
            for azimut in [90, 180, 270]:
                pvPlant["ModuleAzimute"] = azimut
                dfangles = None if angles is None else angles[nuts2]
                targetfunc = targetfunc + PVPlantOpti.getSolarPower(x[idxcounter+1], power[idxcounter], pvPlant, SolarWeatherDA[nuts2], dfangles, 0.1)
                idxcounter = idxcounter +1
        return targetfunc
    def setup(self, pvProduction,pv_Plants,  SolarWeatherdf):
        x0 = np.ones(len(pv_Plants)*3+1)*32.5
        x0[0] = 4000
        lowerBounds = np.zeros_like(x0)
        upperBounds = np.ones_like(x0)*60
        upperBounds[0] = 10000
        y = pvProduction["PV_Ante"].values
        angles = {}
        for key in pv_Plants.index:
            latitude = pv_Plants.loc[key, "Latitude"]
            longitude = pv_Plants.loc[key, "Longitude"]
            sunrise_set_times = test.sun_rise_set_times(pvProduction.index, (latitude, longitude))
            angles[key] = test.sun_angles(pvProduction.index, (latitude, longitude), sunrise_set_times)
        return x0, lowerBounds, upperBounds, y, SolarWeatherdf, angles
    def optimization(self):
        pv_Plants, pvProduction, SolarWeatherdf = PVPlantOpti.getData()
        x0, lowerBounds, upperBounds, y, SolarWeatherdf, angles = self.setup(pvProduction,pv_Plants, SolarWeatherdf)
        self.res = least_squares(self.errorfunc, x0,verbose=2,ftol = 1e-3, bounds = (lowerBounds, upperBounds), args = (y, pv_Plants, SolarWeatherdf, angles) )
        print(np.sqrt(np.average((self.res.fun)**2)))
        return self.res
    def func2(x ,  pv_Plants, SolarWeatherDA):
        starttime = time.time()
        err = PVPlantOpti.build_func(x, pv_Plants, SolarWeatherDA) 
        endtime = time.time()
        print("Err: " + str(np.sum(np.abs(err))))
        print("Time: "+ str(endtime - starttime))
        return err

if __name__ == "__main__":
    PVPlantOpti.test_function()   
    pv_Plants, pvProduction, SolarWeatherdf = PVPlantOpti.getData()



    from scipy.optimize import least_squares
    from sklearn.linear_model import LinearRegression
    from seaborn import pairplot
    import matplotlib.pyplot as plt

    df = SolarWeatherdf["DE"].loc[:, ["T_env", "RadDirHor", "RadDiffHor" ]].join(pvProduction)
    fig1, ax1 = plt.subplots()
    ax1_1 = ax1.twinx()
    ax1.plot(df["PV_Ante"].groupby(df.index.month).mean(), color = 'C0')
    ax1_1.plot(df[["RadDirHor", "RadDiffHor"]].sum(axis = 1).groupby(df.index.month).mean(), color = 'C1')
    fig, axs = plt.subplots(2,1, sharex= True)
    ax2 = axs[0].twinx()
    axs[0].plot(df["PV_Ante"].groupby(df.index.hour).mean(), color = 'C0', label = "PV Produced")
    dfrad = df[["RadDirHor", "RadDiffHor"]].groupby(df.index.hour).mean().sum(axis= 1)
    ax2.plot(dfrad, color = 'C1', label = "Radiation (Dir + Diff)")
    axs[1].set_xlabel("Stunde des Tages")
    axs[0].set_ylabel("PV Produced [kWh]")

    ax2.set_ylabel("Radiation")
    temp = df["T_env"].groupby(df.index.hour).mean()
    axs[1].plot(df["PV_Ante"].groupby(df.index.hour).std()/df["PV_Ante"].groupby(df.index.hour).mean(), color = 'C0', label = "PV Produced")
    axs[1].plot(df[["RadDirHor", "RadDiffHor"]].groupby(df.index.hour).std().sum(axis= 1)/dfrad, color = 'C1', label = "Radiation (Dir + Diff)")
    #axs[1].plot(df["T_env"].groupby(df.index.hour).std() / temp, label = "Temp")
    axs[1].set_ylabel("Variation = Standardabweichung / Mittelwert")
    axs[1].legend()
    #ax2.plot(df["RadDiffHor"].groupby(df.index.hour).mean())
    #pairplot(df)
    #plt.show()
    dflist = list()
    for keys in SolarWeatherdf:
        if keys == "DE":
            dftemp = SolarWeatherdf[keys][["T_env", "RadDirHor", "RadDiffHor" ]]
            dflist.append(dftemp.add_prefix(keys))
    dflin = pd.concat(dflist, axis = 1)
    #dflin = df[["T_env", "RadDirHor", "RadDiffHor" ]]
    #dflin["Hour"] = df.index.hour
    model = LinearRegression().fit(dflin, pvProduction)

    from sklearn.metrics import mean_squared_error
    err = np.sqrt(np.average((model.predict(dflin)- pvProduction)**2))
    print(err/10000)

    df1 = pd.concat([dflin, pvProduction], axis = 1)
    print(df1.corr().sort_values(by = ["PV_Ante"])["PV_Ante"].iloc[-40:])
    idc = np.argsort(np.abs(model.coef_.flatten()))
    for id in idc:
        print(dflin.columns[id] + ":")
        print(model.coef_.flatten()[id])

    fig, ax = plt.subplots()

    sortedindex = np.argsort(pvProduction.values.flatten())
    ax.plot(np.sort(pvProduction.values.flatten()), label = "tatsächliche Erzeugung")
    ax.plot(np.sort(model.predict( dflin.loc[dflin.index >= pvProduction.index[0]]).flatten()), label ="lineares Modell")
    x = np.array([7171.900500195334, 12.724928353732565, 39.99996039181263, 58.90607605742883, 9.682257011430725e-08, 20.74236795167301, 59.99999999999083, 1.182619272346341e-05, 25.6676609839774,59.99999911387107  ])
    x2 = np.array([0.6567188854773429, 0, 20.0, 40.0, 0.0, 20.0, 40.0, 0.0, 25.81208877759449 , 60.0])
    x3 = np.array([ 20, 40, 60, 0, 20, 60, 0.0, 20, 60])

    xAllNuts = np.array([9.99078602e+03 ,3.17464932e+00, 2.51435934e+01, 2.00523467e+01,
    2.69759578e-01, 2.77241437e+01, 1.54975626e+01, 3.10178192e+00,
    2.47913767e+01, 1.60590738e+01, 6.81261291e-01, 2.58505497e+01,
    6.19638926e+00, 2.54455397e+00, 2.91742495e+01, 2.26281855e+01,
    2.74330328e+00, 2.57722049e+01, 2.17771023e+01, 1.18969967e-03,
    2.91691331e+01, 2.41778759e+01, 2.92563848e+00, 2.58088904e+01,
    1.86365375e+01, 8.45148650e-01, 2.51619141e+01, 2.31615772e+01,
    2.91058634e+00, 2.89574883e+01, 1.98384047e+01, 2.72277789e+00,
    3.33090813e+01, 1.37243085e+01, 2.94711139e-02, 2.80759248e+01,
    1.19561621e+01, 3.34339108e+00, 2.39876543e+01, 1.26505176e+01,
    1.23118527e+00, 3.12653621e+01, 2.14666272e+01, 2.88413743e+00,
    3.17426886e+01, 2.40099403e+01, 3.16911510e+00, 3.06663894e+01,
    2.00016413e+01, 4.44156162e-02, 3.09115569e+01, 2.61750404e+01,
    3.19372940e+00, 3.24750609e+01, 1.39022519e+01, 1.63208703e+00,
    3.16278459e+01, 3.83945450e+00, 3.52791479e+00, 2.91648961e+01,
    8.47447012e+00, 3.28349264e+00, 3.16868882e+01, 8.87269019e+00,
    3.59953915e-01, 3.07509760e+01, 1.77322304e+01, 3.30761559e+00,
    3.06860596e+01, 1.08469835e+01, 1.78753858e+00, 2.38318815e+01,
    1.12347454e+00, 3.66853253e+00, 2.73027871e+01, 8.03368078e+00,
    3.44629297e+00, 2.41150782e+01, 9.44073433e+00, 1.53001251e-02,
    2.61576290e+01, 3.31586603e-02, 2.81037694e+00, 3.38057488e+01,
    1.64642805e+01, 9.64076657e-01, 3.30657962e+01, 2.28573464e+01,
    2.89349712e+00, 3.18657256e+01, 1.88827403e+01, 2.91935988e+00,
    3.09948030e+01, 2.46522208e+01, 1.50437168e-02, 3.20962306e+01,
    2.86358598e+01, 3.02506205e+00, 3.22229372e+01, 1.73743380e+01])
    yvals = AllNuts().build_func(xAllNuts, pv_Plants, SolarWeatherdf).values
    print(np.sqrt(np.average((yvals- pvProduction.values.flatten())**2)))

    ax.plot(np.sort(yvals.flatten()), label = "Prognose")
    #ax1_2 = ax.twinx()
    #for column in dflin.columns:
    #   ax1_2.plot(dflin[column])
    #ax.plot(np.sort(yvals), label = "parametrisiertes Modell")
    from seaborn import histplot
    df = pvProduction.join(dflin)
    #histplot(data = df, x = "DERadiation", y = "PV_Ante", ax  =ax)
    #ax.set_ylabel("PV Erzeugung in kW")
    #ax.set_xlabel("Zeit")
    ax.legend()
    plt.show()
    #from sklearn.dummy import DummyRegressor
    #model2 = DummyRegressor().fit(df[["T_env", "RadDirHor", "RadDiffHor" ]], pvProduction)
    #err2 = np.sqrt(mean_squared_error(model2.predict(df[["T_env", "RadDirHor", "RadDiffHor" ]]), pvProduction))

    res = AllNuts().optimization()
    #errorfunc(x0, y, pv_Plants, SolarWeatherdf)

    print(res.x)
    index = 0
    print("Power MW")
    print(res.x[0])
    print("Neigung in Grad --- Leistung in MW:")
    print("OstAnlagen: ")
    for i in range(1,4,1):
        print(res.x[i] ,"---" )
    print("SüdAnlagen: ")
    for i in range(4,7,1):
        print(res.x[i] ,"---" )
    print("WestAnlagen: ")
    for i in range(7,10,1):
        print(res.x[i] ,"---" )

    # print(np.sqrt(np.mean(errorfunc2(res.x,y, pv_Plants, SolarWeatherdf["DE"])**2))/10000)

    # import matplotlib.pyplot as plt
    # pred = build_func2(res.x, pv_Plants, SolarWeatherdf["DE"])
    # fig, axs = plt.subplots(3,1)
    # err = (pred - pvProduction["PV_Ante"])**2
    # axs[0].plot(np.sqrt(err), color = 'C0')
    # axs[0].plot(pvProduction, color = 'C1')

    # axs[1].plot(np.sqrt(err.groupby(pred.index.hour).mean())/10000)

    # axs[2].plot(np.sqrt(err.groupby(pred.index.month).mean())/10000)
    # plt.show()
    # #forecast_DA.Add((t, power, Attributes.Economics.LCOE))