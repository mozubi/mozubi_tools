#python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
import warnings
warnings.filterwarnings('ignore')
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import os
import urllib.request
from random import randint



# print message after packages imported successfully
print("import of packages successful, Importing and proccecing data")
#print(os.getcwd())

data_comp_Not_Norm = np.loadtxt(fname = os.getcwd()+"\\All_with_Saldo.csv", dtype='str',delimiter = ";")#load from textfile
data_comp = np.loadtxt(fname = os.getcwd()+"\\All_with_Saldo_Normalized.csv", dtype='str',delimiter = ";")#load from textfile

data_comp_Not_Norm = np.delete(data_comp_Not_Norm, 0, 0)
data_comp_Not_Norm = np.delete(data_comp_Not_Norm, 0, 1)#delete other columns we as humans wanted
data_comp_Not_Norm = np.delete(data_comp_Not_Norm, 0, 1)


data_cleaned = np.delete(data_comp, 0, 0)		#delete the first row with titles
#np.random.shuffle(data_comp)#schuffle data so we can split into train and test later
data_cleaned = np.delete(data_cleaned, 0, 1)#delete other columns we as humans wanted
data_cleaned = np.delete(data_cleaned, 0, 1)

#for x in range(47):
	#data_cleaned = np.delete(data_cleaned, 22, 1) #this removes the timestamps



data_comp_Not_Norm = data_comp_Not_Norm.astype(float)
data_cleaned = data_cleaned.astype(float)

YNN = data_comp_Not_Norm[:,0];
XNN = np.delete(data_comp_Not_Norm,0, 1)

Y = data_cleaned[:,0]; #get the right col for "labels" should be 0
X = np.delete(data_cleaned,0, 1)#Remove the labels to create data should be 0,1

X = np.where(X==0, 0, X)
X = np.where(X==1, 0.5, X)
print(X[:,0])
#print(X[:,3])
#plt.plot(Y,linewidth=0.5)
#plt.plot(X[:,3],color='red',linewidth=0.5)

#20

#plt.show()
for x in range(20):
	#print("Pearsons r: " ,scipy.stats.pearsonr(X[:,x],Y)[0])
	#print("Col: " , x)
	if (abs(scipy.stats.pearsonr(X[:,x],Y)[0])-0.000001) < 0:#0.01 gives good results
		X = np.delete(X, x, 1)
		#print("Pearsons r: " ,scipy.stats.pearsonr(X[:,x],Y)[0])

print("X size: ", X.shape)		
print(X)	


	
	#print("Spearmans rho: " ,scipy.stats.spearmanr(X[:,x],Y))
	#print("Kendall's tau: " ,scipy.stats.kendalltau(X[:,x],Y))
# np.savetxt('X.csv', X, delimiter=';')
# np.savetxt('Y.csv', Y, delimiter=';')


XNN_test, XNN_train = XNN[:1000], XNN[1000:]
YNN_test, YNN_train = YNN[:1000], YNN[1000:]


testX, trainingX = X[:1000], X[1000:]	#get training and testing X
testY, trainingY = Y[:1000], Y[1000:]	#get training and testing Y
print("Data poccessed starting on the ML stuff")

print(trainingX.shape)
model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(68, activation='tanh', input_shape=[trainingX.shape[1]]),
  keras.layers.Dense(68, activation='tanh'),
  keras.layers.Dense(68, activation='tanh'),
  keras.layers.Dense(68, activation='tanh'),
  keras.layers.Dense(68, activation='tanh'),
  tf.keras.layers.Dense(1)
])
sgd = tf.optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)

#loss='mean_squared_logarithmic_error'
#loss='mean_squared_error'
#optimizer='rmsprop'

#optimizer='Adagrad' crappy
#optimizer='Nadam' 3okay ish.. stops arounf 7700

rmspop = tf.keras.optimizers.RMSprop(0.001)
model.compile(optimizer=rmspop,loss='mean_squared_error')
model.fit(
trainingX,trainingY,
epochs=150,
validation_data = (testX, testY))


predictions = model.predict(testX)
print(XNN_test.shape)
print(predictions.shape)
for x in range (10):
	print(predictions[x])

plt.plot(predictions,color='blue',linewidth=0.5)
plt.plot(testY,color='red',linewidth=0.5)
plt.plot(XNN_test[:,0],color='green',linewidth=0.5)
plt.show()
