|| Name || Inhalt || von-bis || Anmerkung ||
| DayAhead | Preise/Volumen | 2014 - 2018 | 2019-2020 folgt |
| IntraDay15min | Preise/Volumen | 2014 - 2019 | - |
| Generation | Erzeugung nach Technologie | 2015 - 2020 | - |
| Generation_units | Erzeugung von Anlagen > 100MW | 2015 - 2020 | - |
| Gridload | Volume | 2015 - 2020 | - |
| SRL | AP/EP, VOL | 2015 - 2020 | - |
| MRL | AP/EP, VOL | 2015 - 2020 | - |
| RE | Erzeugung PV+Wind (ante/post) | 2014 - 2020 | - |
| NRV | Regelzonensaldo | 2014 - 2020 | - |
| reBAP | Ausgleichsenergiepreis | 2014 - 2020 | - |
| Weather | Precipitation/Temperature/Irr_Surf/Irr_toa/Snowfall/Snow_mass/Cloud_cover/Air_density | 2000-2019 | - |
| Sources | Quellenangaben | - | - |