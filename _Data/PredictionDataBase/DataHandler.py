import sqlite3
import numpy as np
import os
import pandas as pd
import datetime
import numpy as np
import matplotlib.pyplot as plt

datapath = os.path.dirname(__file__)+'\\'

sources = pd.DataFrame(columns=['TableName','Source','Date'])

def findMissing(tableName, data, startdate, enddate, frequency, columns = None, prefix = ""):
	df = pd.DataFrame(columns = ['Table', 'Column', 'StartMissing', 'EndMissing', 'TimeDelta'])

	date = data.set_index(pd.DatetimeIndex(data["TimeStamp"]))
	columns = date.columns if columns is None else columns
	for column in columns:
		if column != "Id" and column != "TimeStamp":
			s = date[column].dropna(how='any')
			missingDates = pd.date_range(startdate, enddate, freq = frequency).difference(s.index)
			start = missingDates[0]
			before = start
			for idx in missingDates:
				if idx-before > pd.to_timedelta(frequency):
					df = df.append({'Table':tableName, 'Column':column, 'StartMissing':start, 'EndMissing':before, 'TimeDelta':before-start}, ignore_index = True)
					start = idx
					before = start
				else:
					before = idx
			df = df.append({'Table':tableName, 'Column':column, 'StartMissing':start, 'EndMissing':missingDates[-1], 'TimeDelta':missingDates[-1]-start }, ignore_index = True)
	df.to_csv(datapath+prefix+'missing'+tableName+'.csv',sep=';')
def fillMissing(data, frequency):
	from dateutil.relativedelta import relativedelta
	data = data.set_index(pd.DatetimeIndex(data["TimeStamp"]))
	data = data.asfreq(frequency)
	for column in data:
		if column != "Id" and column != "TimeStamp":
			colWithNan = data[column]
			colInter = colWithNan.fillna(method = 'pad',limit = 4)
			colInter = colInter.interpolate(limit = 8)
			missingDates = pd.date_range(data.index[0], data.index[-1], freq = frequency).difference(colInter.dropna().index)
			newDates = []
			for date in missingDates:
				new = date-relativedelta(years=1)
				if new >=data.index[0] :
					newDates.append(new)
				else:
					newDates.append(date+relativedelta(years=1))
			colInter.loc[missingDates] = colInter.loc[newDates].values
			data.loc[:, column] = colInter
	return data.drop(['Id','TimeStamp'], axis = 'columns').reset_index().rename(columns={'index':'TimeStamp'})







def appendSource(tableName, source, date):
	global sources
	sources = sources.append({'TableName': str(tableName),'Source': str(source),'Date': str(date)},ignore_index=True)

def fill_sources(conn):
	appendSource('Time-Zone-Info','Europe/Berlin','-')
	sources.to_sql('Source', conn,if_exists = 'append', index = False)
	print("Sources\t\t\t----->\t filled")

def fill_MRL(conn):

	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	
	header_names = ["Date","Time", "AbgerMenge_Pos","AbgerMenge_Neg","Arbeitspreis_Pos","Arbeitspreis_Neg","VorgehMenge_Pos","VorgehMenge_Neg","Leistungspreis_Pos","Leistungspreis_Neg"]
	
	data = pd.read_csv(datapath+"CSV_files\\MRL.csv", names = header_names, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	
	#Date;Time of day;MRL_AberMenge_Pos;MRL_AberMenge_Neg;MRL_Arbeitspreis_pos;MRL_Arbeitspreis_Neg;MRL_VorgehMenge_pos;MRL_VorgehMenge_Neg;MRL_LeistungsPreis_Pos;MRL_LeistungsPreis_Neg

	appendSource('MRL','smard.de','2020')

	data.dropna(how='all').to_sql("MRL", conn, if_exists = 'append', index = False)
	print("MRL\t\t\t----->\t filled")

def fill_SRL(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	
	header_names = ["Date","Time", "AbgerMenge_Pos","AbgerMenge_Neg","Arbeitspreis_Pos","Arbeitspreis_Neg","VorgehMenge_Pos","VorgehMenge_Neg","Leistungspreis_Pos","Leistungspreis_Neg"]
	
	data = pd.read_csv(datapath+"CSV_files\\SRL.csv", names = header_names, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	
	#Date;Time of day;SRL_AberMenge_Pos;SRL_AberMenge_Neg;SRL_Arbeitspreis_pos;SRL_Arbeitspreis_Neg;SRL_VorgehMenge_pos;SRL_VorgehMenge_Neg;SRL_LeistungsPreis_Pos;SRL_LeistungsPreis_Neg
	
	appendSource('SRL','smard.de','2020')

	data.dropna(how='all').to_sql("SRL", conn, if_exists = 'append', index = False)
	print("SRL\t\t\t----->\t filled")

def fill_NRV(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	
	header_names = ["Date","Time", "Saldo"]
	
	data = pd.read_csv(datapath+"CSV_files\\NRVSaldo.csv", names = header_names,header = 0, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	
	appendSource('NRV','regelleistung.net','2020')

	data.dropna().to_sql("NRV", conn, if_exists = 'append', index = False)
	print("NRV\t\t\t----->\t filled")

def fill_Rebap(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	
	header_names = ["Date","Time", "Price"]
	
	data = pd.read_csv(datapath+"CSV_files\\Rebap.csv", names = header_names, header = 0, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	
	appendSource('reBAP','regelleistung.net','2020')
	
	data.dropna().to_sql("reBAP", conn, if_exists = 'append', index = False)
	print("reBAP\t\t\t----->\t filled")



def fill_RE(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	#header_names = ["Date","Time", "Price"]
	cols = ['Datum', 'Zeit',
	"pv50HerzMW","wind50HerzMW",
	"pvAmprion","windAmprion",
	"pvTransnetBW","windTransnetBW",
	"pvTenneT","windTenneT",
	"pv_50HerzProjection","wind_50HerzProjection",
	"pv_AmprionProjection","wind_AmprionProjection",
	"pv_transnetBWProjection","wind_transnetBWProjection",
	"wind_tennetProjection","pv_tennetProjection"]

	data = pd.read_csv(datapath+"CSV_files\\RE.csv", usecols = cols, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	
	data_out = pd.DataFrame()
	data_out['TimeStamp'] = data['TimeStamp']
	data_out['PV_Ante'] = data['pv_50HerzProjection'] + data['pv_AmprionProjection'] + data['pv_transnetBWProjection'] + data['pv_tennetProjection']
	data_out['WI_Ante'] = data['wind_50HerzProjection'] + data['wind_AmprionProjection'] + data['wind_transnetBWProjection'] + data['wind_tennetProjection']
	data_out['PV_Post'] = data['pv50HerzMW'] + data['pvAmprion'] + data['pvTransnetBW'] + data['pvTenneT']
	data_out['WI_Post'] = data['wind50HerzMW'] + data['windAmprion'] + data['windTransnetBW'] + data['windTenneT']
	
	appendSource('RE','entsoe-transparency','2020')

	data_out.dropna().to_sql("RE", conn, if_exists = 'append', index = False)

	print("RE\t\t\t----->\t filled")

def fill_APMAX(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S")
	header_names = ["TimeStamp", "APmax"]
	data = pd.read_csv(datapath+"CSV_files\\APMAX.csv", names = header_names, header = 0, sep = ';', na_values = '-',parse_dates = ['TimeStamp'] )#,date_parser = dateparser)
	#data.rename(columns={"Load": "Volume"}, inplace = True)

	#print(data)

	appendSource('APMax','a calculation using SRL_Bedarf and result list, from regelleistung.net','2020')


	data.dropna().to_sql("APMax", conn, if_exists = 'append', index = False)

	print("APMax\t\t\t----->\t filled")

def fill_GridLoad(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	header_names = ["Date","Time", "Load"]
	data = pd.read_csv(datapath+"CSV_files\\GridLoad.csv", names = header_names, header = 0, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	data.rename(columns={"Load": "Volume"}, inplace = True)

	appendSource('GridLoad','entsoe-transparency','2020')

	data.dropna().to_sql("GridLoad", conn, if_exists = 'append', index = False)

	print("GridLoad\t\t----->\t filled")

def fill_Intraday(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")

	header_names = ["Date","Time","Intraday_Preis","Intraday_Volumen"]

	data = pd.read_csv(datapath+"CSV_files\\ID.csv", names = header_names, header = 0, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	data.rename(columns={"Intraday_Preis": "Price", "Intraday_Volumen":"Volume"}, inplace = True)
	data = data.set_index('TimeStamp')
	data.index = data.index.tz_localize('Europe/Berlin')

	missing = pd.read_csv(datapath+"CSV_files\\ID_2018-2019_convert.csv", sep = ';')
	missing['TimeStamp'] = pd.to_datetime(missing['TimeStamp'])
	missing= missing.set_index('TimeStamp')
	# missing.index = missing.index.tz_convert('Europe/Berlin')
	missing.rename(columns={"IDPrice": "Price", "IDVolume":"Volume"}, inplace = True)

	missing.index = missing.index.tz_localize(None)
	data.index = data.index.tz_localize(None)

	data = data.loc[:'2017']
	data = data.append(missing).sort_index()
	data['TimeStamp'] = data.index
	data['TimeStamp'] = pd.to_datetime(data['TimeStamp'])
	data = data.drop_duplicates(subset='TimeStamp')

	appendSource('Intraday','No-Source','2020')

	data.dropna().to_sql("Intraday", conn, if_exists = 'append', index = False)

	print("Intraday\t\t----->\t filled")

def fill_DayAhead(conn):
	dateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y %H:%M")
	cols = ['Datum', 'Zeit', 
			"SpotPrice","SpotVol"]
	#header_names = ["Date", "Time", "Price", "Volume"]
	data = pd.read_csv(datapath+"CSV_files\\All_with_Saldo.csv", usecols = cols, sep = ';', na_values = '-', parse_dates = {'TimeStamp': [0,1]} ,date_parser = dateparser)
	data.rename(columns={"SpotPrice": "Price", "SpotVol": "Volume"}, inplace = True)
	data = data.set_index('TimeStamp')
	data.index = data.index.tz_localize('Europe/Berlin')


	df = pd.read_csv(datapath+"CSV_files\da_missing_prices_entsoe.csv", sep=';')
	df.TimeStamp = pd.to_datetime(df.TimeStamp, utc=True)
	df = df.set_index('TimeStamp')
	df.index = df.index.tz_convert('Europe/Berlin')
	df = df.resample(datetime.timedelta(minutes=15)).ffill()

	df2 = pd.read_csv(datapath+"CSV_files\DAPrices_2015mar29_dayAhead.csv", sep=';')
	df2.TimeStamp = pd.to_datetime(df2.TimeStamp, utc=True)
	df2 = df2.set_index('TimeStamp')
	df2.index = df2.index.tz_convert('Europe/Berlin')

	idx = pd.date_range(df2.tail(1).index[0], periods=4, freq='15Min')[1:]
	df2 = df2.append(pd.DataFrame(pd.Series(np.repeat(df2.tail(1).values, 3), index=idx,name='Prices')))
	df2 = df2.resample(datetime.timedelta(minutes=15)).ffill()

	df.index = df.index.tz_localize(None)
	df2.index = df2.index.tz_localize(None)
	data.index = data.index.tz_localize(None)

	df3= data.append(df)
	df3 = df3.append(data.loc[df.tail(1).index.values[0]:])

	for t in df2.index.values:
		df3.loc[t] = df2.loc[t].Prices

	export = pd.DataFrame()
	export['TimeStamp'] = df3.index
	export['Price'] = df3.Price.values
	export['Volume'] = df3.Volume.fillna(0).values
	export = export.drop_duplicates(subset='TimeStamp')
	export = export.sort_values(by='TimeStamp')
	export= export.reset_index().drop(columns='index')

	appendSource('DayAhead','entsoe-transparency','2020')
	export.to_sql("DayAhead", conn, if_exists = 'append', index = False)
	print("DayAhead\t\t----->\t filled")

def fill_Generation(conn):
	df = pd.read_csv(datapath+"CSV_files\entsoe_act_generation_2015-2020.csv", sep=';')
	df.TimeStamp = pd.to_datetime(df.TimeStamp, utc=True)
	local = pd.DataFrame(df.TimeStamp).set_index('TimeStamp').index.tz_convert('Europe/Berlin')
	df.TimeStamp = local.values

	df = __correctColnames(df)
	
	sources.append(['Generation','entsoe-transparency','2020'])

	df.to_sql("Generation", conn, if_exists = 'append', index = False)
	print("Generation\t\t----->\t filled")

def fill_GenerationUnits(conn):
	df = pd.read_csv(datapath+"CSV_files\entsoe_generation_units.csv", sep=';' , encoding = "utf-8")
	df.TimeStamp = pd.to_datetime(df.TimeStamp, utc=True)
	local = pd.DataFrame(df.TimeStamp).set_index('TimeStamp').index.tz_convert(None)
	df.TimeStamp = local.values
	df = __correctColnames(df)
	df = df.drop_duplicates().fillna(0)

	appendSource('Generation_Units','entsoe-transparency','2020')

	df.to_sql("Generation_Units", conn, if_exists = 'append', index = False)
	print("Generation_units\t----->\t filled")

def fill_WxData(conn):
	df = pd.read_csv(datapath+'\\CSV_files\\ninja_weather_country_DE_merra-2_land_area_weighted.csv', sep=';')
	appendSource('Weather','renewables.ninja','2020')

	df.to_sql("Weather", conn, if_exists = 'append', index = False)
	print("Weather\t\t\t----->\t filled")


def __correctColnames(df):
    original = df.columns
    new = []

    for name in original:
        new.append(name.replace(' ','_').replace('/','_').replace('-','_'))

    for i in range(0,len(original)):
        df = df.rename(columns={original[i]:new[i]})
    return df