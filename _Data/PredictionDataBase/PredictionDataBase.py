import pandas as pd
import sqlite3
from datetime import datetime, timedelta
import numpy as np
import DataHandler as DataHandler
import os
from TableDefinition import *
import DBFunctions as dbfunc

dbFile = DataHandler.datapath+'Prognosedaten.db'

#create list of all create tables statements
valdict = globals()
create_list = [globals()[table_names] for table_names in list(globals().keys()) if "create" in table_names]
#print(create_list[0])
"""create_list = [create_source_table, create_GridLoad_table,create_intraday_table, 
               create_dayahead_table, create_RE_table, 
               create_rebap_table, create_MRL_table, 
               create_SRL_table, create_NRV_table, create_Generation_table]"""

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return conn

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object

    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Exception as e:
        print(e)
		

def findMissing(tableList, columnsDict, prefix = ""):
    for name in tableList:
        df = dbfunc.GetDFfromSqlite(dbFile, name)
        columns = columnsDict[name] if name in columnsDict else None
        DataHandler.findMissing(name, df, df['TimeStamp'].iloc[0], df['TimeStamp'].iloc[-1], '15min', columns)

def fillMissing(conn, tableList, columnsDict):
    from sklearn.experimental import enable_iterative_imputer
    from sklearn.impute import IterativeImputer
    import matplotlib.pyplot as plt
    def getTable(name):
        df = dbfunc.GetDFfromSqlite(dbFile, name)
        df.loc[:, "TimeStamp"] = pd.to_datetime(df["TimeStamp"])
        df.set_index("TimeStamp", inplace = True)
        if "Id" in df:
            df.drop("Id", axis = 1, inplace = True)
        return df
    def getPV(tseries_init):
        # Values in PV are imputed based on minute of day (daily periodicity), current year and current week
        hourperiodsin = np.sin(2 * np.pi * (4 * tseries_init.index.hour +   tseries_init.index.minute/15.0) / 96.0)
        hourperiodcos = np.cos(2 * np.pi * (4 * tseries_init.index.hour +  tseries_init.index.minute/15.0) / 96.0)
        x_vals = np.vstack([tseries_init.index.year, tseries_init.index.isocalendar().week, hourperiodsin, hourperiodcos, tseries_init.values]).T
        tseries_final = pd.Series(IterativeImputer(min_value= 0.0).fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    def getWind(tseries_init):
        # Values in Wind are imputed based on hour of day, current year and current week
        x_vals = np.vstack([tseries_init.index.year, tseries_init.index.isocalendar().week, tseries_init.index.hour, tseries_init.values]).T
        tseries_final = pd.Series(IterativeImputer(min_value= 0.0).fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    def getLoad(tseries_init):
        # Values in Load are imputed based on minute of day (daily periodicity), current year and current week
        hourperiodsin = np.sin(2 * np.pi * (4 * tseries_init.index.hour +  tseries_init.index.minute/15.0) / 96.0)
        hourperiodcos = np.cos(2 * np.pi * (4 * tseries_init.index.hour +  tseries_init.index.minute/15.0) / 96.0)
        x_vals = np.vstack([tseries_init.index.year, tseries_init.index.isocalendar().week, hourperiodsin, hourperiodcos, tseries_init.values]).T
        tseries_final = pd.Series(IterativeImputer(min_value= 0.0).fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    def getHydro(tseries_init):
        # Values in Hydro are imputed based on monthly period, week and year
        monthperiodsin = np.sin(2 * np.pi * tseries_init.index.month / 12.0)
        monthperiodcos = np.cos(2 * np.pi * tseries_init.index.month / 12.0)
        x_vals = np.vstack([tseries_init.index.year, tseries_init.index.isocalendar().week, monthperiodsin, monthperiodcos, tseries_init.values]).T
        tseries_final = pd.Series(IterativeImputer(min_value= 0.0).fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    def getDA(tseries_init):
        # Values in Hydro are imputed based on monthly period, week and year
        dfRE = pd.Series(getTable("RE")[["PV_Ante", "WI_Ante"]].sum(axis=1), name = "RE")
        dfInit = pd.DataFrame(tseries_init.values, index = tseries_init.index, columns = ["Vol"])
        dfLoad = getTable("GridLoad")
        tseries_final = None
        if tseries_init.name == "Volume":
            x = dfInit.join(dfRE, how = "left").join(dfLoad, how = "left").resample('1h').sum()
            x_vals = x.values
            tseries_final = pd.Series(IterativeImputer().fit_transform(x_vals)[:,0], index = x.index).resample('15min').ffill() / 4.0
        elif tseries_init.name == "Price":
            x = dfInit.join(dfRE, how = "left").join(dfLoad, how = "left").resample('1h').mean()
            x_vals = x.values
            tseries_final = pd.Series(IterativeImputer().fit_transform(x_vals)[:,0], index = x.index).resample('15min').ffill()
        endofday = pd.Series([tseries_final[-1],]*3, index =pd.date_range(tseries_final.index[-1], periods = 4, freq = '15min')[1:])
        return tseries_final.append(endofday)
    def getID(tseries_init):
        x_vals = pd.to_numeric(tseries_init, errors = 'coerce').values.reshape(-1,1)
        tseries_final = pd.Series(IterativeImputer().fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    def getNRV(tseries_init):
        # Values in NRV are imputed based on mean NRV
        
        x_vals = pd.to_numeric(tseries_init, errors = 'coerce').values.reshape(-1,1)
        tseries_final = pd.Series(IterativeImputer().fit_transform(x_vals)[:,-1], index = tseries_init.index)
        return tseries_final
    for name in tableList:
        df = getTable(name)
        df = df.resample("15min").asfreq()
        dfRes = df.copy()
        dfRes.reset_index(inplace = True)
        columns = columnsDict[name] if name in columnsDict else df.columns
        for column in columns:
            tseries_init = pd.to_numeric(df[column], errors = "coerce")
            tseries_final = None
            if "PV" in column:
                tseries_final = getPV(tseries_init)
            elif "WI" in column:
                tseries_final = getWind(tseries_init)
            elif "GridLoad" in name:
                tseries_final = getLoad(tseries_init)
            elif "Hydro_Run_of_river_and_poundage" in column:
                tseries_final = getHydro(tseries_init)
            elif "DayAhead" in name:
                tseries_final = getDA(tseries_init)
            elif "NRV" in name:
                tseries_final = getNRV(tseries_init)
            elif "Intraday" in name:
                tseries_final = getID(tseries_init)
            fig, axs = plt.subplots(2,1, sharex= True)
            fig.suptitle(name + " " + column)
            axs[0].plot(tseries_init)
            axs[1].plot(tseries_final)
            dfRes.loc[:, column] = tseries_final.values
            fig.show()
        c = conn.cursor()
        c.execute("DELETE FROM "+ name+ " WHERE ID >= 0")
        dfRes.to_sql(name, conn, if_exists = "append", index = False)
        
            


if __name__ == "__main__":
#create Tables
    conn = create_connection(dbFile)

    if os._exists(dbFile):
        os.remove(dbFile)
    else:
        open(dbFile, mode='w+')

    conn = create_connection(dbFile)
    [create_table(conn, table) for table in create_list]

    DataHandler.fill_APMAX(conn)
    DataHandler.fill_GenerationUnits(conn)
    DataHandler.fill_Generation(conn)
    DataHandler.fill_DayAhead(conn)

    DataHandler.fill_GridLoad(conn)
    DataHandler.fill_MRL(conn)
    DataHandler.fill_Intraday(conn)
    DataHandler.fill_NRV(conn)
    DataHandler.fill_RE(conn)


    DataHandler.fill_Rebap(conn)
    DataHandler.fill_SRL(conn)
    DataHandler.fill_WxData(conn)
    DataHandler.fill_sources(conn)
    findMissing(["DayAhead","Intraday", "RE", "NRV", "GridLoad", "Generation"], {"Generation": ["Hydro_Run_of_river_and_poundage"]})

    fillMissing(conn, ["Intraday", "RE", "GridLoad","DayAhead", "NRV", "Generation"], {"Generation": ["Hydro_Run_of_river_and_poundage"]})
    conn.close()

