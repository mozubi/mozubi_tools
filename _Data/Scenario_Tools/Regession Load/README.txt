Dateien/Tools f�r die Berechnung der Coeffizienten zur Modellierung der Ausl�ndischen Last.


mz_TimeSeriesCreator: Holt die Ursprungsdaten aus der (IZES lokalen) Datenbank und berechnet die Kalendardummies und erzeugt f�r jeden Zeitraum und jedes Land eine Zeitreihe zur Analyse in R


20191021 R_skript.R: Enth�lt das Skript zur stapelweisen multiplen Regression und Ausgabe der jeweiligen Coeffizienten je Inputfile

20191021 Import Lastparameter.xlsm: Enth�lt eine VBA_Sammlung um die Ausgabefiles von R in ein Tabellenblatt zu Importieren sowie aus dem tabellenblatt in eine SQLLite-DB zu schreiben.