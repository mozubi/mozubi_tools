Tool, dass aus der lokalen Datenbank die Temperatur und Lastdaten entnimmt.
Diese werden dann unter berücksichitung der Feiertage für eine Regressionsanalyse mit 
Kalendarischen Dummy Variablen aufbereitet und unter RDatasets gespeichert.

Erwartet im Root Verzeichnis eine MySQLConnstring.txt.
diese enthält AUSSCHLIESSLICH den connectionstring
BSPW:SERVER=127.0.0.1;DATABASE=ConPowerplants;PORT=3306;UID=USERNAME;PWD=password;SslMode=none;

eine Verschlüsselte Verbindung ist nicht implementiert.
