﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mz_TimeSeriesCreator
{
    class Load_Stundenwert
    {
        public byte[] IsHour = new byte[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public byte[] IsDay = new byte[7] { 0, 0, 0, 0, 0, 0, 0 };
        public byte[] IsQuarter = new byte[4] { 0, 0, 0, 0 };

        public DateTime TS { get; set; }
        public double Temperature { get; set; }
        public double ForeCast { get; set; }

        public double relForeCast { get; set; }
        
        public Load_Stundenwert(DateTime timestamp, double forecast, double temp, byte DayValue)
        {
            TS = timestamp;
            Temperature = temp;
            ForeCast = forecast;
            //Setze Stunde
            IsHour[TS.Hour] = 1;
            //Setze Quartal
            int k = ((timestamp.Month + 2) / 3)-1;
            IsQuarter[k] = 1;
            IsDay[DayValue] = 1;

        }

        public void SetRelForecast(double avgForecast)
        {
            relForeCast = ForeCast / avgForecast;
        }
    }
}
