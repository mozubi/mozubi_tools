﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace mz_TimeSeriesCreator
{
    class ConstructLoadDatasets
    {
        public static string ProjectPath { get; private set; }
        private string FolderPathRDataSet;
        MySqlConnection DBConnection;
        private List<Load_Stundenwert> LstStundenwerte;
        private string[] Countries = { "BE","CH","CZ","DE","DK","FR","IT","NL","SE"};
        HolidayDayValues DayValues;
        string HeadlinesRRawdata;
        double AvgLoad;
        int TSCounter;

        public ConstructLoadDatasets()
        {
            ProjectPath = Directory.GetParent(System.Reflection.Assembly.GetCallingAssembly().Location).Parent.Parent.Parent.FullName;
            FolderPathRDataSet = $@"{ProjectPath}\RDatasets\";

            OpenSQLCompute();
            SetHeadlinesRRawdata();
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            
        }
        
        public void GetData()
        {

            DateTime Startdate = new DateTime(2016, 1, 1,0,0,0);
            DateTime EndDate = new DateTime(2016, 12, 31,23,59,0);

            DayValues = new HolidayDayValues(Startdate, EndDate);

            foreach(string ShortCode in Countries)
            {
                getLoadData(ShortCode, Startdate, EndDate);
                WriteRRawData(ShortCode, Startdate, EndDate);
            }

            Console.WriteLine("Fertig <ENTER>");

            Console.ReadLine();

            DBConnection.Close();

        }





        private void getLoadData(string ShortCode, DateTime Start, DateTime Ende)
        {

            string sSQL = BuildSQLString(ShortCode, Start, Ende);
            LstStundenwerte = new List<Load_Stundenwert>();


            //Daten Laden
            MySqlCommand myCommand = new MySqlCommand(sSQL, DBConnection);
            MySqlDataReader myReader;
            myReader = myCommand.ExecuteReader();

            DateTime tmpDate = Start.AddDays(-4);
            byte iTag = 0;
            AvgLoad = 0;
            TSCounter = 0;


            while (myReader.Read())
            {
                DateTime DSDate = myReader.GetDateTime(0);
                if (!(myReader.IsDBNull(1) || myReader.IsDBNull(2))) {
                    double Load = myReader.GetDouble(1);
                    double Temp = myReader.GetDouble(2);
                    AvgLoad += Load;
                    TSCounter += 1;

                    if (tmpDate.Day != DSDate.Day)
                    {
                        iTag = (byte)DayValues.getDay(ShortCode, DSDate);
                        tmpDate = DSDate;
                    }

                    Load_Stundenwert TmpStundenwert = new Load_Stundenwert(DSDate, Load, Temp, iTag);
                    LstStundenwerte.Add(TmpStundenwert);
                }
            }

            AvgLoad = AvgLoad / TSCounter;

            myReader.Close();
            

            ReCalcStundenwerte();



        }


        private void WriteRRawData(string ShortCode, DateTime Start, DateTime Ende)
        {
            string sFile = FolderPathRDataSet + ShortCode + "_" + Start.Year + "-" + Ende.Year + ".csv";

            string DS;

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(sFile))
            {
                file.WriteLine(HeadlinesRRawdata);

                foreach(Load_Stundenwert tmpWerte in LstStundenwerte)
                {
                    DS = tmpWerte.TS.ToString("yyyy-MM-dd HH:mm");
                    DS += "," + tmpWerte.ForeCast;
                    DS += "," + tmpWerte.relForeCast;
                    DS += "," + tmpWerte.Temperature;

                    for (int i = 0; i < 4; i++)
                    {
                        DS += "," + tmpWerte.IsQuarter[i];
                    }
                    for (int i = 0; i < 7; i++)
                    {
                        DS += "," + tmpWerte.IsDay[i];
                    }
                    for (int i = 0; i < 24; i++)
                    {
                        DS += "," + tmpWerte.IsHour[i];
                    }

                    file.WriteLine(DS);


                }


            }

            sFile = FolderPathRDataSet + ShortCode + "_" + Start.Year + "-" + Ende.Year + "_Average.csv";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(sFile))
            {
                file.WriteLine((int)AvgLoad);
            }


        }


        private void SetHeadlinesRRawdata()
        {
            HeadlinesRRawdata = "TS,Load_Forecast,relForecast,Temperature";

            for(int i=0;i<4;i++)
            {
                HeadlinesRRawdata += ",isQuarter_" + i;
            }
            for (int i = 0; i < 7; i++)
            {
                HeadlinesRRawdata += ",isDay_" + i;
            }
            for (int i = 0; i < 24; i++)
            {
                HeadlinesRRawdata += ",isHour_" + i;
            }

        }


        private void ReCalcStundenwerte()
        {
            foreach (Load_Stundenwert Tmp in LstStundenwerte)
            {
                Tmp.SetRelForecast(AvgLoad);
            }
        }



        private bool OpenSQLCompute()
        {
            Console.WriteLine("Getting Conn");
            DBConnection = DBConnections.GetConMGExternMyCon();

            try
            {
                Console.WriteLine("Öffne DB-Verbindung ....");
                DBConnection.Open();

                Console.WriteLine("DB Connection erfolgreich!");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Fehler: " + e.Message);
                Console.WriteLine(DBConnection.ConnectionString);
            }

            return false;

        }

        private string BuildSQLString(string ShortCode, DateTime StartDate, DateTime EndDate)
        {
            //Startdatum um 2 Stunden Reduzieren da UTC

            string sSQL = "SELECT t60_" + ShortCode + ".cet_cest_timestamp, " +
                   "t60_" + ShortCode + ".load_actual_entsoe_transparency, weather_" + ShortCode + ".temperature" +
                   " FROM weather_" + ShortCode + " INNER JOIN t60_" + ShortCode + " ON weather_" + ShortCode +
                   ".ts_utc = t60_" + ShortCode + ".ts_utc" +
                   " WHERE ((t60_" + ShortCode + ".ts_utc >'" + StartDate.AddHours(-2).ToString("yyy-MM-dd H:mm") + "')" +
                   " And (t60_" + ShortCode + ".ts_utc <'" + EndDate.AddHours(-1).ToString("yyy-MM-dd HH:mm") + "'))" +
                   " ORDER BY cet_cest_timestamp ASC;";
                
            return sSQL;
        }



    }
}
