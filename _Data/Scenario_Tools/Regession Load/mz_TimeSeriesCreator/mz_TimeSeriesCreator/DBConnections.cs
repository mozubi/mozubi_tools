﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;

namespace mz_TimeSeriesCreator
{
    static class DBConnections
    {
        public static MySqlConnection GetConMGExternMyCon()
        {
            
            string connString = System.IO.File.ReadAllText(Directory.GetParent(System.Reflection.Assembly.GetCallingAssembly().Location).Parent.Parent.Parent.FullName + "/MySQLConnstring.txt");
            MySqlConnection conn = new MySqlConnection(connString);
            return conn;
        }


    }
}
