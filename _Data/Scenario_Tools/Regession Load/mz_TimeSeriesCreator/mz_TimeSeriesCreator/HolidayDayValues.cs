﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mz_TimeSeriesCreator
{
    class HolidayDayValues
    {

        private DateTime Karfreitag { get; set; }
        private DateTime Gruendonnerstag { get; set; }
        private DateTime Ostermontag { get; set; }
        private DateTime ChristiHimmelfahrt { get; set; }
        private DateTime Pfingstmontag { get; set; }
        private DateTime Fronleichnam { get; set; }
        private DateTime MariaHimmelfahrt { get; set; }
        private DateTime Neujahr { get; set; }
        private DateTime Allerheiligen { get; set; }
        private DateTime TagDerArbeit { get; set; }
        private DateTime ErsterWeichnachtstag { get; set; }
        private DateTime ZweiterWeihnachtstag { get; set; }
        private DateTime DreiKoenige { get; set; }

        private List<(string, DateTime)> LstFeiertag;

        public HolidayDayValues(int Jahr)
        {
            LstFeiertag = new List<(string, DateTime)>();

            SetGlobalHolidays(Jahr);

            setAT(Jahr);
            setBE(Jahr);
            setCH(Jahr);
            setCZ(Jahr);
            setDE(Jahr);
            setDK(Jahr);
            setFR(Jahr);
            setIT(Jahr);
            setNL(Jahr);
            setSE(Jahr);
            setPL(Jahr);
        }
        public HolidayDayValues(List<int> LstJahr)
        {
            LstFeiertag = new List<(string, DateTime)>();

            foreach (int Jahr in LstJahr)
            {

                SetGlobalHolidays(Jahr);

                setAT(Jahr);
                setBE(Jahr);
                setCH(Jahr);
                setCZ(Jahr);
                setDE(Jahr);
                setDK(Jahr);
                setFR(Jahr);
                setIT(Jahr);
                setNL(Jahr);
                setSE(Jahr);
                setPL(Jahr);
            }
        }
        public HolidayDayValues(DateTime StartTag, DateTime EndTag)
        {
            LstFeiertag = new List<(string, DateTime)>();

            DateTime datTmp = StartTag;

            while (datTmp < EndTag.AddDays(355))
            {
                int Jahr = StartTag.Year;

                SetGlobalHolidays(Jahr);

                setAT(Jahr);
                setBE(Jahr);
                setCH(Jahr);
                setCZ(Jahr);
                setDE(Jahr);
                setDK(Jahr);
                setFR(Jahr);
                setIT(Jahr);
                setNL(Jahr);
                setSE(Jahr);
                setPL(Jahr);

                datTmp = datTmp.AddYears(1);
            }
        }

        public int getDay(string Shortcode, DateTime datum)
        {
            int iTag = (int)datum.DayOfWeek;

            if (LstFeiertag.Contains((Shortcode, datum)))
            {
                return 0;
            }

            return iTag;

        }

        private void SetGlobalHolidays(int year)
        {
            DateTime Ostern;

            //http://stackoverflow.com/questions/2510383/how-can-i-calculate-what-date-good-friday-falls-on-given-a-year

            var g = year % 19;
            var c = year / 100;
            var h = (c - c / 4 - (8 * c + 13) / 25 + 19 * g + 15) % 30;
            var i = h - (h / 28) * (1 - (h / 28) * (29 / (h + 1)) * ((21 - g) / 11));

            var day = i - ((year + (int)(year / 4) + i + 2 - c + (int)(c / 4)) % 7) + 28;
            var month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }

            Ostern = new DateTime(year, month, day);

            Gruendonnerstag = Ostern.AddDays(-3);
            Karfreitag = Ostern.AddDays(-2);
            Ostermontag = Ostern.AddDays(1);
            ChristiHimmelfahrt = Ostern.AddDays(39);
            Pfingstmontag = Ostern.AddDays(50);
            Fronleichnam = Ostern.AddDays(60);

            Neujahr = new DateTime(year: year, month: 1, day: 1);
            TagDerArbeit = new DateTime(year: year, month: 5, day: 1);
            ErsterWeichnachtstag = new DateTime(year: year, month: 12, day: 25);
            ZweiterWeihnachtstag = new DateTime(year: year, month: 12, day: 26);
            MariaHimmelfahrt = new DateTime(year: year, month: 8, day: 15);
            Allerheiligen = new DateTime(year: year, month: 11, day: 1);
            DreiKoenige = new DateTime(year: year, month: 1, day: 6);

        }

        private void setDE(int jahr)
        {
            LstFeiertag.Add(("DE", Neujahr));
            LstFeiertag.Add(("DE", Karfreitag));
            LstFeiertag.Add(("DE", Ostermontag));
            LstFeiertag.Add(("DE", ChristiHimmelfahrt));
            LstFeiertag.Add(("DE", Pfingstmontag));
            LstFeiertag.Add(("DE", Fronleichnam));
            LstFeiertag.Add(("DE", TagDerArbeit));
            LstFeiertag.Add(("DE", ErsterWeichnachtstag));
            LstFeiertag.Add(("DE", ZweiterWeihnachtstag));

            LstFeiertag.Add(("DE", new DateTime(jahr, 10, 3)));
        }

        private void setFR(int jahr)
        {
            LstFeiertag.Add(("FR", Neujahr));
            LstFeiertag.Add(("FR", Ostermontag));
            LstFeiertag.Add(("FR", ChristiHimmelfahrt));
            LstFeiertag.Add(("FR", Pfingstmontag));
            LstFeiertag.Add(("FR", Allerheiligen));
            LstFeiertag.Add(("FR", TagDerArbeit));
            LstFeiertag.Add(("FR", MariaHimmelfahrt));
            LstFeiertag.Add(("FR", ErsterWeichnachtstag));
            LstFeiertag.Add(("FR", ErsterWeichnachtstag));
            LstFeiertag.Add(("FR", new DateTime(jahr, 5, 8))); // Tag des Waffenstillstandes
            LstFeiertag.Add(("FR", new DateTime(jahr, 7, 14))); // Nationalfeiertag            
        }

        private void setBE(int jahr)
        {
            LstFeiertag.Add(("BE", Neujahr));
            LstFeiertag.Add(("BE", TagDerArbeit));
            LstFeiertag.Add(("BE", MariaHimmelfahrt));
            LstFeiertag.Add(("BE", Allerheiligen));
            LstFeiertag.Add(("BE", ErsterWeichnachtstag));
            LstFeiertag.Add(("BE", Ostermontag));
            LstFeiertag.Add(("BE", ChristiHimmelfahrt));
            LstFeiertag.Add(("BE", Pfingstmontag));
            LstFeiertag.Add(("BE", new DateTime(jahr, 11, 11))); // Tag des Waffenstillstandes
            LstFeiertag.Add(("BE", new DateTime(jahr, 7, 21))); // Nationalfeiertag                
        }

        private void setDK(int jahr)
        {
            LstFeiertag.Add(("DK", Neujahr));
            LstFeiertag.Add(("DK", Gruendonnerstag));
            LstFeiertag.Add(("DK", Karfreitag));
            LstFeiertag.Add(("DK", Ostermontag));
            LstFeiertag.Add(("DK", ChristiHimmelfahrt));
            LstFeiertag.Add(("DK", Pfingstmontag));
            LstFeiertag.Add(("DK", ErsterWeichnachtstag));
            LstFeiertag.Add(("DK", ZweiterWeihnachtstag));
            LstFeiertag.Add(("DK", new DateTime(jahr, 5, 17))); // Großer Bettag
        }

        private void setNL(int jahr)
        {
            LstFeiertag.Add(("NL", Neujahr));
            LstFeiertag.Add(("NL", Karfreitag));
            LstFeiertag.Add(("NL", Ostermontag));
            LstFeiertag.Add(("NL", ChristiHimmelfahrt));
            LstFeiertag.Add(("NL", Pfingstmontag));
            LstFeiertag.Add(("NL", ErsterWeichnachtstag));
            LstFeiertag.Add(("NL", ZweiterWeihnachtstag));
            LstFeiertag.Add(("NL", new DateTime(jahr, 4, 27))); // Königstag
            LstFeiertag.Add(("NL", new DateTime(jahr, 5, 5))); // Befreiungstag
        }

        private void setAT(int jahr)
        {
            LstFeiertag.Add(("AT", Neujahr));
            LstFeiertag.Add(("AT", Ostermontag));
            LstFeiertag.Add(("AT", ChristiHimmelfahrt));
            LstFeiertag.Add(("AT", Pfingstmontag));
            LstFeiertag.Add(("AT", Fronleichnam));
            LstFeiertag.Add(("AT", MariaHimmelfahrt));
            LstFeiertag.Add(("AT", Allerheiligen));
            LstFeiertag.Add(("AT", ErsterWeichnachtstag));
            LstFeiertag.Add(("AT", ZweiterWeihnachtstag));
            LstFeiertag.Add(("AT", DreiKoenige));
            LstFeiertag.Add(("AT", new DateTime(jahr, 5, 1))); // Staatsfeiertag
            LstFeiertag.Add(("AT", new DateTime(jahr, 10, 26))); // Nationalfeiertag
            LstFeiertag.Add(("AT", new DateTime(jahr, 12, 8))); // Maria Empfängnis
        }

        private void setCZ(int jahr)
        {
            LstFeiertag.Add(("CZ", Neujahr));
            LstFeiertag.Add(("CZ", Karfreitag));
            LstFeiertag.Add(("CZ", Ostermontag));
            LstFeiertag.Add(("CZ", TagDerArbeit));
            LstFeiertag.Add(("CZ", ErsterWeichnachtstag));
            LstFeiertag.Add(("CZ", ZweiterWeihnachtstag));

            LstFeiertag.Add(("CZ", new DateTime(jahr, 5, 1))); // Befreiung
            LstFeiertag.Add(("CZ", new DateTime(jahr, 7, 5))); // Apostel
            LstFeiertag.Add(("CZ", new DateTime(jahr, 7, 6))); // Magister
            LstFeiertag.Add(("CZ", new DateTime(jahr, 9, 28))); // Staatlichkeit
            LstFeiertag.Add(("CZ", new DateTime(jahr, 10, 28))); // Staatsgründung
            LstFeiertag.Add(("CZ", new DateTime(jahr, 11, 17))); // Studentenkampf
            LstFeiertag.Add(("CZ", new DateTime(jahr, 12, 24))); // Heiligabend
        }

        private void setCH(int jahr)
        {
            LstFeiertag.Add(("CH", Neujahr));
            LstFeiertag.Add(("CH", ChristiHimmelfahrt));
            LstFeiertag.Add(("CH", ErsterWeichnachtstag));
            LstFeiertag.Add(("CH", Karfreitag));
            LstFeiertag.Add(("CH", Ostermontag));
            LstFeiertag.Add(("CH", Pfingstmontag));
            LstFeiertag.Add(("CH", ZweiterWeihnachtstag));
            LstFeiertag.Add(("CH", new DateTime(jahr, 8, 1))); //Bundesfeier
        }

        private void setPL(int jahr)
        {
            LstFeiertag.Add(("PL", Neujahr));
            LstFeiertag.Add(("PL", DreiKoenige));
            LstFeiertag.Add(("PL", Ostermontag));
            LstFeiertag.Add(("PL", TagDerArbeit));
            LstFeiertag.Add(("PL", Fronleichnam));
            LstFeiertag.Add(("PL", MariaHimmelfahrt));
            LstFeiertag.Add(("PL", Allerheiligen));
            LstFeiertag.Add(("PL", ErsterWeichnachtstag));
            LstFeiertag.Add(("PL", ZweiterWeihnachtstag));

            LstFeiertag.Add(("PL", new DateTime(jahr, 5, 3))); //Nationalfeiertag
            LstFeiertag.Add(("PL", new DateTime(jahr, 11, 11))); //UNabhängikeit
        }

        private void setIT(int jahr)
        {
            LstFeiertag.Add(("IT", Neujahr));
            LstFeiertag.Add(("IT", DreiKoenige));
            LstFeiertag.Add(("IT", Ostermontag));
            LstFeiertag.Add(("IT", TagDerArbeit));
            LstFeiertag.Add(("IT", Pfingstmontag));
            LstFeiertag.Add(("IT", MariaHimmelfahrt));
            LstFeiertag.Add(("IT", Allerheiligen));
            LstFeiertag.Add(("IT", ErsterWeichnachtstag));
            LstFeiertag.Add(("IT", ZweiterWeihnachtstag));

            LstFeiertag.Add(("IT", new DateTime(jahr, 4, 25))); //Tag der Befreiung
            LstFeiertag.Add(("IT", new DateTime(jahr, 6, 2))); //Nationalfeiertag
            LstFeiertag.Add(("IT", new DateTime(jahr, 12, 31))); // SIlvester
        }

        private void setSE(int jahr)
        {
            LstFeiertag.Add(("SE", Neujahr));
            LstFeiertag.Add(("SE", DreiKoenige));
            LstFeiertag.Add(("SE", Karfreitag));
            LstFeiertag.Add(("SE", Ostermontag));
            LstFeiertag.Add(("SE", TagDerArbeit));
            LstFeiertag.Add(("SE", ChristiHimmelfahrt));
            LstFeiertag.Add(("SE", Allerheiligen));
            LstFeiertag.Add(("SE", ErsterWeichnachtstag));
            LstFeiertag.Add(("SE", ZweiterWeihnachtstag));
            LstFeiertag.Add(("SE", new DateTime(jahr, 6, 6))); //Nationalfeiertag
            LstFeiertag.Add(("SE", new DateTime(jahr, 6, 24))); //Midsommertag

        }

    }
}
