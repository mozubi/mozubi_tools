Dieses Projekt dient der Sammlung von Tools, welche zur Erstellung des MozuBi-Hauptprojekts oder zur Aufbereitung von Daten verwendet wurden.


| **Tool** | **Beschreibung** |
| ------ | ------ |
| [**EEG_Asset_Aggregation**](https://gitlab.com/mozubi/mozubi_tools/tree/master/EEG_Asset_Aggregation) | Programm zur Aggregation von ÜNB-EEG-Anlagen aus OPSD Wetterdatenbank, welche zusätzlich benötigt wird und [hier](https://data.open-power-system-data.org/renewable_power_plants/2019-04-05) heruntergeladen werden kann. |
| [**NUTS2xml**](https://gitlab.com/mozubi/mozubi_tools/tree/master/NUTS2xml) | Tools, das NUTS-Zonen entsprechend der [EuroStat-NUTS-Daten](https://ec.europa.eu/eurostat/de/web/nuts/correspondence-tables/postcodes-and-nuts) auswertet und als XML oder CSV zurückgibt. Location-Informationen werden mittels BING-Geocode-API geladen. | 
| [**MaStR-Processor**](https://gitlab.com/mozubi/mozubi_tools/tree/master/MaStR-Processor) | Tool zur Verarbeitung und Aufbereitung der Marktstammdatenregister (MaStR)-Dumpfiles (Excel) hin zu einer SQLite Datenbank|
